{
    "id": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_cat_Sasha_bounce",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "707e205f-8f33-41b7-927e-fccb6a581212",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "cf70c87d-67f9-49f7-a5a9-8e186dc9193a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "707e205f-8f33-41b7-927e-fccb6a581212",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5da99d5-eb22-474e-85bc-9748d3d18be7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "707e205f-8f33-41b7-927e-fccb6a581212",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "8729e270-bf1c-488b-acac-3576f19fed50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "8faec621-6155-4b97-951b-6e75146649db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8729e270-bf1c-488b-acac-3576f19fed50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a9584a1-5ffb-4124-98d3-5d2730285b76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8729e270-bf1c-488b-acac-3576f19fed50",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "eca4d8e6-2b29-4008-9249-3de024bcb6eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "63bb1723-df32-42bd-9728-52a33d162714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eca4d8e6-2b29-4008-9249-3de024bcb6eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8a71db4-0b68-480f-8941-6b1dc4554eb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eca4d8e6-2b29-4008-9249-3de024bcb6eb",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "505dafe0-162f-408f-932f-69d59e3e30c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "9369f461-6a4a-4dd4-a7ef-f5429ec728a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "505dafe0-162f-408f-932f-69d59e3e30c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1059326d-b771-48eb-b998-af7e3d28aebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "505dafe0-162f-408f-932f-69d59e3e30c7",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "900b59b3-3fc0-41ce-9a66-7d18a22d5519",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "6c64eefc-9340-4061-b645-5138da3d8749",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "900b59b3-3fc0-41ce-9a66-7d18a22d5519",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "627d7c85-d799-46ed-aa5f-f98ce3d65a32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "900b59b3-3fc0-41ce-9a66-7d18a22d5519",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "e2300576-c4d7-4bca-b8e8-fadb0f2c2e24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "ac653f51-8471-494d-a582-5199a728c519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2300576-c4d7-4bca-b8e8-fadb0f2c2e24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e423fc58-9432-47dc-b540-7aea0bc4227c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2300576-c4d7-4bca-b8e8-fadb0f2c2e24",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "853aa6f8-22dd-4885-9622-90076f4a8cea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "aca65e46-5011-42c4-b478-c382f5f107f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "853aa6f8-22dd-4885-9622-90076f4a8cea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f57ada9b-eb68-4ce0-9aa8-016d2e30f622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "853aa6f8-22dd-4885-9622-90076f4a8cea",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "6011aeb6-9a12-4bf9-802c-e197c4707ddd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "5b34853e-55e5-44c7-ab9f-bc7d56740898",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6011aeb6-9a12-4bf9-802c-e197c4707ddd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1de03ca-93b9-4f8c-84dc-d8df6678871f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6011aeb6-9a12-4bf9-802c-e197c4707ddd",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "2d642692-9b3c-4951-99df-4fcb24e4cb52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "07483402-6b0d-4c6b-81a4-317a658aba8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d642692-9b3c-4951-99df-4fcb24e4cb52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df75c3b3-7849-47b1-9dc1-b4ae056184bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d642692-9b3c-4951-99df-4fcb24e4cb52",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "b335b8a3-cfcf-4648-8be3-2bdd261df988",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "36f01dd0-df48-4638-b438-89b5d42f49e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b335b8a3-cfcf-4648-8be3-2bdd261df988",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08f4a156-3f34-4b5f-aad3-fdfce1011472",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b335b8a3-cfcf-4648-8be3-2bdd261df988",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "08abfff4-e7ff-4948-b342-5f921ebc4626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "1b6205b2-44eb-416f-8690-496b2746a96d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08abfff4-e7ff-4948-b342-5f921ebc4626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34661c5c-398d-4c1f-8a8d-621f4ce890ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08abfff4-e7ff-4948-b342-5f921ebc4626",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "85403a82-ec18-4dc6-8c62-8e2619af0b7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "20ed0e75-bf0b-43c9-95d9-5be6b39a7e03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85403a82-ec18-4dc6-8c62-8e2619af0b7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba19e423-71ec-4867-b83b-2e53e8e05237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85403a82-ec18-4dc6-8c62-8e2619af0b7d",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "2c883097-20f3-4e35-8c9f-7848df27e110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "c4fb87df-b1bf-4858-8973-71b6149175f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c883097-20f3-4e35-8c9f-7848df27e110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cad949f2-be37-4f80-9c7d-9b5e1eb85d27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c883097-20f3-4e35-8c9f-7848df27e110",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "99d37b42-78a9-4682-9885-af7d21af3ddd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "a70cf3c5-2ca9-4a42-99cc-e6cb2eda8c92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99d37b42-78a9-4682-9885-af7d21af3ddd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "639bae52-c883-4a2f-9650-9c47511957a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99d37b42-78a9-4682-9885-af7d21af3ddd",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "82b7a528-b107-4a7e-bb1d-8576c1df4522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "57ad105d-c31d-4140-8466-57d59ab4f8b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82b7a528-b107-4a7e-bb1d-8576c1df4522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5844b0c8-ef80-4f13-8b72-6ddfb2ea2152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82b7a528-b107-4a7e-bb1d-8576c1df4522",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "b445cb31-fd70-4dff-989c-fae0b172f8af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "a879b132-3627-4ead-b99e-8e76ef99c107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b445cb31-fd70-4dff-989c-fae0b172f8af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e60d1c5e-b567-4646-95ac-eb5dcaddea9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b445cb31-fd70-4dff-989c-fae0b172f8af",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "2f3f3e8f-17d9-4f53-8340-eceecc753455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "58ecfc31-5ff0-4ac7-a05f-6ed3c90e24c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f3f3e8f-17d9-4f53-8340-eceecc753455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd07d91f-d7a3-47f2-a303-bb36ab915d56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f3f3e8f-17d9-4f53-8340-eceecc753455",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "523a1a83-5e67-42a4-a0cd-bb005f23c212",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "12639464-dedd-4584-b24f-6b2a9a491d24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "523a1a83-5e67-42a4-a0cd-bb005f23c212",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdbc63a6-8287-480f-b602-d73bad096ab9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "523a1a83-5e67-42a4-a0cd-bb005f23c212",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "fd8c0f50-ae37-4acf-927c-15589691640b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "9777027a-eaa8-4ecc-a059-b98b2c8b7586",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd8c0f50-ae37-4acf-927c-15589691640b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a3a0481-c1bd-448a-987d-48ce9b027c99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd8c0f50-ae37-4acf-927c-15589691640b",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        },
        {
            "id": "820047ca-357c-40b5-804b-2492f772e117",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "compositeImage": {
                "id": "65b98ff1-57e5-4dc8-9852-42e7048ddb9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "820047ca-357c-40b5-804b-2492f772e117",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b117f1a2-ceac-4a8f-8d46-4562fdccc816",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "820047ca-357c-40b5-804b-2492f772e117",
                    "LayerId": "68b36a19-887f-43d9-9f62-dcd5a9ccde38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "68b36a19-887f-43d9-9f62-dcd5a9ccde38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 13,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 23
}