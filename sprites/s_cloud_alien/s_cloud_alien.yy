{
    "id": "9c174384-b94e-44b4-aeeb-10757da487c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_cloud_alien",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7065dec2-46f9-40a3-b5b5-456388163c92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c174384-b94e-44b4-aeeb-10757da487c5",
            "compositeImage": {
                "id": "79be188d-f3ba-4c23-9e4a-666872200f77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7065dec2-46f9-40a3-b5b5-456388163c92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a43a194-046c-49ea-bbf8-3109b174c5af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7065dec2-46f9-40a3-b5b5-456388163c92",
                    "LayerId": "99592c6d-1d75-4e22-bb10-0f55f40878f9"
                }
            ]
        },
        {
            "id": "d257e8da-016f-4452-890f-646a02ea3c23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c174384-b94e-44b4-aeeb-10757da487c5",
            "compositeImage": {
                "id": "fd5d32f5-1f53-40a1-a32b-d66acdf72aa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d257e8da-016f-4452-890f-646a02ea3c23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7df42fef-4954-4b4a-ab38-b4865807403c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d257e8da-016f-4452-890f-646a02ea3c23",
                    "LayerId": "99592c6d-1d75-4e22-bb10-0f55f40878f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "99592c6d-1d75-4e22-bb10-0f55f40878f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c174384-b94e-44b4-aeeb-10757da487c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 19
}