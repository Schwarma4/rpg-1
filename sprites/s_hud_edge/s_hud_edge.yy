{
    "id": "814c65e4-f8ee-4070-a0ad-ad08a8efab7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hud_edge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ae3e3d0-a02e-45c5-9e1e-254188d7ea66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "814c65e4-f8ee-4070-a0ad-ad08a8efab7b",
            "compositeImage": {
                "id": "e9ad0f4b-1e5f-4ca6-894b-525c1a6ba176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ae3e3d0-a02e-45c5-9e1e-254188d7ea66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36ad9032-129f-43da-99a1-5695217c2e40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ae3e3d0-a02e-45c5-9e1e-254188d7ea66",
                    "LayerId": "0de059b2-cb86-47de-baa3-d7d4b39f95a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "0de059b2-cb86-47de-baa3-d7d4b39f95a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "814c65e4-f8ee-4070-a0ad-ad08a8efab7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 32
}