{
    "id": "f8a70356-9f7d-4b34-88f4-feb3f4b44fd5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_slingshot_dart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2a42936-baaa-4be2-a011-805518def80c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8a70356-9f7d-4b34-88f4-feb3f4b44fd5",
            "compositeImage": {
                "id": "647487ba-ddf9-4d6e-8ee3-f733c2ea3f43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2a42936-baaa-4be2-a011-805518def80c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9b9ffb6-e625-4906-bef0-ba56508173ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2a42936-baaa-4be2-a011-805518def80c",
                    "LayerId": "2df7cf1c-83a7-4c73-8179-c80e0ff7e4c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "2df7cf1c-83a7-4c73-8179-c80e0ff7e4c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8a70356-9f7d-4b34-88f4-feb3f4b44fd5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 0,
    "yorig": 3
}