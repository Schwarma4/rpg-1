{
    "id": "13189f20-d963-4385-b184-0a7bff234f36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "700ec3c5-f9c9-4930-bb85-dbdb6dd1c175",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13189f20-d963-4385-b184-0a7bff234f36",
            "compositeImage": {
                "id": "98c9c8ce-8a4d-4995-aecb-45e1a6ea52fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "700ec3c5-f9c9-4930-bb85-dbdb6dd1c175",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64722c7b-8fce-4b79-9de3-5703cdd18a9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "700ec3c5-f9c9-4930-bb85-dbdb6dd1c175",
                    "LayerId": "67d8a4a8-be24-4cb1-9cf5-d2cf798202e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "67d8a4a8-be24-4cb1-9cf5-d2cf798202e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13189f20-d963-4385-b184-0a7bff234f36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}