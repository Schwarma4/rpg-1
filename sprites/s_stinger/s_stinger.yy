{
    "id": "95cb6e04-362d-474f-88e7-727552b9a2a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_stinger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5dbcb35-4eb0-44d8-9983-38a223bcf20b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95cb6e04-362d-474f-88e7-727552b9a2a5",
            "compositeImage": {
                "id": "3deca6dd-128b-4799-bf45-6626ef866989",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5dbcb35-4eb0-44d8-9983-38a223bcf20b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39bc63ba-8c94-4770-ba26-697f65e0ce6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5dbcb35-4eb0-44d8-9983-38a223bcf20b",
                    "LayerId": "c3cf69aa-163a-4035-9cd4-e0e45489b83a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "c3cf69aa-163a-4035-9cd4-e0e45489b83a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95cb6e04-362d-474f-88e7-727552b9a2a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 0,
    "yorig": 3
}