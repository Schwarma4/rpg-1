{
    "id": "7dedc7d2-ba1f-4001-a72b-b1ae100c1dec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bat",
    "eventList": [
        {
            "id": "69e8fc4f-cde4-4f0c-8297-babe75f14e24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7dedc7d2-ba1f-4001-a72b-b1ae100c1dec"
        },
        {
            "id": "acdc4968-9758-4bba-9417-dd71528270f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "7dedc7d2-ba1f-4001-a72b-b1ae100c1dec"
        },
        {
            "id": "39bed465-bee8-4cbe-837c-15a9bd614bee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "7dedc7d2-ba1f-4001-a72b-b1ae100c1dec"
        },
        {
            "id": "c816c065-20e1-4097-8d04-fe631f49e0f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "7dedc7d2-ba1f-4001-a72b-b1ae100c1dec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e45b7d6c-9a43-4674-a7e3-af78639eee20",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "25f05a28-7c02-4a04-8c34-a514048ad117",
    "visible": true
}