{
    "id": "df9f7251-a240-4b92-8280-9e8b478240e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_WORM_run_right1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "442c6032-d56e-4340-8004-9ce642c7f12c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df9f7251-a240-4b92-8280-9e8b478240e5",
            "compositeImage": {
                "id": "d0322d00-f95a-4c08-adc8-1c863b5ec842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "442c6032-d56e-4340-8004-9ce642c7f12c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f6c4524-cdd6-4dbe-a85b-f816a7f6c1e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "442c6032-d56e-4340-8004-9ce642c7f12c",
                    "LayerId": "43781705-199b-42b9-8332-d4b858717361"
                }
            ]
        },
        {
            "id": "bd894c69-fc5a-492a-ae9f-0190a45b22e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df9f7251-a240-4b92-8280-9e8b478240e5",
            "compositeImage": {
                "id": "b9151c60-7156-40bc-8e17-f8abace271e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd894c69-fc5a-492a-ae9f-0190a45b22e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "143acb86-2827-45c8-b787-6ce08002b779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd894c69-fc5a-492a-ae9f-0190a45b22e8",
                    "LayerId": "43781705-199b-42b9-8332-d4b858717361"
                }
            ]
        },
        {
            "id": "b239ae2c-addf-4ca5-b0f4-9ac1d31c6ec5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df9f7251-a240-4b92-8280-9e8b478240e5",
            "compositeImage": {
                "id": "c97d63aa-a8f4-4642-a786-95561bc024d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b239ae2c-addf-4ca5-b0f4-9ac1d31c6ec5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6521313-e667-4363-a0dc-c98a683f142d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b239ae2c-addf-4ca5-b0f4-9ac1d31c6ec5",
                    "LayerId": "43781705-199b-42b9-8332-d4b858717361"
                }
            ]
        },
        {
            "id": "83cfd21d-fba8-4816-af34-4e71bb733446",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df9f7251-a240-4b92-8280-9e8b478240e5",
            "compositeImage": {
                "id": "8197ade1-da91-4c4d-b2ea-5c483b3f83eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83cfd21d-fba8-4816-af34-4e71bb733446",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d9fa473-8ef6-42dd-ad48-1a3e0b31db8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83cfd21d-fba8-4816-af34-4e71bb733446",
                    "LayerId": "43781705-199b-42b9-8332-d4b858717361"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "43781705-199b-42b9-8332-d4b858717361",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df9f7251-a240-4b92-8280-9e8b478240e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 11,
    "yorig": 14
}