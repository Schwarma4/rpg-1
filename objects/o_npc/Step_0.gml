if o_input.dialogue_pressed_ {
	if place_meeting(x,y, o_player) {
		if myTextbox_ = noone {
			myTextbox_ = instance_create_layer(x, y, "Text", o_textbox);	
			myTextbox_.text_ = myText_;
			myTextbox_.creator = self;
			myTextbox_.name_ = MyName_;
		}
	} else {
		if myTextbox_ != noone {
			instance_destroy(myTextbox_);	
			myTextbox_ = noone;
		}
	}
}
