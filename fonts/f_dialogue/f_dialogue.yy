{
    "id": "916d834a-348b-4816-ac1e-c27911b5d1e1",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "f_dialogue",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6df78a73-7e73-4498-bbaa-a8b9c6f2f861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 93,
                "y": 72
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "521e3bbe-b25f-4cfb-8dca-c9487f7300e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 68,
                "y": 72
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d7cdfd85-f75c-43d2-b5cd-59e8865d036f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 20,
                "y": 72
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "270353f1-f55e-410e-9a2d-6922112968f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 58
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ef5277fd-9cf2-4151-9a3b-fecbbfeb4059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 58
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8354c7c9-0d68-49fe-867f-4e7053720cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 12,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "702af38e-6d7f-4ef0-9e67-65a418979329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 102,
                "y": 16
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "08217373-d988-4b9f-a70c-6ac290d89c21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 113,
                "y": 72
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "46e45851-ecd4-47b0-92c2-a864ba89b989",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 14,
                "y": 72
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c274899d-954a-4f60-a9b6-d590763c7804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "915a7836-26ca-49df-ac41-09283d846d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 120,
                "y": 58
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "615f1ac4-1311-40fc-84b7-5b49d197dead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 58
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2b5eecb0-e858-4099-a2a3-1534a7bc6437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 103,
                "y": 72
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d4be9a5c-295e-4180-a32e-638f460150ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 8,
                "y": 72
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0236d3ec-4fd3-41ad-aef6-3af44e19f673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "4f591d3b-47f2-4541-a337-cf38209c5319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 38,
                "y": 72
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e6a69439-b62d-4c55-a386-383044e1a5a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 58
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "42ebd984-f862-440a-bc3f-1f507cf39238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 44,
                "y": 72
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "43bf85f3-520d-4337-9c13-53fb9f9ffa80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7e33b556-94e9-4390-9466-5e27a0e8bfe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "cb28b1ed-9dbe-46cf-b796-6ad756441fa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 58
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4d110204-559f-4edd-a013-22dac72c6393",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 58
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5b71730b-d295-4aea-b2ed-cbaedca69edf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5a3c8950-81ec-4a2c-8f00-bd97321222bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0621c690-1a76-444f-aebe-e52d109192ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 58
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5d89be3e-088d-46ec-a5aa-fa8a2eb3abb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 58
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8d8f235a-74d9-4ec2-8221-790c20ff23b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 117,
                "y": 72
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a6cb3a5d-96c1-43e1-a801-aa52af09673d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 73,
                "y": 72
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "59c2cb73-151e-471d-9ac9-2fb2ac0a8bc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 58
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "40539991-89ad-403c-b701-edcf55b85b75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 58
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "45f02b5f-3609-4221-91c6-d1b35f2eaff1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 58
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2a718050-8ec1-4968-91d6-a5144d01b108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 58
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "66806038-a964-4f5e-85be-8089e957d1f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 12,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f3b90181-bb1f-41b3-bff9-ba376c792ab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 12,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6a6245e9-efc9-439d-90e3-68f3b5e22f11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 30
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3b003f15-4cb3-4853-9ce3-af601140f77a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 16
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e2eef18d-1fef-4453-9a23-e1db5bcc4f5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1bd3f4d9-534e-4240-81ae-3a071b220b40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 30
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d66b5e8c-752c-41b9-9084-fa9c0ff3067b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 30
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3147db79-a144-4177-a702-374a7b4329cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 16
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "35e0db42-c6a0-4822-af16-3d9d123f8e36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "85f8c876-7572-4e9d-9473-0cf011c55ea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 6,
                "y": 86
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5d4f9f4a-cacc-4aaf-a00a-9fba81edae10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 113,
                "y": 58
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "bbd28e89-a68a-422d-a085-54aa4d254d91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 32,
                "y": 16
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "03adfe6c-0f66-4706-bd5b-6126b3ecd5b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "91d3c8e2-c18f-4cc3-95d3-79cafdcac0ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f26e054f-23f0-4c52-8dad-915d19055648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 82,
                "y": 16
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b23e6005-481a-44a2-9273-572f71dc51ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f5e41fc3-b130-4aff-aee1-5f2e2e7a1985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 112,
                "y": 16
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7f58989c-1fa6-4a2b-b7b3-dd28e07d9e7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7082b9a8-7dfe-49d4-ac20-a09a1218b96a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 16
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "332c854c-cb84-44f4-a769-ec4aa822c61f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 30
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4baf3d6c-1991-4929-855f-f14418e566cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 30
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "78baec85-c172-44c9-b5aa-e72eb54f446b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 16
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ca38fe75-2fdb-458c-892e-39c4f640522a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 22,
                "y": 16
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a1e69524-848e-46c6-9f7e-19035026d9dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 12,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "04b7c0b3-d722-4c53-92c8-26d8a0714a50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 42,
                "y": 16
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "46cbebbd-1839-46d3-a962-035430140c6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "37d93e88-647e-4635-bb39-a468d956f01b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d88c5e55-752c-42bb-9122-b27f4a6a8e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 78,
                "y": 72
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e6a1e48f-eb00-4991-b958-149d39c6eafe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 50,
                "y": 72
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c0cd4998-ac25-4bad-aed4-89643ddc2af7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 108,
                "y": 72
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b9c6e6c7-5b14-4482-96bb-e719a0f9b4ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 106,
                "y": 58
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "07062a3c-aaf8-49fc-a288-88e1ca086fbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 72,
                "y": 16
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "21198bba-a5bf-4db3-afcb-b522286e7c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 98,
                "y": 72
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4e2c2d8a-3bf2-4612-bd9b-7531612be2d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 44
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "496ae7da-6129-450c-b487-23269042eade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "184c69f9-4cde-4966-9f78-3207cc2be914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "44db8d6a-8396-4efd-b1a3-94f142a0058b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 44
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "deef10f5-426e-4ade-8bd9-8b8d76d3130d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 104,
                "y": 30
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ef6954bb-6608-4a63-ac5a-66b71fbd7675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 62,
                "y": 72
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9a790756-29f9-422a-bae0-7bd2975753ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 44
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "edd1a11c-5781-4d7c-895b-3099c2310936",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 44
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "74890a86-677b-49d7-a61a-12abe84b2ea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 121,
                "y": 72
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1e2b85ac-450a-4c9b-9192-01424118f555",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 12,
                "offset": -1,
                "shift": 2,
                "w": 3,
                "x": 83,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1d44cdb0-f687-41b3-a4c5-6f4200d0dd81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 44
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2a8c3e24-0772-4eaf-a943-98d20f4178e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 10,
                "y": 86
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7fee4879-f9f9-4508-96c3-fdcf776cb13e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "317ed307-0392-4657-aa13-5791aeb232c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6564e4f6-f14d-498f-b16c-98407e80cd33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "aafb6eda-9f9c-49b5-ae78-3f40c666536e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 112,
                "y": 30
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "47433ff4-7713-4f76-a592-76ad4f2eb31f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 96,
                "y": 30
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9e000fe2-fdde-4a00-adf0-e8aa5aae676f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 32,
                "y": 72
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d256973c-66db-4531-9bbf-3a6bfeabc9cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "55662834-cd7f-4048-90f6-306052cb49e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 88,
                "y": 72
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "35f30851-5a1e-42c2-bd8b-dba741b52ca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 88,
                "y": 30
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4815b376-36d3-4279-9b52-cf02c78f83b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 80,
                "y": 30
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1f515ab5-bc1d-40b6-a044-f7f268bce627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 92,
                "y": 16
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7c1df32b-d9bc-457e-94cc-e5023178b3e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 72,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e0656e84-2670-4574-83c9-ff3c79e8104c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 64,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "97751b72-e028-41c0-a040-efe395a3785f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 56,
                "y": 30
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "10e0858d-f782-454c-89b9-54163281a61e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 26,
                "y": 72
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d2ed458c-1cce-4a6b-84c5-e5dad4843ef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 14,
                "y": 86
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f7939ddd-bf5c-47ac-be64-aa5c3e9069e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 56,
                "y": 72
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4a72033c-397e-4c49-9259-78fd7e0cd2ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 44
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "4abe9d0f-4898-4998-8154-3cf2e48e18c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "99522825-6c4b-4c90-a41c-f7496d860d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "045240d7-4a44-4bd8-b1e8-7f41d951b89a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "a43d018c-d99f-4b2b-9edf-3055bdd9a7b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "03903677-084d-42cc-a0ac-1057fdca8208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "b974a4a3-71dc-4698-a49b-28e3d4b64738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "b8885d03-c950-46d3-8af3-4f6f2649de23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "c6b0ef0c-e61a-4be9-b1e9-3acb948193a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "c265c83f-b6e1-4a4a-b4a3-5b574cb887a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "301e42e3-7c56-4d97-b7f9-f56e0b9e959f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "0526fca5-817c-42f8-bb91-3c3b822485c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "bb4acb37-10b1-46b4-aac1-a78c68275ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "72fdf432-5504-4176-ad19-92577e70165f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "49362240-2ab2-47ac-baf0-1bf0b67742c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "760826b9-d635-4560-9175-748961796583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "a08e948e-4b5b-4854-9221-b775c34724bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}