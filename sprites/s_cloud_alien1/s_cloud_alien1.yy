{
    "id": "6e2c54c5-7d41-42dc-8052-ef1f28799290",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_cloud_alien1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee9aceab-e1b8-4189-b1fd-3f892d0f5461",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e2c54c5-7d41-42dc-8052-ef1f28799290",
            "compositeImage": {
                "id": "4a9217f2-ef0c-4328-9358-1df8a906b39e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee9aceab-e1b8-4189-b1fd-3f892d0f5461",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "072bfec6-63fc-46ff-9172-0d941d6a142a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee9aceab-e1b8-4189-b1fd-3f892d0f5461",
                    "LayerId": "f1c060c6-79d5-4a94-82d1-c2928d3f5485"
                }
            ]
        },
        {
            "id": "b72faa30-ee7a-45a2-bc17-d5ef8f7152d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e2c54c5-7d41-42dc-8052-ef1f28799290",
            "compositeImage": {
                "id": "520866f7-40c4-4c33-a9f7-7bc7273667b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b72faa30-ee7a-45a2-bc17-d5ef8f7152d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1656761-206a-433a-8d4e-df7d26802c13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b72faa30-ee7a-45a2-bc17-d5ef8f7152d4",
                    "LayerId": "f1c060c6-79d5-4a94-82d1-c2928d3f5485"
                }
            ]
        },
        {
            "id": "6948c16f-647a-47cf-aee5-f88f3a795aca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e2c54c5-7d41-42dc-8052-ef1f28799290",
            "compositeImage": {
                "id": "4708f111-729f-41cd-925f-dcc709d0d9aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6948c16f-647a-47cf-aee5-f88f3a795aca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df3d789b-4f91-496a-8ead-1493142f63f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6948c16f-647a-47cf-aee5-f88f3a795aca",
                    "LayerId": "f1c060c6-79d5-4a94-82d1-c2928d3f5485"
                }
            ]
        },
        {
            "id": "d73aa4ab-47cc-4fc0-b1ff-b8cfaf264968",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e2c54c5-7d41-42dc-8052-ef1f28799290",
            "compositeImage": {
                "id": "4cf73a4e-bda1-45fb-9711-dbb562cacc4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d73aa4ab-47cc-4fc0-b1ff-b8cfaf264968",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65083349-2f8a-48ea-bca6-822fbe7f0d2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d73aa4ab-47cc-4fc0-b1ff-b8cfaf264968",
                    "LayerId": "f1c060c6-79d5-4a94-82d1-c2928d3f5485"
                }
            ]
        },
        {
            "id": "a5f9de31-272d-4fbd-a0a2-9cea24cf8a6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e2c54c5-7d41-42dc-8052-ef1f28799290",
            "compositeImage": {
                "id": "6e4d8909-1773-4cba-afb3-8109270f31a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5f9de31-272d-4fbd-a0a2-9cea24cf8a6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b63e57a-2d9a-441c-84d9-9633a4dca0a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5f9de31-272d-4fbd-a0a2-9cea24cf8a6f",
                    "LayerId": "f1c060c6-79d5-4a94-82d1-c2928d3f5485"
                }
            ]
        },
        {
            "id": "2775ea47-2fcf-4482-9b04-c59b93ae06e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e2c54c5-7d41-42dc-8052-ef1f28799290",
            "compositeImage": {
                "id": "0af367ce-eff3-4c1a-8b30-31be7c16b8c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2775ea47-2fcf-4482-9b04-c59b93ae06e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "834308eb-5dfc-4650-b942-31bc4f8fd38c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2775ea47-2fcf-4482-9b04-c59b93ae06e5",
                    "LayerId": "f1c060c6-79d5-4a94-82d1-c2928d3f5485"
                }
            ]
        },
        {
            "id": "fd90c458-df5d-4f43-b073-c909ae343dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e2c54c5-7d41-42dc-8052-ef1f28799290",
            "compositeImage": {
                "id": "3f87c983-e13d-4595-8401-a17a74969c11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd90c458-df5d-4f43-b073-c909ae343dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79f30832-88dd-4ff6-a096-05b0abeb156b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd90c458-df5d-4f43-b073-c909ae343dce",
                    "LayerId": "f1c060c6-79d5-4a94-82d1-c2928d3f5485"
                }
            ]
        },
        {
            "id": "b8a7e876-634e-4775-ae80-efa530116067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e2c54c5-7d41-42dc-8052-ef1f28799290",
            "compositeImage": {
                "id": "36cb0d10-c931-4654-9f3f-1a03c741fab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8a7e876-634e-4775-ae80-efa530116067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1501645f-f657-473d-b696-4d85bee1963a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8a7e876-634e-4775-ae80-efa530116067",
                    "LayerId": "f1c060c6-79d5-4a94-82d1-c2928d3f5485"
                }
            ]
        },
        {
            "id": "f0c4c221-c222-4e59-a452-1d6534a8f2fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e2c54c5-7d41-42dc-8052-ef1f28799290",
            "compositeImage": {
                "id": "bc34ade2-b8c0-4c67-ac2e-17a1b5b68ebf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c4c221-c222-4e59-a452-1d6534a8f2fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24d9c316-555a-4ee6-8bb6-551d893e1f35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c4c221-c222-4e59-a452-1d6534a8f2fc",
                    "LayerId": "f1c060c6-79d5-4a94-82d1-c2928d3f5485"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "f1c060c6-79d5-4a94-82d1-c2928d3f5485",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e2c54c5-7d41-42dc-8052-ef1f28799290",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 19
}