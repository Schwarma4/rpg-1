
{
    "name": "r_sword",
    "id": "239146c9-5836-4452-a930-4134d826601e",
    "creationCodeFile": "",
    "inheritCode": true,
    "inheritCreationOrder": true,
    "inheritLayers": true,
    "instanceCreationOrderIDs": [
        "9f73cfad-6eeb-4943-9168-93538f0b66cb",
        "d9863213-f200-4180-a714-e03f6279c09a",
        "44360146-0960-4368-802b-7836d3ee0364",
        "de047900-f439-427f-9d03-597ec351fec3",
        "c2817f32-9f18-4ce1-9289-5431e7220f9d",
        "dbc3997a-000c-4c1e-b5c8-1e599aa613fa",
        "b4c22b39-5b6d-4cb3-9d2f-30f39e1ab907",
        "7423d5f5-0bd9-4f0e-8654-92adecf0b8c3",
        "4af0378e-e3c3-4279-8f1a-d6ce2421f4a4"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Text",
            "id": "a58a3685-0df5-427c-a69b-58258cd515f2",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "b36c5421-e5a5-45b1-a5cb-845d21e31145",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Effects",
            "id": "329c8f38-60b8-4c02-8946-db6e6128e8dd",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": false,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": false,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "94a0e9a8-2b33-44c4-a033-25c382d41a02",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": false
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "404b0347-fa12-4158-a230-1957e77b5e5e",
            "depth": 200,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "instances": [
{"name": "inst_41D8CDBF","id": "9f73cfad-6eeb-4943-9168-93538f0b66cb","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_41D8CDBF","objId": "8b6eac83-532b-4339-b7ae-34b6c460fbbd","properties": null,"rotation": 0,"scaleX": 3,"scaleY": 8,"mvc": "1.0","x": -16,"y": 0},
{"name": "inst_B58FF66","id": "d9863213-f200-4180-a714-e03f6279c09a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_B58FF66","objId": "8b6eac83-532b-4339-b7ae-34b6c460fbbd","properties": null,"rotation": 0,"scaleX": 18,"scaleY": 2,"mvc": "1.0","x": 32,"y": 0},
{"name": "inst_28A85D5C","id": "44360146-0960-4368-802b-7836d3ee0364","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_28A85D5C","objId": "8b6eac83-532b-4339-b7ae-34b6c460fbbd","properties": null,"rotation": 0,"scaleX": 2,"scaleY": 9,"mvc": "1.0","x": 288,"y": 32},
{"name": "inst_5B8E632F","id": "de047900-f439-427f-9d03-597ec351fec3","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": true,"inheritItemSettings": true,"IsDnD": false,"m_originalParentID": "f5ef4366-4c2a-4a1e-a91f-57ad85923ffd","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5B8E632F","objId": "26c2eb1d-5d3b-4ab2-837a-3db03ef7d499","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 16,"y": -48},
{"name": "inst_361EA6BE","id": "c2817f32-9f18-4ce1-9289-5431e7220f9d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_361EA6BE","objId": "8b6eac83-532b-4339-b7ae-34b6c460fbbd","properties": null,"rotation": 0,"scaleX": 19,"scaleY": 1,"mvc": "1.0","x": -16,"y": 160},
{"name": "i_world_to_sword","id": "dbc3997a-000c-4c1e-b5c8-1e599aa613fa","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "i_world_to_sword","objId": "e603065d-f642-468f-8c9c-5b8acd60758e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 0,"y": 144},
{"name": "inst_41CD88CB","id": "b4c22b39-5b6d-4cb3-9d2f-30f39e1ab907","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_41CD88CB.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_41CD88CB","objId": "139322a5-2ec5-4c61-a3a9-78e4162ec82f","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 3,"mvc": "1.0","x": -16,"y": 144},
{"name": "inst_23F79AFC","id": "7423d5f5-0bd9-4f0e-8654-92adecf0b8c3","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_23F79AFC","objId": "0e39545d-60a5-47ba-99bb-b353f59197c8","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 160,"y": 80},
{"name": "inst_9DC5BCE","id": "4af0378e-e3c3-4279-8f1a-d6ce2421f4a4","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_9DC5BCE","objId": "0e39545d-60a5-47ba-99bb-b353f59197c8","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 208,"y": 96}
            ],
            "layers": [

            ],
            "m_parentID": "84a4ddba-4db5-4c78-9101-9836d9ae6e57",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Dirtwalls",
            "id": "690298f1-f389-4194-ab91-687a96919319",
            "depth": 300,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "layers": [

            ],
            "m_parentID": "42b9e2d2-695c-4119-97fc-cf728992fd61",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 32,
            "prev_tilewidth": 32,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 6,
                "SerialiseWidth": 10,
                "TileSerialiseData": [
                    8,4,4,4,4,4,4,4,4,13,
                    6,1,1,1,1,1,1,1,1,12,
                    6,1,1,1,1,1,1,1,1,12,
                    2,1,1,1,1,1,1,1,1,12,
                    1,1,1,1,1,1,1,1,1,12,
                    14,14,14,14,14,14,14,14,14,16
                ]
            },
            "tilesetId": "78b7a7f0-ef19-49c0-8d40-361b55e0325e",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Dirtpaths",
            "id": "644ecf4a-a14b-4b5c-934e-50ed9ca949a1",
            "depth": 400,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "layers": [

            ],
            "m_parentID": "9d081902-c305-442f-94d2-1baa96f39ecf",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 16,
            "prev_tilewidth": 16,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 12,
                "SerialiseWidth": 20,
                "TileSerialiseData": [
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483663,2147483648,2147483648,17,17,17,17,17,17,17,2147483648,2147483648,2147483648,2147483665,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,17,17,17,17,17,17,17,17,2147483648,2147483648,2147483648,2147483665,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483665,17,17,17,17,17,17,17,17,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,17,17,17,17,17,17,17,17,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483665,2147483648,2147483665,2147483665,2147483665,2147483648
                ]
            },
            "tilesetId": "d1ab8c37-b579-4a03-9b91-594b54500a0e",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "7c6842ae-9ad2-44f7-a37a-5df48dd912cf",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 500,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "layers": [

            ],
            "m_parentID": "d4bdf19e-a402-4fc9-b18c-6845c3ba6bf1",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "ebb871f6-9316-4b78-97dc-dfa79b0ed826",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": true,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "137c8519-7cfe-4e50-8ad2-ed9bd87e85b0",
    "physicsSettings":     {
        "id": "a824a67b-03df-4abd-9c45-6038a8fab50a",
        "inheritPhysicsSettings": true,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "96714330-8248-49da-91f9-a0927b2e4a77",
        "Height": 180,
        "inheritRoomSettings": true,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 320
    },
    "mvc": "1.0",
    "views": [
{"id": "06033cb2-1b73-4079-8b79-d170af01676e","hborder": 32,"hport": 720,"hspeed": -1,"hview": 180,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 1280,"wview": 320,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "58752f08-52a1-49ae-a05c-d879a49b27c2","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "b0530b37-047c-4609-a09b-9bf7fbdc4f88","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "435e455c-914f-44c2-9b63-1fc4771ed347","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "1ed81fa5-3c60-4ea3-a7fc-340a6c608748","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "b758bb4c-0f46-4943-a13e-76f7e85495d2","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ea3fdc52-1882-447f-956a-2a314b347d53","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "a518eaa0-0d67-4c0c-b2c0-262975282778","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "d3e1db35-8066-4132-a72a-ec4fc2a76a7a",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": true,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}