event_inherited();
myTextbox_ = noone;
MyName_ = "Player 2";
myText_[0] = "HELP ME !!!"


//Sprite move lookup table
sprite_[player.move , dir.right] = s_fly_run_right;
sprite_[player.move , dir.left] = s_fly_run_right;
sprite_[player.move , dir.up] = s_fly_run_UP;
sprite_[player.move , dir.down] = s_fly_run_down;

//Sprite attack lookup table
sprite_[player.sword , dir.right] = s_player_attack_right;
sprite_[player.sword , dir.left] = s_player_attack_right;
sprite_[player.sword , dir.up] = s_player_attack_up;
sprite_[player.sword , dir.down] = s_player_attack_down;

//Sprite evade lookup table
sprite_[player.evade , dir.right] = s_player_roll_right;
sprite_[player.evade , dir.left] = s_player_roll_right;
sprite_[player.evade , dir.up] = s_player_roll_up;
sprite_[player.evade , dir.down] = s_player_roll_down;

//Sprite hit lookup table
sprite_[player.hit , dir.right] = s_lizard_billy;
sprite_[player.hit , dir.left] = s_lizard_billy;
sprite_[player.hit , dir.up] = s_lizard_billy;
sprite_[player.hit , dir.down] = s_lizard_billy;


//Sprite bomb lookup table
sprite_[player.bomb , dir.right] = s_lizard_billy;
sprite_[player.bomb , dir.left] = s_lizard_billy;
sprite_[player.bomb , dir.up] = s_lizard_billy;
sprite_[player.bomb , dir.down] = s_lizard_billy;


//Sprite attack lookup table
sprite_[player.melee , dir.right] = s_fly_attack_right;
sprite_[player.melee , dir.left] = s_fly_attack_right;
sprite_[player.melee , dir.up] = s_player_attack_up;
sprite_[player.melee , dir.down] = s_fly_attack_down;
//s_player_two_pause.sprite_index = s_fly_pause;