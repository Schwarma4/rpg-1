{
    "id": "219b3984-40d9-48c1-a561-97ddfe40d36d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_start_position",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4a777de-62a5-4e78-859f-942333be0d23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "219b3984-40d9-48c1-a561-97ddfe40d36d",
            "compositeImage": {
                "id": "62484e49-d804-43df-963a-45fff36a1076",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a777de-62a5-4e78-859f-942333be0d23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60c96475-0576-4389-8bce-582c82c0c9ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a777de-62a5-4e78-859f-942333be0d23",
                    "LayerId": "2c6806e7-3e67-49b4-b7c6-eabeb87e36e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2c6806e7-3e67-49b4-b7c6-eabeb87e36e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "219b3984-40d9-48c1-a561-97ddfe40d36d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}