{
    "id": "fb0c31dd-584e-456b-b3e1-c2f36c6f80c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_one_pause",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d275eb39-db8b-47d3-b117-9498006822e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb0c31dd-584e-456b-b3e1-c2f36c6f80c7",
            "compositeImage": {
                "id": "c53c79c7-cba8-4182-bcd6-b9293141828c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d275eb39-db8b-47d3-b117-9498006822e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "039a50cf-b8bf-4829-8591-dac8ba8b8911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d275eb39-db8b-47d3-b117-9498006822e2",
                    "LayerId": "5167a1d8-4382-438b-8e0f-94c3be6534fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5167a1d8-4382-438b-8e0f-94c3be6534fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb0c31dd-584e-456b-b3e1-c2f36c6f80c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}