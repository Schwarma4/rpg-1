/// @arg item
/// @arg _inventory_number

var _item = singleton(argument0);
var _inventory_number  = argument1;

if _inventory_number == 1 {
	var _item_index = array_find_index(_item, global.inventory_one);

	if _item_index == -1 {
		var _i = 0;
		var _array_size = array_length_1d(global.inventory_one);
		for (var _i = 0; _i < _array_size; _i++) {
			if global.inventory_one[_i] == noone {
				global.inventory_one[_i] = _item;
				return true;
			}
		}
	} else {
		return true;
	}
} else if _inventory_number == 2 {
	var _item_index = array_find_index(_item, global.inventory_two);

	if _item_index == -1 {
		var _i = 0;
		var _array_size = array_length_1d(global.inventory_two);
		for (var _i = 0; _i < _array_size; _i++) {
			if global.inventory_two[_i] == noone {
				global.inventory_two[_i] = _item;
				return true;
			}
		}
	} else {
		return true;
	}
}

return false;