{
    "id": "8974e42d-77bc-43dd-8722-f3d03f5b5508",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "no",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d589733-2fc0-4472-9cf3-f188655f2668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8974e42d-77bc-43dd-8722-f3d03f5b5508",
            "compositeImage": {
                "id": "d92ebf62-8c8e-40b7-9ab6-9fb150ae10e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d589733-2fc0-4472-9cf3-f188655f2668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20619c68-c571-4634-bd06-ff6df93ef476",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d589733-2fc0-4472-9cf3-f188655f2668",
                    "LayerId": "141291a6-bbd8-4105-939f-f7973544b252"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "141291a6-bbd8-4105-939f-f7973544b252",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8974e42d-77bc-43dd-8722-f3d03f5b5508",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": -4,
    "yorig": 10
}