{
    "id": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_font",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e84fb66a-3bb8-4026-927a-a7e9dbde9061",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "b844efec-6edb-475f-b24b-46926eeeb621",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e84fb66a-3bb8-4026-927a-a7e9dbde9061",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19693c5f-1406-493b-aff1-98e62e6f777a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84fb66a-3bb8-4026-927a-a7e9dbde9061",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "8c38e766-f985-4fe0-b0a2-43b222f2e3e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "98578c5d-635f-4cae-be34-ce19821c2897",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c38e766-f985-4fe0-b0a2-43b222f2e3e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2cc45b4-062c-4379-93fe-6d815822dd10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c38e766-f985-4fe0-b0a2-43b222f2e3e6",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "04cc469f-9a2c-4020-974c-4c3ff1cb0cb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "4b75d5ff-b0f5-4161-8159-0b7d481337b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04cc469f-9a2c-4020-974c-4c3ff1cb0cb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18786a47-9e71-40f3-bb81-25ef7ec02096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04cc469f-9a2c-4020-974c-4c3ff1cb0cb3",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "caadb7ea-c15a-4d6e-86f5-4defc45ff495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "344ecccf-6192-4b8d-9aad-22f47bfc22d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caadb7ea-c15a-4d6e-86f5-4defc45ff495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d7c426c-daaf-479e-a38a-73fca9253f41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caadb7ea-c15a-4d6e-86f5-4defc45ff495",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "a0de7b26-dcaa-4959-8466-616a875731ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "67880c45-f4a8-4fa0-b31c-77eadb3ce34b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0de7b26-dcaa-4959-8466-616a875731ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b5fc772-23fa-4ad7-90f0-d0f0518da35e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0de7b26-dcaa-4959-8466-616a875731ec",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "6b06ceaa-8591-46ea-a1d9-6b590d21c16a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "03fe176b-38e1-4658-a77b-38b5e647a59e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b06ceaa-8591-46ea-a1d9-6b590d21c16a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b599163c-9709-4e69-8039-442cb1c86e32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b06ceaa-8591-46ea-a1d9-6b590d21c16a",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "a718592f-292c-4c13-97d4-8375185693aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "615aedca-5362-4a5f-9d3a-7b20f6f52829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a718592f-292c-4c13-97d4-8375185693aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a3d5a44-8c13-4890-8b76-773e67e7a94d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a718592f-292c-4c13-97d4-8375185693aa",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "b5a6d889-d1d9-4c3a-9933-adedc28b9cae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "c8f49ced-5295-438d-b81d-78aa1f412e22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5a6d889-d1d9-4c3a-9933-adedc28b9cae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8a9368f-7a58-43d6-8bf4-bc52bbd760f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5a6d889-d1d9-4c3a-9933-adedc28b9cae",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "64617553-3d40-4943-adae-9842ab8cbb8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "17700349-be1d-4a03-b34a-ba4e4b1713e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64617553-3d40-4943-adae-9842ab8cbb8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "899c0766-8e75-40ac-8914-73440e85792e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64617553-3d40-4943-adae-9842ab8cbb8c",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "5964ecfb-c378-409f-9056-256d928a8bbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "41e54214-b5dc-466d-bfbf-b0e9c99c14a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5964ecfb-c378-409f-9056-256d928a8bbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8d2fae1-97c7-499a-95de-88c650124032",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5964ecfb-c378-409f-9056-256d928a8bbe",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "e59b653b-f6ca-43af-bbf5-626d04273868",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "112da894-ee86-4600-a13f-c33a31b678c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e59b653b-f6ca-43af-bbf5-626d04273868",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f858c1f7-56d3-4f94-afa3-501fbce9065c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e59b653b-f6ca-43af-bbf5-626d04273868",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "88be396c-6d73-4779-87e7-cf6661f388e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "bf4614a5-7bba-4d03-9fe0-eb4bc2ee80fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88be396c-6d73-4779-87e7-cf6661f388e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d129581-054c-4122-afd4-31eb47233919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88be396c-6d73-4779-87e7-cf6661f388e0",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "3a0bb32d-a641-4e8e-aeba-7e698056ef0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "9a937cf9-229a-4c32-b60d-e4271b6d381a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a0bb32d-a641-4e8e-aeba-7e698056ef0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2ece373-09a2-47ff-9b03-aa3f9f93bacf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a0bb32d-a641-4e8e-aeba-7e698056ef0c",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "a065e724-d71c-4e1c-986c-3486ba6777bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "7596318e-6d4d-40c3-9916-e8216602acdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a065e724-d71c-4e1c-986c-3486ba6777bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cfd02e0-3e0c-4730-9d54-e96cddc455a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a065e724-d71c-4e1c-986c-3486ba6777bf",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "997a76c5-3d3b-40ad-ab81-00ca5891a5c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "46126dfd-57bb-43e7-8fde-1093bf4ee4e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "997a76c5-3d3b-40ad-ab81-00ca5891a5c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8476c14a-570e-43d4-8b03-64cf227bbe3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "997a76c5-3d3b-40ad-ab81-00ca5891a5c1",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "cf00f9b3-9a02-47b2-a5e7-75a3af1c752c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "07b4e0be-db40-40d0-a5a1-4d0b43371351",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf00f9b3-9a02-47b2-a5e7-75a3af1c752c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5948de93-91cc-4eae-90cb-204cddd579ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf00f9b3-9a02-47b2-a5e7-75a3af1c752c",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "33ffa99b-fbe8-4718-a9c6-91ab41f72d26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "5493b0db-2abc-4f5f-a9e4-804b80c4984f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33ffa99b-fbe8-4718-a9c6-91ab41f72d26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8519585-3d55-490f-a9de-0a5df0f5516a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33ffa99b-fbe8-4718-a9c6-91ab41f72d26",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "7ffbe3b1-594a-4ac9-8360-c2ad60b82654",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "1bdbe266-2bc8-4062-b08f-b7720a292c77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ffbe3b1-594a-4ac9-8360-c2ad60b82654",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "942ac30b-f485-4c67-b40a-c0f8701a9415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ffbe3b1-594a-4ac9-8360-c2ad60b82654",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "760c4449-12ef-4ee0-84ba-cc3767d63a11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "7e269b3c-109c-4536-acc3-f5cfaeed9f45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "760c4449-12ef-4ee0-84ba-cc3767d63a11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0700a069-69a3-47fe-a8da-40aaf3596c4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "760c4449-12ef-4ee0-84ba-cc3767d63a11",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "1217ff81-1e8e-43f7-b7de-4d2e9d5c7b47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "22641c30-019c-49c1-8dcd-ccea1a3457e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1217ff81-1e8e-43f7-b7de-4d2e9d5c7b47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c64c8f66-480e-4c0e-91a0-a39b07cfa42f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1217ff81-1e8e-43f7-b7de-4d2e9d5c7b47",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "560e1e92-d260-49c6-b33e-44e009e4d3e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "83e0b366-1ad3-4e7b-8353-a8d781b6e14c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "560e1e92-d260-49c6-b33e-44e009e4d3e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c91424c-c432-43e3-a978-465a182ed68e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "560e1e92-d260-49c6-b33e-44e009e4d3e5",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "89d3070b-2604-4dc7-b9af-8c7e4d02b369",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "955c5422-2ce0-46b6-a660-631337f6aa6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89d3070b-2604-4dc7-b9af-8c7e4d02b369",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a4ca578-fb05-4379-b066-6c8e9489ffc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89d3070b-2604-4dc7-b9af-8c7e4d02b369",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "cfba675e-220a-49cd-952d-91270e17a249",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "ae7d932c-6ea0-4f82-a146-88a22ecb4a4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfba675e-220a-49cd-952d-91270e17a249",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feedd193-e31a-40ba-9355-21d07e31ef44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfba675e-220a-49cd-952d-91270e17a249",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "4ae3e863-34bb-4e6c-8144-5d979e7b194e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "7689bbe3-73eb-4c7a-a72b-fd583d41168b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ae3e863-34bb-4e6c-8144-5d979e7b194e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25b2955f-9210-42c3-a531-f4c4be7f6429",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ae3e863-34bb-4e6c-8144-5d979e7b194e",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "9e2e46e4-7494-4081-ab43-f15d8b2dc6ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "8c3a984f-cb03-4c51-b028-447b31913d43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e2e46e4-7494-4081-ab43-f15d8b2dc6ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83ab9ed1-11bc-4832-9984-47c322d46daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e2e46e4-7494-4081-ab43-f15d8b2dc6ee",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "397bf67d-8053-43d7-9507-9c8ab5e811d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "cff5c37c-ae2e-4560-9edb-b1ae4642af25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "397bf67d-8053-43d7-9507-9c8ab5e811d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7be1e23-1aa4-44f3-857b-bb21d4e57e49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "397bf67d-8053-43d7-9507-9c8ab5e811d6",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "d9ccb1b5-873d-47cd-a959-cf38ecbab4dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "fa7b69fe-a6b3-4e1e-a86e-b7c7ab835ea9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9ccb1b5-873d-47cd-a959-cf38ecbab4dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e5b594f-9d50-48cb-809e-1564e132ee09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9ccb1b5-873d-47cd-a959-cf38ecbab4dc",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "e00baf99-f39b-41b3-8308-dc0eec603313",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "2c63c61b-c651-473b-8619-189eaff994bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e00baf99-f39b-41b3-8308-dc0eec603313",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18ea600b-03be-4443-a365-5c4fa6bd86ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e00baf99-f39b-41b3-8308-dc0eec603313",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "c5e63a45-857f-4b0d-88f0-8734acfafc03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "b3af3f0e-8488-4b86-afb5-8bb56184ebeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5e63a45-857f-4b0d-88f0-8734acfafc03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d791bc6a-d879-4113-9c3e-d81b50d6b95b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5e63a45-857f-4b0d-88f0-8734acfafc03",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "f45f7f5f-cbcf-43cd-a12a-7fdab4f4830b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "dd89cbfa-513b-4d5e-aaf7-f3fdea022c24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f45f7f5f-cbcf-43cd-a12a-7fdab4f4830b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cbffe51-0efc-4f00-b2d8-4cb49596131a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f45f7f5f-cbcf-43cd-a12a-7fdab4f4830b",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "8fc01ad9-2e0f-48ad-9a2f-6c243b47e9f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "b30ea1e0-b249-4321-a0c5-35ff99c15469",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fc01ad9-2e0f-48ad-9a2f-6c243b47e9f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26d7e2b3-c0bd-4dcc-b702-9f7b6c765948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fc01ad9-2e0f-48ad-9a2f-6c243b47e9f8",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "254da33b-f4d4-4171-bbaa-5eb4594918f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "b24a34ed-29a1-4881-8f79-70bfb48c3fa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "254da33b-f4d4-4171-bbaa-5eb4594918f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac0bfbda-7503-431f-9bd0-aa448a651e3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "254da33b-f4d4-4171-bbaa-5eb4594918f5",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "24d87672-1b8a-4770-94ab-403a0b6975ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "4b15acc2-479b-4429-b0bc-797be16a9024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24d87672-1b8a-4770-94ab-403a0b6975ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c6872b5-dc7a-41a3-b818-2de623090585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24d87672-1b8a-4770-94ab-403a0b6975ea",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "332971e3-0a4d-4084-841b-22c8445d1abf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "5fac82e6-192f-4c3a-b670-211fd7e72c9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "332971e3-0a4d-4084-841b-22c8445d1abf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e191d5a-7f84-4831-8159-389c57d2b907",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "332971e3-0a4d-4084-841b-22c8445d1abf",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "33414b56-e3a9-4c2c-a8a9-c7a77b0c8e70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "12733751-490d-4983-b702-49be60852738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33414b56-e3a9-4c2c-a8a9-c7a77b0c8e70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d1ac896-fb88-4f4d-96ae-365785c0fcc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33414b56-e3a9-4c2c-a8a9-c7a77b0c8e70",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "0f027ac1-be78-4e4a-91f7-beaccb465fd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "302ca3ad-32c6-4a8f-95ac-abbdeacfd867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f027ac1-be78-4e4a-91f7-beaccb465fd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9f33803-9f8d-4feb-a130-cf7646992e93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f027ac1-be78-4e4a-91f7-beaccb465fd8",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "a143e5bf-39c9-4aaa-837f-afa0fb82b1ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "4f64f355-2b23-48fa-98b3-82ecd0f8266d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a143e5bf-39c9-4aaa-837f-afa0fb82b1ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f26938be-1d45-4e6e-9e11-947fa2896e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a143e5bf-39c9-4aaa-837f-afa0fb82b1ba",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "385f709b-1154-440e-be84-ac37349d6aca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "aa7ce7a0-c8a6-464c-b175-a2a4f90c5961",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "385f709b-1154-440e-be84-ac37349d6aca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "647fc3d3-5084-473a-84f8-837af0aef1b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "385f709b-1154-440e-be84-ac37349d6aca",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "2d92f7be-c656-4493-9b8a-7c00464020b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "e621968b-5b99-400f-adf1-31011b0bb4d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d92f7be-c656-4493-9b8a-7c00464020b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcaf69c5-b36d-4f4d-93bd-8e1d05e4c486",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d92f7be-c656-4493-9b8a-7c00464020b6",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "ef3b5b0a-510b-4a86-a50b-97ec359ef78d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "97113f93-4f90-44a0-bdd5-db6050122c98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef3b5b0a-510b-4a86-a50b-97ec359ef78d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3266e971-9d01-4a3a-ae96-ae3c9a9e1b1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef3b5b0a-510b-4a86-a50b-97ec359ef78d",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "1bc61bb6-96c4-4445-b113-190fd544adeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "68c1d9b9-cfb3-4270-95ff-22b5d39f54ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bc61bb6-96c4-4445-b113-190fd544adeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9c5a2ec-83e4-4540-87b6-2ecff1f99007",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bc61bb6-96c4-4445-b113-190fd544adeb",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "647f0fe5-8cae-40eb-bb16-0384da45fa3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "26b1df77-8286-4f84-980a-d822db45c9e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647f0fe5-8cae-40eb-bb16-0384da45fa3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c385c844-2af0-4a53-b023-436545620345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647f0fe5-8cae-40eb-bb16-0384da45fa3e",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "0ed45c99-ddde-44f8-93cd-61b40b3d847d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "18cf69c3-0bc2-4927-8805-d0b2aac8152b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ed45c99-ddde-44f8-93cd-61b40b3d847d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7091569e-a4fb-43c7-9560-6d9420068d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ed45c99-ddde-44f8-93cd-61b40b3d847d",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "31fb1b7b-d570-4db7-a08c-6eabfae797a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "0f008ce8-b071-4b74-a78f-3e3c8c7320c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31fb1b7b-d570-4db7-a08c-6eabfae797a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c54f6a16-4988-4b69-8c3d-aa2900dc15f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31fb1b7b-d570-4db7-a08c-6eabfae797a7",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "ee1b76ea-b58f-4d28-94ea-898d6f9b281d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "95a947bc-55d4-4d5a-9860-eae2131c1551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee1b76ea-b58f-4d28-94ea-898d6f9b281d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fd157d2-54dd-441c-a87b-caa2bf45511f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee1b76ea-b58f-4d28-94ea-898d6f9b281d",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "c432bd18-a415-4fb0-881e-ac368c4bc49b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "370347ac-2990-4474-8608-fd540d5c9e46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c432bd18-a415-4fb0-881e-ac368c4bc49b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fa01040-10ee-40c2-b2e0-4700ed09b06a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c432bd18-a415-4fb0-881e-ac368c4bc49b",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "f523b383-b710-4f64-aaaa-b7db305262a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "f9c39747-5654-4433-8de5-86ead34743e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f523b383-b710-4f64-aaaa-b7db305262a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d720f0e-5fbf-4ad6-ab0a-88c4a56267d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f523b383-b710-4f64-aaaa-b7db305262a0",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "64347868-c178-4c06-a9d6-688898a7aaff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "465b8501-32e4-4f23-b66d-fd293205cf7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64347868-c178-4c06-a9d6-688898a7aaff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5762dc07-e39e-47e7-a754-2f4000795300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64347868-c178-4c06-a9d6-688898a7aaff",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "3e97993b-27fc-405f-b6d8-312eb8445a65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "2387ae8b-ace1-460e-ac84-a8db7a6590a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e97993b-27fc-405f-b6d8-312eb8445a65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a83aea57-8334-4415-9322-d3bc7843fa5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e97993b-27fc-405f-b6d8-312eb8445a65",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "f46ec120-f110-4514-b5f8-d6282c5c24ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "0f3930de-dd58-4092-99ee-95c8001efa4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f46ec120-f110-4514-b5f8-d6282c5c24ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08718a0e-8cd7-4b45-8820-59ca60eaa765",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f46ec120-f110-4514-b5f8-d6282c5c24ee",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "bf168e64-d8ed-4941-8575-daace5bfd7e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "e1a1cffa-4c2d-4251-813c-b3677a6fd837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf168e64-d8ed-4941-8575-daace5bfd7e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "934fed62-3062-4193-9a0b-1b1c8815dde0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf168e64-d8ed-4941-8575-daace5bfd7e6",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "00995692-4827-4d18-ba61-3cfe9d823a85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "c9c1fb6b-74fa-48a9-a867-e783ca25b8a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00995692-4827-4d18-ba61-3cfe9d823a85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8739b8d0-7438-4f61-9be5-92557fa10404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00995692-4827-4d18-ba61-3cfe9d823a85",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "1610e87a-ddd8-49e7-8004-09484a1d58c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "63b5720c-ce71-4f7f-bf3a-31641faa410e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1610e87a-ddd8-49e7-8004-09484a1d58c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6332deb5-0eff-4529-b921-c7227adeeaa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1610e87a-ddd8-49e7-8004-09484a1d58c1",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "ba6d49fe-2103-4f67-bcb2-c10164703ce2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "baf2bd8a-5cf8-460b-b181-f7f04b927f63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba6d49fe-2103-4f67-bcb2-c10164703ce2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1bce8c0-1ffa-4bb7-bbff-3b4910fde508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba6d49fe-2103-4f67-bcb2-c10164703ce2",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "cd737efd-d62f-494c-8c2d-f7126d33f7cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "847772b7-57a4-45cf-9493-2ff7e43f626c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd737efd-d62f-494c-8c2d-f7126d33f7cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fface93b-24a5-4eb6-a1f0-27cc2e8f4807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd737efd-d62f-494c-8c2d-f7126d33f7cb",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "d3b337bd-5ef1-4eec-982c-6b4c52552e82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "fac784c9-5e81-4210-87ff-394c48e674b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3b337bd-5ef1-4eec-982c-6b4c52552e82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd13b816-d93d-409f-89c6-1d6b6e0062fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3b337bd-5ef1-4eec-982c-6b4c52552e82",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "75c87799-5fe3-475f-8e85-da937142e015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "7c1dea56-45e7-4deb-86bc-105071f30859",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75c87799-5fe3-475f-8e85-da937142e015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b500db49-dc01-4afd-90c9-16492adef73c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75c87799-5fe3-475f-8e85-da937142e015",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "ba73a1ad-d64a-43a1-9982-fe6fb18f4a51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "00a082dd-e2b7-4175-a427-a1f4bd127a3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba73a1ad-d64a-43a1-9982-fe6fb18f4a51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65f68f21-8f38-4325-841d-743eea2ce363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba73a1ad-d64a-43a1-9982-fe6fb18f4a51",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "c1a3ae72-86b2-4f20-a54c-5a914a1400f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "5bf50243-5d98-46e6-a64e-f7397878dc12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1a3ae72-86b2-4f20-a54c-5a914a1400f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "793455a1-93fe-4aa0-9a6b-76e65032276f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1a3ae72-86b2-4f20-a54c-5a914a1400f0",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "9d5e9550-e026-4f89-920c-fb4d15671eb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "c4deaf66-e32f-49c0-873a-763f994c2038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d5e9550-e026-4f89-920c-fb4d15671eb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19cb8639-e8cf-47f9-a816-6723523a113a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d5e9550-e026-4f89-920c-fb4d15671eb8",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "70074e28-054d-4ec7-a1b0-8a7091e952bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "95cd6533-e03c-40ab-a0e4-afdd74d0fcb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70074e28-054d-4ec7-a1b0-8a7091e952bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01ff5225-6133-4033-a2c7-f4a3547091d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70074e28-054d-4ec7-a1b0-8a7091e952bf",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "7361f2b7-acd6-44c8-ab97-88dabf0059c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "d31c774c-200e-44d0-880d-ad7e92f765d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7361f2b7-acd6-44c8-ab97-88dabf0059c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6107e3c3-a7e1-470f-98e1-7a57e8036d2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7361f2b7-acd6-44c8-ab97-88dabf0059c4",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "a051bfee-935c-4056-818a-e2fb5a00f145",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "88c28177-0194-448a-abdf-0f5ef3610273",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a051bfee-935c-4056-818a-e2fb5a00f145",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12cf189f-ea4c-471c-8e78-8576b05ded1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a051bfee-935c-4056-818a-e2fb5a00f145",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "99868a52-a06c-49d4-82b2-a8055665a265",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "b983cdcf-8168-462e-895e-cbc1e6c596c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99868a52-a06c-49d4-82b2-a8055665a265",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d56d3861-6891-4dd3-b73d-c86cd78f096f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99868a52-a06c-49d4-82b2-a8055665a265",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "3852977c-f734-4700-9687-106838df12d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "eeffe84f-afd9-4ead-a536-cdd46431eacd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3852977c-f734-4700-9687-106838df12d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3faaafc-b7d6-4538-bc9c-7124adacc986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3852977c-f734-4700-9687-106838df12d5",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "f3e4fdd8-538a-4ba7-97de-023a0d51de80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "9284a5ce-733b-4c02-ba69-5413fc55d8b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3e4fdd8-538a-4ba7-97de-023a0d51de80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1644159-fa80-4018-811e-ca715a567a2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3e4fdd8-538a-4ba7-97de-023a0d51de80",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "78fc1547-b4b8-4fe9-8b80-b34422b1c2f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "70fb5c19-9840-4935-968f-71bc071673f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78fc1547-b4b8-4fe9-8b80-b34422b1c2f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a0fcf49-b3af-4efe-a231-5861d6223746",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78fc1547-b4b8-4fe9-8b80-b34422b1c2f2",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "e8646015-2df1-471e-aeaa-7e31dc26e102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "5d403acf-4111-408c-96ba-e1f737b9c1aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8646015-2df1-471e-aeaa-7e31dc26e102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b75aa25-8d82-417e-a2e3-e49b9eb4ba07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8646015-2df1-471e-aeaa-7e31dc26e102",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "b20ee4ab-727f-4120-b0ef-47610ba9c111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "229ce054-ee88-4d63-a2cf-6667de0b4ecd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b20ee4ab-727f-4120-b0ef-47610ba9c111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0e54ab1-756c-43bd-82dc-ab680f9a27bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b20ee4ab-727f-4120-b0ef-47610ba9c111",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        },
        {
            "id": "870ba250-1ee9-43ff-b617-3b2b4c0a1242",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "compositeImage": {
                "id": "18ccab19-2201-4b45-af9f-312955ba3eb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "870ba250-1ee9-43ff-b617-3b2b4c0a1242",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e678881d-c387-43a2-9d76-209716c3f5f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "870ba250-1ee9-43ff-b617-3b2b4c0a1242",
                    "LayerId": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "ad6e3636-b5ff-4922-bf7c-e7105dc032fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81c4ba0f-246a-4bac-8c17-6b1632efe36b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}