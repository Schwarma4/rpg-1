{
    "id": "eecaa607-fed2-4579-ad1e-83dc778338bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_two_pause",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b85845c-1b08-4e27-bafd-0a8eb866860a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eecaa607-fed2-4579-ad1e-83dc778338bb",
            "compositeImage": {
                "id": "81ec4831-42b8-42af-a126-7fa6d24dba53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b85845c-1b08-4e27-bafd-0a8eb866860a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "143b63c8-6349-4cec-99a8-922fde3a8800",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b85845c-1b08-4e27-bafd-0a8eb866860a",
                    "LayerId": "4b7e0e75-25cc-4da9-8e0e-460a25b5e904"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4b7e0e75-25cc-4da9-8e0e-460a25b5e904",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eecaa607-fed2-4579-ad1e-83dc778338bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}