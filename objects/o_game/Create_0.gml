global.one_second = game_get_speed(gamespeed_fps);
global.destroyed = [];
instance_create_layer(0, 0, "Instances", o_input);

var _font_string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ.abcdefghijklmnopqrstuvwxyz1234567890>,!':-+";
global.font = font_add_sprite_ext(s_font,_font_string,true, 1);
draw_set_font(global.font);

global.player_one_max_health = 3;
global.player_one_health = global.player_one_max_health;
global.player_one_max_stamina = 3;
global.player_one_stamina = global.player_one_max_stamina;
global.player_two_max_health = 3;
global.player_two_health = global.player_two_max_health;
global.player_two_max_stamina = 3;
global.player_two_stamina = global.player_two_max_stamina;
global.player_gems = 0;
global.player_start_position = i_game_start;
global.start_x = noone;
global.start_y = noone;
global.load = false;
global.current_player = 1;
global.pause_screen_sprite_one = s_worm_pause;
global.pause_screen_sprite_two = s_fly_pause;
global.follower_state_one = 0;
global.follower_state_two = 0;

//audio_play_sound(a_music, 10, true);
var _view_width = camera_get_view_width(view_camera[0]);
var _view_height = camera_get_view_height(view_camera[0]);
display_set_gui_size(_view_width, _view_height);

paused_ = false;
paused_sprite_ = noone;
paused_sprite_scale_ = display_get_gui_width()/view_wport[0];

item_index_ = 0;
inventory_create(6);

inventory_add_item(o_sword_item, 1);
inventory_add_item(o_ring_item, 2);
//inventory_add_item(o_metal_item, 1);
inventory_add_item(o_melee_item, 2);

global.item[0] = global.inventory_one[0];
global.item[1] = global.inventory_one[1];
global.item[2] = global.inventory_two[0];	
global.item[3] = global.inventory_two[1];