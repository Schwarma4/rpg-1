{
    "id": "7ffd4590-a503-44aa-bddb-8d8285e20529",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_small_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bf30ea9-a3c6-48aa-af1f-d4d860544993",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ffd4590-a503-44aa-bddb-8d8285e20529",
            "compositeImage": {
                "id": "1553d2d4-824e-4758-b748-69957a356cac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bf30ea9-a3c6-48aa-af1f-d4d860544993",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1ef7b1d-8aaa-4c4f-a207-9deb2ba46240",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bf30ea9-a3c6-48aa-af1f-d4d860544993",
                    "LayerId": "a03d9dcb-b195-4961-85e3-25a9678389fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "a03d9dcb-b195-4961-85e3-25a9678389fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ffd4590-a503-44aa-bddb-8d8285e20529",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 4
}