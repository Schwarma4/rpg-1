{
    "id": "cc576aca-e69a-4556-9e76-ddf1b8e9f108",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_sasha_cat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e888b871-38ce-4a8a-85e7-95b2d1052c4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc576aca-e69a-4556-9e76-ddf1b8e9f108",
            "compositeImage": {
                "id": "7b9f2aa9-2a74-42e6-be63-73dd48ed8323",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e888b871-38ce-4a8a-85e7-95b2d1052c4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34c0de49-7b70-4f23-9589-0419a8b3032c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e888b871-38ce-4a8a-85e7-95b2d1052c4b",
                    "LayerId": "186ae4ae-8544-4850-8be1-e1d58e9a6280"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "186ae4ae-8544-4850-8be1-e1d58e9a6280",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc576aca-e69a-4556-9e76-ddf1b8e9f108",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 11,
    "yorig": 20
}