/// @arg x
/// @arg y
/// @arg _inventory_number

if !o_game.paused_ exit;

var _x = argument0;
var _y = argument1;
var _inventory_number  = argument2;

if _inventory_number == 1 {
	var _array_size = array_length_1d(global.inventory_one);

	for (var _i = 0; _i<_array_size; _i++) {
		var _box_x = _x + _i * 32;
		var _box_y = _y;
		draw_sprite(s_inventory_box, 0, _box_x, _box_y);
	
		var _item = global.inventory_one[_i];
		if instance_exists(_item){
			draw_sprite(_item.sprite_, 0, _box_x + 16, _box_y + 16);	
		}
	
		if _i == item_index_ {
			draw_sprite(s_pause_cursor, image_index/8, _box_x, _box_y);	
			if instance_exists(_item){
				draw_text(_x+4, _y+36, _item.description_);
				var _description_height = string_height(_item.description_);
				draw_text(_x+4, _y+48+_description_height, "Stamina cost: " + string(_item.cost_));
			}
		}
	}

	draw_sprite(s_inventory_box, 0, 4, 4);
	draw_sprite(s_inventory_box, 0, 36, 4);
	draw_sprite(s_inventory_box, 0, 68, 4);
	draw_sprite(global.pause_screen_sprite_one, 1, 20, 30);	
	if instance_exists(global.item[0]) { 
		draw_sprite(global.item[0].sprite_, 0, 52, 20);	
	}
	if instance_exists(global.item[1]) { 
		draw_sprite(global.item[1].sprite_, 0, 84, 20);	
	}	
} else {
	var _array_size = array_length_1d(global.inventory_two);

	for (var _i = 0; _i<_array_size; _i++) {
		var _box_x = _x + _i * 32;
		var _box_y = _y;
		draw_sprite(s_inventory_box, 0, _box_x, _box_y);
	
		var _item = global.inventory_two[_i];
		if instance_exists(_item){
			draw_sprite(_item.sprite_, 0, _box_x + 16, _box_y + 16);	
		}
	
		if _i == item_index_ {
			draw_sprite(s_pause_cursor, image_index/8, _box_x, _box_y);	
			if instance_exists(_item){
				draw_text(_x+4, _y+36, _item.description_);
				var _description_height = string_height(_item.description_);
				draw_text(_x+4, _y+48+_description_height, "Stamina cost: " + string(_item.cost_));
			}
		}
	}

	draw_sprite(s_inventory_box, 0, 4, 4);
	draw_sprite(s_inventory_box, 0, 36, 4);
	draw_sprite(s_inventory_box, 0, 68, 4);
	draw_sprite(global.pause_screen_sprite_two, 1, 20, 30);	
	if instance_exists(global.item[2]) { 
		draw_sprite(global.item[2].sprite_, 0, 52, 20);	
	}
	if instance_exists(global.item[3]) { 
		draw_sprite(global.item[3].sprite_, 0, 84, 20);	
	}	
}