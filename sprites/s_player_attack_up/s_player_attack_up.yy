{
    "id": "7ebed1fa-cf56-4a62-9bd8-e87e587ddd9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_attack_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 3,
    "bbox_right": 44,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d39caa9-7616-42d8-948f-3d44703e84ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ebed1fa-cf56-4a62-9bd8-e87e587ddd9f",
            "compositeImage": {
                "id": "5908da45-72ef-4220-ac32-dc2e30b5fed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d39caa9-7616-42d8-948f-3d44703e84ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59c51015-3ca8-4bed-b403-9ae593a80f3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d39caa9-7616-42d8-948f-3d44703e84ad",
                    "LayerId": "0026dd65-1944-4974-b4d8-f5587b0328e7"
                }
            ]
        },
        {
            "id": "c690e3b7-1490-42ff-8a46-b4d42ed56ba9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ebed1fa-cf56-4a62-9bd8-e87e587ddd9f",
            "compositeImage": {
                "id": "27201e4b-a987-4959-95c2-56bbc3f8ef18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c690e3b7-1490-42ff-8a46-b4d42ed56ba9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "482b25b0-8cef-4f8f-b5ce-6a9529126741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c690e3b7-1490-42ff-8a46-b4d42ed56ba9",
                    "LayerId": "0026dd65-1944-4974-b4d8-f5587b0328e7"
                }
            ]
        },
        {
            "id": "4560f977-7780-4cb9-95ad-31f49308d42a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ebed1fa-cf56-4a62-9bd8-e87e587ddd9f",
            "compositeImage": {
                "id": "5d8a1cbc-2b37-4079-8212-600810e4192e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4560f977-7780-4cb9-95ad-31f49308d42a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "956d45ab-71d6-4666-89fc-b4155ed6c1b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4560f977-7780-4cb9-95ad-31f49308d42a",
                    "LayerId": "0026dd65-1944-4974-b4d8-f5587b0328e7"
                }
            ]
        },
        {
            "id": "8c53e9bd-502c-4419-8f16-e4e2b1b9f91a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ebed1fa-cf56-4a62-9bd8-e87e587ddd9f",
            "compositeImage": {
                "id": "fcd5067e-41ee-46a3-a3b7-24e2f001bf17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c53e9bd-502c-4419-8f16-e4e2b1b9f91a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e303ee7-96e1-4fc6-b2af-b7f4a6d26d58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c53e9bd-502c-4419-8f16-e4e2b1b9f91a",
                    "LayerId": "0026dd65-1944-4974-b4d8-f5587b0328e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "0026dd65-1944-4974-b4d8-f5587b0328e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ebed1fa-cf56-4a62-9bd8-e87e587ddd9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 23,
    "yorig": 38
}