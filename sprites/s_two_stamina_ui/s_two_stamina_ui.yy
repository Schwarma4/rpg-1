{
    "id": "5a4f4a61-3ac3-4df1-9802-1e44fe742c5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_two_stamina_ui",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "520827c6-e370-4ab3-93eb-673e45cd66bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a4f4a61-3ac3-4df1-9802-1e44fe742c5f",
            "compositeImage": {
                "id": "3bd9f8de-0755-4e1a-8cb1-cbf78c80347d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "520827c6-e370-4ab3-93eb-673e45cd66bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b45a225-9a90-4064-a31c-1f2a44d501d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "520827c6-e370-4ab3-93eb-673e45cd66bb",
                    "LayerId": "59e4009d-dc91-4576-94b9-a68897168ea9"
                }
            ]
        },
        {
            "id": "4ca7eb75-2be6-4624-876a-535c0fc986e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a4f4a61-3ac3-4df1-9802-1e44fe742c5f",
            "compositeImage": {
                "id": "72a06572-147b-4383-8da9-e9bd4f7177cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ca7eb75-2be6-4624-876a-535c0fc986e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "726061c0-821c-4222-8a02-5ebe5ff26d4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ca7eb75-2be6-4624-876a-535c0fc986e3",
                    "LayerId": "59e4009d-dc91-4576-94b9-a68897168ea9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "59e4009d-dc91-4576-94b9-a68897168ea9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a4f4a61-3ac3-4df1-9802-1e44fe742c5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}