{
    "id": "26c2eb1d-5d3b-4ab2-837a-3db03ef7d499",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_camera",
    "eventList": [
        {
            "id": "c0180ae9-7304-4a7c-8e8e-c1bb3055d0dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "26c2eb1d-5d3b-4ab2-837a-3db03ef7d499"
        },
        {
            "id": "51355f03-7c62-4c75-bfa2-65758aae5956",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "26c2eb1d-5d3b-4ab2-837a-3db03ef7d499"
        },
        {
            "id": "e3501c31-b2f9-4a42-a026-4b8f9e1731b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "26c2eb1d-5d3b-4ab2-837a-3db03ef7d499"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}