{
    "id": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_fly_run_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a623c10e-b2f0-4098-ad55-1b8123d81d96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "21dcbf7c-a1b4-4425-a5b0-46065cf5d2e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a623c10e-b2f0-4098-ad55-1b8123d81d96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd496fba-854a-4558-bb8d-37090bd54af6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a623c10e-b2f0-4098-ad55-1b8123d81d96",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        },
        {
            "id": "52b2dc2e-6e83-4610-ab9d-c8f760fc5bee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "bdae8d7d-89cc-444b-91ed-2a54929bfba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b2dc2e-6e83-4610-ab9d-c8f760fc5bee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e1ee722-9113-458f-92f1-f2e6bfb09bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b2dc2e-6e83-4610-ab9d-c8f760fc5bee",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        },
        {
            "id": "37837164-d635-4fe2-b9ca-ad6a7c4966c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "b9b0b9bf-6d62-4a5d-9874-a5bf4b0b65d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37837164-d635-4fe2-b9ca-ad6a7c4966c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a17208c2-f62b-43d2-b395-0dfe2f822fa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37837164-d635-4fe2-b9ca-ad6a7c4966c7",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        },
        {
            "id": "d4d04ee2-2788-4447-9821-2fecbb36c85e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "ffbe64f7-3c1d-4257-8a23-a4b59acb3cc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4d04ee2-2788-4447-9821-2fecbb36c85e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84a7d5a8-ebcc-4d22-aa2f-9fd4a2f1472f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4d04ee2-2788-4447-9821-2fecbb36c85e",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        },
        {
            "id": "c6fbafb0-e60f-4ba4-bde9-cdf2b3fedf1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "b534ea97-1773-43eb-9778-e60629f469be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6fbafb0-e60f-4ba4-bde9-cdf2b3fedf1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2ab3cdb-d581-4ed5-94e8-190f28c112ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6fbafb0-e60f-4ba4-bde9-cdf2b3fedf1a",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        },
        {
            "id": "d9c51860-524f-4af2-98e6-e6ca907d4faf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "5e9d9220-de28-49f8-b9c9-631ee7be48e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9c51860-524f-4af2-98e6-e6ca907d4faf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75df3901-e282-4ff4-bbb9-22616477f818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9c51860-524f-4af2-98e6-e6ca907d4faf",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        },
        {
            "id": "b5c444b6-5056-44a6-b39c-944e602fa978",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "e9fb455f-a998-4e3b-aa37-9b0af343a483",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5c444b6-5056-44a6-b39c-944e602fa978",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1117374-1829-448f-b1ea-eb2b70b87136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5c444b6-5056-44a6-b39c-944e602fa978",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        },
        {
            "id": "fb94f4b3-b43d-47fb-a9c8-99038c5e6dd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "bb5df18a-07d4-4c79-98f6-c5ac19aa413c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb94f4b3-b43d-47fb-a9c8-99038c5e6dd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc09344d-84b4-4fdc-9d44-9e8459b4f22b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb94f4b3-b43d-47fb-a9c8-99038c5e6dd1",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        },
        {
            "id": "0093c4d6-ad62-47ec-ac3c-7349f651986e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "38fe9e3f-5c7d-4c66-baa8-f58ef76351d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0093c4d6-ad62-47ec-ac3c-7349f651986e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c9ba52b-7b78-4fcd-8627-260685b4e9a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0093c4d6-ad62-47ec-ac3c-7349f651986e",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        },
        {
            "id": "11364132-a69b-4007-acb5-2ed6c7e4aca4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "b7c5a3aa-e958-44ae-853f-701dfff40008",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11364132-a69b-4007-acb5-2ed6c7e4aca4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fad1b27e-2068-44fe-b67d-ceea69736a22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11364132-a69b-4007-acb5-2ed6c7e4aca4",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        },
        {
            "id": "c1a500d3-13b8-43cf-8bd6-3763f91d0d3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "48db08ed-8514-4f23-9365-9f04535f02ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1a500d3-13b8-43cf-8bd6-3763f91d0d3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "984ae62a-13a7-4e85-b1c5-8a46b5a604d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1a500d3-13b8-43cf-8bd6-3763f91d0d3d",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        },
        {
            "id": "80aca1d2-5be1-4a12-b7c4-2d82653e3b83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "b9d52553-c69f-43a9-9823-182c6d0fcadf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80aca1d2-5be1-4a12-b7c4-2d82653e3b83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfd994ed-4645-4eba-b5f2-29c2c69b6517",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80aca1d2-5be1-4a12-b7c4-2d82653e3b83",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        },
        {
            "id": "1bd777dc-f1f3-4994-a329-71ae2ebb5790",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "116cca12-f0c5-4092-a701-3978e5ac43ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bd777dc-f1f3-4994-a329-71ae2ebb5790",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd19909e-5b1d-428d-92f1-28ad96afa5b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bd777dc-f1f3-4994-a329-71ae2ebb5790",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        },
        {
            "id": "981963b9-568c-4d19-a953-899cec09dc35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "compositeImage": {
                "id": "224f41e7-08cb-48e4-999c-98f4d6973447",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "981963b9-568c-4d19-a953-899cec09dc35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2095d145-8a98-486d-9645-eb2ea4fe15dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "981963b9-568c-4d19-a953-899cec09dc35",
                    "LayerId": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "64ad1dd7-5b6d-470a-8b8b-ccd73d459a59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 23
}