
if paused_ {
	
	if o_input.game_reset_pressed_ {
		game_restart();
	}
	
	if o_input.switch_char_pressed_ {
		if (global.current_player == 2) {
			global.current_player = 1;
			o_camera.target_ = o_player_one;
		} else {
			global.current_player = 2;
			o_camera.target_ = o_player_two;
		}
	}

	if global.current_player = 1 {
			
		if o_input.switch_follower_pressed_ {
			if o_player_two.follower_state_ = 3 {
				o_player_two.follower_state_ = 0;	
			} else {
				o_player_two.follower_state_ = o_player_two.follower_state_ + 1;
				global.follower_state_two = o_player_two.follower_state_;
			}
		}
		var _array_size = array_length_1d(global.inventory_one);
	
		if o_input.right_pressed_ {
			item_index_ = min(item_index_ + 1, _array_size -1);	
			audio_play_sound(a_menu_move, 1, false);
		}
		
		if o_input.left_pressed_ {
			item_index_ = max(item_index_ - 1, 0);	
			audio_play_sound(a_menu_move, 1, false);
		}
	
		if o_input.action_one_pressed_ {
			global.item[0] = global.inventory_one[item_index_];	
			audio_play_sound(a_menu_select, 3, false);
		}
	
		if o_input.action_two_pressed_ {
			global.item[1] = global.inventory_one[item_index_];	
			audio_play_sound(a_menu_select, 3, false);
		}
		
		if o_input.switch_item_pressed_ && global.inventory_one[item_index_] != noone {
			inventory_switch(global.inventory_one[item_index_], 1);
		}
	} else {
			
		if o_input.switch_follower_pressed_ {
			if o_player_one.follower_state_ = 3 {
				o_player_one.follower_state_ = 0;	
			} else {
				o_player_one.follower_state_ = o_player_one.follower_state_ + 1;
				global.follower_state_one = o_player_one.follower_state_;
			}
		}
		var _array_size = array_length_1d(global.inventory_two);
	
		if o_input.right_pressed_ {
			item_index_ = min(item_index_ + 1, _array_size -1);	
			audio_play_sound(a_menu_move, 1, false);
		}
		
		if o_input.left_pressed_ {
			item_index_ = max(item_index_ - 1, 0);	
			audio_play_sound(a_menu_move, 1, false);
		}
	
		if o_input.action_one_pressed_ {
			global.item[2] = global.inventory_two[item_index_];	
			audio_play_sound(a_menu_select, 3, false);
		}
	
		if o_input.action_two_pressed_ {
			global.item[3] = global.inventory_two[item_index_];	
			audio_play_sound(a_menu_select, 3, false);
		}
				
		if o_input.switch_item_pressed_  && global.inventory_two[item_index_] != noone {
			inventory_switch(global.inventory_two[item_index_], 2);
		}
	}
	
}

if o_input.pause_pressed_ {
	if paused_ {
		paused_ = false;	
		if sprite_exists(paused_sprite_) {
			sprite_delete(paused_sprite_);	
		}
		instance_activate_all();
		audio_play_sound(a_unpause, 5, false);
	} else {
		paused_ = true;	
		paused_sprite_ = sprite_create_from_surface(application_surface, 0, 0, view_wport[0], view_hport[0], false, false, 0, 0);
		instance_deactivate_all(true);
		var _array_size = array_length_1d(global.inventory_one);
		for (var _i=0; _i< _array_size; _i++) {
			instance_activate_object(global.inventory_one[_i]);	
		}
		var _array_size = array_length_1d(global.inventory_two);
		for (var _i=0; _i< _array_size; _i++) {
			instance_activate_object(global.inventory_two[_i]);	
		}
		instance_activate_object(o_input);
		instance_activate_object(o_camera);
		instance_activate_object(o_player);
		audio_play_sound(a_pause, 5, false);
	}
}