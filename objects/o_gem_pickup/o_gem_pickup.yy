{
    "id": "a67419b6-3e0c-48ef-bd88-c3f2c014c43e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_gem_pickup",
    "eventList": [
        {
            "id": "62305216-3f42-4412-b12c-a26c9ed8338d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a67419b6-3e0c-48ef-bd88-c3f2c014c43e"
        },
        {
            "id": "07401012-3b9a-4fbd-94b4-69df183fe880",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a67419b6-3e0c-48ef-bd88-c3f2c014c43e"
        },
        {
            "id": "2f70bc35-ae61-4da2-b109-c5438c00ba49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "a67419b6-3e0c-48ef-bd88-c3f2c014c43e"
        },
        {
            "id": "9d293280-2078-434b-a802-3dadeffa4da5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "a67419b6-3e0c-48ef-bd88-c3f2c014c43e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8abdd0d5-6a40-4bf2-b52f-5d9751fb07e3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b927b9a-a848-4d1b-b595-8739e6d51534",
    "visible": true
}