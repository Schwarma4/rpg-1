{
    "id": "1472049c-de27-4fe2-b445-85fe2c0d1799",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dirtpath_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 16,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8970ef18-837c-4f4c-80b4-a7b41cbb5d84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1472049c-de27-4fe2-b445-85fe2c0d1799",
            "compositeImage": {
                "id": "47a8c958-ac06-4815-a473-196fcfacacd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8970ef18-837c-4f4c-80b4-a7b41cbb5d84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c31dadb0-8375-4b64-b3bd-b07af5104545",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8970ef18-837c-4f4c-80b4-a7b41cbb5d84",
                    "LayerId": "00c202cf-6468-44e3-8d6b-7656449dd1b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "00c202cf-6468-44e3-8d6b-7656449dd1b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1472049c-de27-4fe2-b445-85fe2c0d1799",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 0,
    "yorig": 0
}