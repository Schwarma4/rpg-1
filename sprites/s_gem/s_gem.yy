{
    "id": "8b927b9a-a848-4d1b-b595-8739e6d51534",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_gem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4887fccc-afbb-482f-a94b-ca9bb9d0ee21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b927b9a-a848-4d1b-b595-8739e6d51534",
            "compositeImage": {
                "id": "e27973ea-04fa-489c-b58c-efe19c480e9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4887fccc-afbb-482f-a94b-ca9bb9d0ee21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9732ddfd-aced-4864-a724-982ca354c85e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4887fccc-afbb-482f-a94b-ca9bb9d0ee21",
                    "LayerId": "07a8ecb8-6f90-46ce-88f6-2fd1e3d1939b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "07a8ecb8-6f90-46ce-88f6-2fd1e3d1939b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b927b9a-a848-4d1b-b595-8739e6d51534",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}