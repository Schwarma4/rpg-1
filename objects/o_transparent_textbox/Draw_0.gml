draw_sprite(s_transparent_textbox, 0, x, y);
draw_set_font(f_dialogue);
if charCount_ < string_length(text_[page_]){
	charCount_ += 1;
}
textPart_ = string_copy(text_[page_], 1, charCount_);
draw_text(x + (boxWidth_/3), y, name_);
draw_text_ext(x, y + stringHeight_, textPart_, stringHeight_, boxWidth_);


draw_set_font(global.font);
