{
    "id": "ec51eb8f-ccb6-4765-b704-d03164660885",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_fly_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "432b97d8-8f2f-47d0-982e-11ce267c37fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec51eb8f-ccb6-4765-b704-d03164660885",
            "compositeImage": {
                "id": "2e638c50-9583-48ed-852b-e535617c9ac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "432b97d8-8f2f-47d0-982e-11ce267c37fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45a93b70-6782-4e19-88fa-4a83f808d0f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "432b97d8-8f2f-47d0-982e-11ce267c37fb",
                    "LayerId": "ffebd330-7c07-4050-824d-984a571c345d"
                }
            ]
        },
        {
            "id": "afca8e91-63c0-42c9-9fa3-e5492dc070e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec51eb8f-ccb6-4765-b704-d03164660885",
            "compositeImage": {
                "id": "5bf7e1f6-1273-4711-aa5b-afea8b20774f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afca8e91-63c0-42c9-9fa3-e5492dc070e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9249f7e6-71fb-4d56-8bea-a9ac1c40a5e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afca8e91-63c0-42c9-9fa3-e5492dc070e6",
                    "LayerId": "ffebd330-7c07-4050-824d-984a571c345d"
                }
            ]
        },
        {
            "id": "82dc7165-6bb6-4591-b2fa-8d6570eaf4e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec51eb8f-ccb6-4765-b704-d03164660885",
            "compositeImage": {
                "id": "bd0bc24c-014b-4081-907c-f0f2a6a7360b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82dc7165-6bb6-4591-b2fa-8d6570eaf4e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39cd0d04-4469-4ad5-b560-68544af31b8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82dc7165-6bb6-4591-b2fa-8d6570eaf4e6",
                    "LayerId": "ffebd330-7c07-4050-824d-984a571c345d"
                }
            ]
        },
        {
            "id": "417e35c6-2385-42e4-b27e-2b480483129c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec51eb8f-ccb6-4765-b704-d03164660885",
            "compositeImage": {
                "id": "59fc7055-80f8-44dc-b46f-bb6d47bb7313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417e35c6-2385-42e4-b27e-2b480483129c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2e9299d-e2b3-4f36-9502-fabec4c452cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417e35c6-2385-42e4-b27e-2b480483129c",
                    "LayerId": "ffebd330-7c07-4050-824d-984a571c345d"
                }
            ]
        },
        {
            "id": "d61bdbc6-11fa-48f6-b52c-dcf5f8b0e369",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec51eb8f-ccb6-4765-b704-d03164660885",
            "compositeImage": {
                "id": "726cdb76-92c5-4c9c-adf5-0a96dca62d35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d61bdbc6-11fa-48f6-b52c-dcf5f8b0e369",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef96aee4-bbd5-4e86-a064-99a0b91b7f4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d61bdbc6-11fa-48f6-b52c-dcf5f8b0e369",
                    "LayerId": "ffebd330-7c07-4050-824d-984a571c345d"
                }
            ]
        },
        {
            "id": "ed08c581-8a23-4ab5-8e26-b6c975cce2a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec51eb8f-ccb6-4765-b704-d03164660885",
            "compositeImage": {
                "id": "32a627cc-9bff-4bb5-9412-8a9ca02badeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed08c581-8a23-4ab5-8e26-b6c975cce2a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65a963ce-0ccf-4a4c-b853-b9e5453902cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed08c581-8a23-4ab5-8e26-b6c975cce2a3",
                    "LayerId": "ffebd330-7c07-4050-824d-984a571c345d"
                }
            ]
        },
        {
            "id": "9c653418-ba02-4700-bd3c-0591ea1db027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec51eb8f-ccb6-4765-b704-d03164660885",
            "compositeImage": {
                "id": "a1799165-1ac9-43c2-b1ab-4849f7a5d410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c653418-ba02-4700-bd3c-0591ea1db027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d12dc0b-c339-4acc-bb6d-bde9b020853e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c653418-ba02-4700-bd3c-0591ea1db027",
                    "LayerId": "ffebd330-7c07-4050-824d-984a571c345d"
                }
            ]
        },
        {
            "id": "ae144e1d-0e2b-48ee-b768-6ceb8e0270af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec51eb8f-ccb6-4765-b704-d03164660885",
            "compositeImage": {
                "id": "deba5517-b172-40b7-a148-f25982b3c956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae144e1d-0e2b-48ee-b768-6ceb8e0270af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c2be33a-b2df-4dc8-abad-309d5e0d1cc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae144e1d-0e2b-48ee-b768-6ceb8e0270af",
                    "LayerId": "ffebd330-7c07-4050-824d-984a571c345d"
                }
            ]
        },
        {
            "id": "418e2367-1c1d-4219-aa53-d2445b8a45de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec51eb8f-ccb6-4765-b704-d03164660885",
            "compositeImage": {
                "id": "89efb7de-7876-4a56-8887-4715efc5d47f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "418e2367-1c1d-4219-aa53-d2445b8a45de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf6fdd34-0308-48d4-b82b-ce41984e2110",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "418e2367-1c1d-4219-aa53-d2445b8a45de",
                    "LayerId": "ffebd330-7c07-4050-824d-984a571c345d"
                }
            ]
        },
        {
            "id": "59038194-1379-4f3f-a86b-34c2d9870d23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec51eb8f-ccb6-4765-b704-d03164660885",
            "compositeImage": {
                "id": "ccd9ca04-09ae-4d99-bc8f-4287b7098275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59038194-1379-4f3f-a86b-34c2d9870d23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc95d0e1-e280-4c71-bbd8-eaf4346f83b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59038194-1379-4f3f-a86b-34c2d9870d23",
                    "LayerId": "ffebd330-7c07-4050-824d-984a571c345d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "ffebd330-7c07-4050-824d-984a571c345d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec51eb8f-ccb6-4765-b704-d03164660885",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 24
}