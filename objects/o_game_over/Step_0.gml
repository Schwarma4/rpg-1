var _last_index = index_;

if o_input.up_pressed_ {
	index_ = max(--index_, 0);	
}

if o_input.down_pressed_ {
	index_ = min(++index_, gameover_options_length_ - 1);	
}

if _last_index != index_ {
	audio_play_sound(a_menu_move, 1, false);	
}

if o_input.action_one_pressed_ {
	switch (index_) {
		case gameover_options.continue_game:
			audio_play_sound(a_menu_select, 3, false);
			ini_load("save_data.ini");
			break;
		case gameover_options.restart:
			audio_play_sound(a_menu_select, 3, false);
			game_restart();
			break;
		case gameover_options.quit:
			game_end();
			break;
	}
}