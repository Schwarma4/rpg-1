{
    "id": "224d222f-71c0-4594-b549-19671009dddc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_roll_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 4,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "572b3a95-e465-4a58-9bed-55964f470778",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "224d222f-71c0-4594-b549-19671009dddc",
            "compositeImage": {
                "id": "71659565-fecf-4302-9ea6-df313c4e8c25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "572b3a95-e465-4a58-9bed-55964f470778",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1dbea7a-e34d-4596-a6b1-a3bd2090041f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "572b3a95-e465-4a58-9bed-55964f470778",
                    "LayerId": "d5ae790f-4158-4964-88b3-304c2379b121"
                }
            ]
        },
        {
            "id": "ced51aa0-1aed-48e2-a580-726a026b87bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "224d222f-71c0-4594-b549-19671009dddc",
            "compositeImage": {
                "id": "fc64f010-86fd-4a00-9640-82bd07ce69bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ced51aa0-1aed-48e2-a580-726a026b87bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "371d2861-fc6c-4475-84b4-fdca360327cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ced51aa0-1aed-48e2-a580-726a026b87bd",
                    "LayerId": "d5ae790f-4158-4964-88b3-304c2379b121"
                }
            ]
        },
        {
            "id": "2bd006df-978a-4cb7-beee-98a87c0bbddf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "224d222f-71c0-4594-b549-19671009dddc",
            "compositeImage": {
                "id": "9b2426e6-a418-4a52-8b1f-e125a013ed7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bd006df-978a-4cb7-beee-98a87c0bbddf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b67b640-8028-4df5-90a3-d5b5f931abe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bd006df-978a-4cb7-beee-98a87c0bbddf",
                    "LayerId": "d5ae790f-4158-4964-88b3-304c2379b121"
                }
            ]
        },
        {
            "id": "3ef8fa49-5124-4fff-a38e-bfc155416dbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "224d222f-71c0-4594-b549-19671009dddc",
            "compositeImage": {
                "id": "00709335-43c2-444f-acb7-b14d20af295e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ef8fa49-5124-4fff-a38e-bfc155416dbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45c63de8-d524-4820-bbbb-4a24d7bdee2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ef8fa49-5124-4fff-a38e-bfc155416dbb",
                    "LayerId": "d5ae790f-4158-4964-88b3-304c2379b121"
                }
            ]
        },
        {
            "id": "496d163e-d500-40b0-8364-f8db9b2eebed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "224d222f-71c0-4594-b549-19671009dddc",
            "compositeImage": {
                "id": "2eca5e7e-8308-4499-99a1-f6dd95a7a02c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "496d163e-d500-40b0-8364-f8db9b2eebed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d8609dd-3234-4517-811a-96355053ddb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "496d163e-d500-40b0-8364-f8db9b2eebed",
                    "LayerId": "d5ae790f-4158-4964-88b3-304c2379b121"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d5ae790f-4158-4964-88b3-304c2379b121",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "224d222f-71c0-4594-b549-19671009dddc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 24
}