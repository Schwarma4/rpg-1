{
    "id": "a0ac3a1a-e841-4fca-892c-9eed0ee3aa90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_FLY_attack_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 11,
    "bbox_right": 35,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bae19984-cdd0-43e4-b952-c16e275a0dc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0ac3a1a-e841-4fca-892c-9eed0ee3aa90",
            "compositeImage": {
                "id": "36ee03ff-8133-4911-87fa-299edbd6a1d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bae19984-cdd0-43e4-b952-c16e275a0dc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1703489-456a-4d88-bcf5-98bb93ae6143",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bae19984-cdd0-43e4-b952-c16e275a0dc4",
                    "LayerId": "07079ab5-e9a0-4b07-9d03-4c928d5105c8"
                }
            ]
        },
        {
            "id": "4e455508-2b57-483f-b49b-53968600d2b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0ac3a1a-e841-4fca-892c-9eed0ee3aa90",
            "compositeImage": {
                "id": "870c40a5-4c21-4e03-a667-d0422b1458ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e455508-2b57-483f-b49b-53968600d2b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f2ed2a7-27b3-4c2c-89e8-a10bc3ceb4b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e455508-2b57-483f-b49b-53968600d2b1",
                    "LayerId": "07079ab5-e9a0-4b07-9d03-4c928d5105c8"
                }
            ]
        },
        {
            "id": "f4f1383e-d46b-476a-8434-9b483458f491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0ac3a1a-e841-4fca-892c-9eed0ee3aa90",
            "compositeImage": {
                "id": "11358618-7442-4047-9b31-f2e10830c5ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4f1383e-d46b-476a-8434-9b483458f491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3887c81-9b12-4eff-ab53-93ea107410c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4f1383e-d46b-476a-8434-9b483458f491",
                    "LayerId": "07079ab5-e9a0-4b07-9d03-4c928d5105c8"
                }
            ]
        },
        {
            "id": "1fb0ae56-23e3-41e9-8136-92143d247ffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0ac3a1a-e841-4fca-892c-9eed0ee3aa90",
            "compositeImage": {
                "id": "bbe173ff-4821-40c1-8b3e-341e3545b5ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fb0ae56-23e3-41e9-8136-92143d247ffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86ba1059-d7e3-417d-bf23-c92b347a7dff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fb0ae56-23e3-41e9-8136-92143d247ffd",
                    "LayerId": "07079ab5-e9a0-4b07-9d03-4c928d5105c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "07079ab5-e9a0-4b07-9d03-4c928d5105c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0ac3a1a-e841-4fca-892c-9eed0ee3aa90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 23,
    "yorig": 27
}