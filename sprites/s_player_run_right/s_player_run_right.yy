{
    "id": "5b8a4ded-a9da-459e-9489-8814eb38c50c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6afcc26-8967-4133-98c5-268a4e6c0d7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b8a4ded-a9da-459e-9489-8814eb38c50c",
            "compositeImage": {
                "id": "e1d6d9c5-9201-45fa-96fb-c1db9338c10d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6afcc26-8967-4133-98c5-268a4e6c0d7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4868ad9-e636-4b43-a473-313604dfd41b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6afcc26-8967-4133-98c5-268a4e6c0d7d",
                    "LayerId": "89a56cfe-8252-4cf3-8ec6-3c7db14fad8c"
                }
            ]
        },
        {
            "id": "8b517c22-1ff5-44d3-adf1-e8ff71e018bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b8a4ded-a9da-459e-9489-8814eb38c50c",
            "compositeImage": {
                "id": "541b0685-734c-4980-ab1c-8ff877b2e79b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b517c22-1ff5-44d3-adf1-e8ff71e018bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "171caabb-a67b-43e4-94c5-49975e6d21f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b517c22-1ff5-44d3-adf1-e8ff71e018bd",
                    "LayerId": "89a56cfe-8252-4cf3-8ec6-3c7db14fad8c"
                }
            ]
        },
        {
            "id": "0218ce43-40c6-4533-a815-f74c1a33151a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b8a4ded-a9da-459e-9489-8814eb38c50c",
            "compositeImage": {
                "id": "ec08f155-4129-4e51-a321-1a1a08db570e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0218ce43-40c6-4533-a815-f74c1a33151a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ee03cc9-cd37-4358-b2f7-507734541f20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0218ce43-40c6-4533-a815-f74c1a33151a",
                    "LayerId": "89a56cfe-8252-4cf3-8ec6-3c7db14fad8c"
                }
            ]
        },
        {
            "id": "7fee2270-a4d5-4cd6-990c-e3adb81155e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b8a4ded-a9da-459e-9489-8814eb38c50c",
            "compositeImage": {
                "id": "7fe49468-8a55-4695-98ca-d3a21f24fefa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fee2270-a4d5-4cd6-990c-e3adb81155e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "764cd62b-79cf-4725-8a48-d65b99b06cdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fee2270-a4d5-4cd6-990c-e3adb81155e8",
                    "LayerId": "89a56cfe-8252-4cf3-8ec6-3c7db14fad8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "89a56cfe-8252-4cf3-8ec6-3c7db14fad8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b8a4ded-a9da-459e-9489-8814eb38c50c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 24
}