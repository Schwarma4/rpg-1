{
    "id": "aff4099e-db5d-4192-afa5-ca18f4c7cf09",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_ranged_item",
    "eventList": [
        {
            "id": "302f7cb6-e3da-4644-bea2-95899514c861",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aff4099e-db5d-4192-afa5-ca18f4c7cf09"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d43e2dfd-4c7c-4148-8835-6544f350ca47",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bab772ba-e2d1-4c5b-b745-47e0319fba93",
    "visible": false
}