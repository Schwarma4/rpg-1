{
    "id": "d872fc10-5d61-48ea-841a-c27672c3381d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bomb_hitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de0d9144-4e5f-4ff3-bb85-a23f4be8e543",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d872fc10-5d61-48ea-841a-c27672c3381d",
            "compositeImage": {
                "id": "d0896054-c9c1-4a9b-aa0c-3174a81c9d3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de0d9144-4e5f-4ff3-bb85-a23f4be8e543",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8625ce55-ff86-4423-9fbb-5c3f52bb70ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de0d9144-4e5f-4ff3-bb85-a23f4be8e543",
                    "LayerId": "a9e2d33b-8dfa-4ca6-8fbd-6a27c5094d99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a9e2d33b-8dfa-4ca6-8fbd-6a27c5094d99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d872fc10-5d61-48ea-841a-c27672c3381d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}