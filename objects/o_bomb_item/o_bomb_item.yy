{
    "id": "aa51d919-e5aa-4eba-98ae-4526ce354022",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bomb_item",
    "eventList": [
        {
            "id": "3641355a-1280-4481-88bd-b7daf45d5615",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aa51d919-e5aa-4eba-98ae-4526ce354022"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d43e2dfd-4c7c-4148-8835-6544f350ca47",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9d5bcf7f-abcd-4e76-8cde-f0ea5b4e03cf",
    "visible": false
}