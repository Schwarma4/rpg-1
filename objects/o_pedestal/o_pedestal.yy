{
    "id": "89d89672-c034-47a6-a313-db6d8235cdee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_pedestal",
    "eventList": [
        {
            "id": "c92478b4-2cc8-4bb9-a3fe-0b5a6dfc3cc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89d89672-c034-47a6-a313-db6d8235cdee"
        },
        {
            "id": "b3d0c8a0-e596-4d61-a843-ba17f0adb408",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "405ac906-b8ad-4e6f-82dc-f64f941e6f84",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "89d89672-c034-47a6-a313-db6d8235cdee"
        },
        {
            "id": "44fc7670-c028-4d35-b562-c5df5c042c94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "567b4618-acdc-4dba-a473-cc37ca31a43a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "89d89672-c034-47a6-a313-db6d8235cdee"
        },
        {
            "id": "dc656007-85b8-457f-9e95-67ba10db787c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "89d89672-c034-47a6-a313-db6d8235cdee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dd533c36-fa69-499b-b3c6-3174a6006860",
    "visible": true
}