{
    "id": "6a6ee44c-58fe-49eb-a11a-8d0073d99602",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_sword_item",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6f1bc4a-05da-499f-aae7-5f4a36000c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6ee44c-58fe-49eb-a11a-8d0073d99602",
            "compositeImage": {
                "id": "464cabab-682a-4263-a625-c670955b366e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6f1bc4a-05da-499f-aae7-5f4a36000c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5e6615f-e06a-42ed-ac2d-f230933f1ef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6f1bc4a-05da-499f-aae7-5f4a36000c01",
                    "LayerId": "f2a62bf0-9773-469e-9a3e-6f7d55d54fd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f2a62bf0-9773-469e-9a3e-6f7d55d54fd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a6ee44c-58fe-49eb-a11a-8d0073d99602",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}