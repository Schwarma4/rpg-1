{
    "id": "b79c7069-91e7-4197-89f7-a54c8b0049f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_textbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 139,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee70e866-4e9c-436a-852f-2fe15e4f17d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79c7069-91e7-4197-89f7-a54c8b0049f1",
            "compositeImage": {
                "id": "4f89a779-eb3f-4a15-870d-8461cf00a579",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee70e866-4e9c-436a-852f-2fe15e4f17d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40d79ef6-a2fe-4f49-82fa-53c8862e1d41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee70e866-4e9c-436a-852f-2fe15e4f17d2",
                    "LayerId": "31792b8a-d499-427c-921e-298f73eda0eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 63,
    "layers": [
        {
            "id": "31792b8a-d499-427c-921e-298f73eda0eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b79c7069-91e7-4197-89f7-a54c8b0049f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 140,
    "xorig": 0,
    "yorig": 0
}