{
    "id": "afb06619-4d7b-49dc-bffc-a50497dbaf93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_eye_alien",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "686cb320-76ef-43b0-b4a4-c52fe5fa517c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afb06619-4d7b-49dc-bffc-a50497dbaf93",
            "compositeImage": {
                "id": "195c700c-c919-481e-b462-14a49c02a144",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "686cb320-76ef-43b0-b4a4-c52fe5fa517c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fa8ea43-5a73-4c1d-8ceb-3ebf23ae529d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "686cb320-76ef-43b0-b4a4-c52fe5fa517c",
                    "LayerId": "9dc7e4a3-cde6-44eb-bc14-ae894636226f"
                }
            ]
        },
        {
            "id": "0bfa3d14-bc11-4acb-890f-abd81e3ed62f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afb06619-4d7b-49dc-bffc-a50497dbaf93",
            "compositeImage": {
                "id": "4507115c-a5a8-4425-9f9f-244919b80cae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bfa3d14-bc11-4acb-890f-abd81e3ed62f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6be26692-b8b9-4306-a287-5c44ff61fe6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bfa3d14-bc11-4acb-890f-abd81e3ed62f",
                    "LayerId": "9dc7e4a3-cde6-44eb-bc14-ae894636226f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "9dc7e4a3-cde6-44eb-bc14-ae894636226f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afb06619-4d7b-49dc-bffc-a50497dbaf93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 23
}