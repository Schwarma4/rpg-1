{
    "id": "94562f1a-dbf3-4823-9268-02a240e4f3fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_lizard_billy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 22,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "711888a3-5094-4249-a461-6f6999dcbad7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94562f1a-dbf3-4823-9268-02a240e4f3fc",
            "compositeImage": {
                "id": "9a69b8c7-5a37-4c2c-9b41-2b79ac33d588",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "711888a3-5094-4249-a461-6f6999dcbad7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eca5e3d-bf85-4212-9826-4c9b5f9bf464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "711888a3-5094-4249-a461-6f6999dcbad7",
                    "LayerId": "6f8cf5c4-0d6f-407d-8c3a-8487ff8d7119"
                }
            ]
        },
        {
            "id": "925f32bc-71b6-45e0-98a6-c7b818086bff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94562f1a-dbf3-4823-9268-02a240e4f3fc",
            "compositeImage": {
                "id": "ddf981e4-d954-4ec2-823b-0030b88cfc78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "925f32bc-71b6-45e0-98a6-c7b818086bff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b5952da-4ce9-42fc-8e82-9ac73f13e6fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "925f32bc-71b6-45e0-98a6-c7b818086bff",
                    "LayerId": "6f8cf5c4-0d6f-407d-8c3a-8487ff8d7119"
                }
            ]
        },
        {
            "id": "72541d10-5826-4928-8861-a210364f59ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94562f1a-dbf3-4823-9268-02a240e4f3fc",
            "compositeImage": {
                "id": "e438b2b4-846e-4742-9418-90bfbb8ce9e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72541d10-5826-4928-8861-a210364f59ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fbd835d-9c04-46a0-b551-d2f950b1f31b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72541d10-5826-4928-8861-a210364f59ce",
                    "LayerId": "6f8cf5c4-0d6f-407d-8c3a-8487ff8d7119"
                }
            ]
        },
        {
            "id": "b25ff2e0-2617-4ad6-9b40-452e82323baf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94562f1a-dbf3-4823-9268-02a240e4f3fc",
            "compositeImage": {
                "id": "4fb53052-b76f-48e3-b1dd-8e6d89c5ee15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b25ff2e0-2617-4ad6-9b40-452e82323baf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc8925b8-8596-442e-85d4-e39dbd695c10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b25ff2e0-2617-4ad6-9b40-452e82323baf",
                    "LayerId": "6f8cf5c4-0d6f-407d-8c3a-8487ff8d7119"
                }
            ]
        },
        {
            "id": "1bd3768b-783c-4188-9f52-4105e71de46a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94562f1a-dbf3-4823-9268-02a240e4f3fc",
            "compositeImage": {
                "id": "13d284c2-f91f-4eab-ae5d-66ad9887dd06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bd3768b-783c-4188-9f52-4105e71de46a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48d004ff-69cb-48d7-a690-8263f8f2b0bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bd3768b-783c-4188-9f52-4105e71de46a",
                    "LayerId": "6f8cf5c4-0d6f-407d-8c3a-8487ff8d7119"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6f8cf5c4-0d6f-407d-8c3a-8487ff8d7119",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94562f1a-dbf3-4823-9268-02a240e4f3fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}