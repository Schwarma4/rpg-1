{
    "id": "71da77cd-2ab9-478a-9b13-044eaabfd248",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5cfd0ee-2beb-4b55-842b-04f7c5f9adc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71da77cd-2ab9-478a-9b13-044eaabfd248",
            "compositeImage": {
                "id": "0763aa80-262e-407a-b24d-ae1d37e70c0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5cfd0ee-2beb-4b55-842b-04f7c5f9adc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cb41f3c-5d84-462b-ae91-206e104e2028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5cfd0ee-2beb-4b55-842b-04f7c5f9adc4",
                    "LayerId": "ec4e249f-c940-4d35-8911-4e8dce77e8da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "ec4e249f-c940-4d35-8911-4e8dce77e8da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71da77cd-2ab9-478a-9b13-044eaabfd248",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 32
}