{
    "id": "cc0d8bf8-ea6d-4f6a-9fe1-4261c5f931d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_WORM_run_up1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f2fffd0-24c4-47c8-94f5-88bcbd689a32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc0d8bf8-ea6d-4f6a-9fe1-4261c5f931d3",
            "compositeImage": {
                "id": "bdd419e5-8ebc-46ce-b99f-cbc027175dc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2fffd0-24c4-47c8-94f5-88bcbd689a32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2b34ae3-b9ae-427d-9a57-3eef7da6d35f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2fffd0-24c4-47c8-94f5-88bcbd689a32",
                    "LayerId": "ae71c538-1358-4ec6-a137-8cdb64507391"
                }
            ]
        },
        {
            "id": "524f8023-6b7c-43b3-9410-31b63324d401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc0d8bf8-ea6d-4f6a-9fe1-4261c5f931d3",
            "compositeImage": {
                "id": "86b0ab1b-f85e-400a-afe1-ed8273eb0e15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "524f8023-6b7c-43b3-9410-31b63324d401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9565100-f97e-4939-8748-11df538724fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "524f8023-6b7c-43b3-9410-31b63324d401",
                    "LayerId": "ae71c538-1358-4ec6-a137-8cdb64507391"
                }
            ]
        },
        {
            "id": "3d7bd9ca-30f1-45d1-9eae-e8634ae8259f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc0d8bf8-ea6d-4f6a-9fe1-4261c5f931d3",
            "compositeImage": {
                "id": "388cbd65-36f0-46ec-addf-9098bbbfc5f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d7bd9ca-30f1-45d1-9eae-e8634ae8259f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32946a35-9250-4cf6-8410-fe13ca1572b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d7bd9ca-30f1-45d1-9eae-e8634ae8259f",
                    "LayerId": "ae71c538-1358-4ec6-a137-8cdb64507391"
                }
            ]
        },
        {
            "id": "cef0b5a7-9a5f-4394-87eb-2d4ef1de785a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc0d8bf8-ea6d-4f6a-9fe1-4261c5f931d3",
            "compositeImage": {
                "id": "8d9b05bf-b40a-48c6-8b88-cfc54755d8d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cef0b5a7-9a5f-4394-87eb-2d4ef1de785a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "210ddb92-62dc-41e7-9a58-26a37c3b25b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cef0b5a7-9a5f-4394-87eb-2d4ef1de785a",
                    "LayerId": "ae71c538-1358-4ec6-a137-8cdb64507391"
                }
            ]
        },
        {
            "id": "19c9ff01-f0b2-4fd6-8cd1-ee954280a3b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc0d8bf8-ea6d-4f6a-9fe1-4261c5f931d3",
            "compositeImage": {
                "id": "dbc29b83-4a40-4ffd-a863-33c1fb32005b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19c9ff01-f0b2-4fd6-8cd1-ee954280a3b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec43b0d2-1c67-4619-9e8d-f71b5de4cdb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19c9ff01-f0b2-4fd6-8cd1-ee954280a3b6",
                    "LayerId": "ae71c538-1358-4ec6-a137-8cdb64507391"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "ae71c538-1358-4ec6-a137-8cdb64507391",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc0d8bf8-ea6d-4f6a-9fe1-4261c5f931d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 29
}