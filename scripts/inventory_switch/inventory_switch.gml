/// @arg item
/// @arg _inventory_number

var _item = singleton(argument0);
var _inventory_number = argument1;


var _item_index_one = array_find_index(_item, global.inventory_one);
var _item_index_two = array_find_index(_item, global.inventory_two);

if _inventory_number == 1 {
		
	if _item_index_two == -1 {
		var _i = 0;
		var _array_size_one = array_length_1d(global.inventory_two);
		for (var _i = 0; _i < _array_size_one; _i++) {
			if global.inventory_two[_i] == noone {
				global.inventory_two[_i] = _item;
				break;
			}
		}
		_i = 0;
		var _array_size_two = array_length_1d(global.inventory_one);
		for (var _i = 0; _i < _array_size_one; _i++) {
			if global.inventory_one[_i] == _item {
				if global.item[0] == _item {
					global.item[0] = noone;	
				}
				if global.item[1] == _item {
					global.item[1] = noone;	
				}
				global.inventory_one[_i] = noone;
				return true;
			}
		}
	} else {
		return true;
	}
} else {
		if _item_index_one == -1 {
		var _i = 0;
		var _array_size_one = array_length_1d(global.inventory_one);
		for (var _i = 0; _i < _array_size_one; _i++) {
			if global.inventory_one[_i] == noone {
				global.inventory_one[_i] = _item;
				break;
			}
		}
		_i = 0;
		var _array_size_two = array_length_1d(global.inventory_two);
		for (var _i = 0; _i < _array_size_one; _i++) {
			if global.inventory_two[_i] == _item {
				if global.item[2] == _item {
					global.item[2] = noone;	
				}
				if global.item[3] == _item {
					global.item[3] = noone;	
				}
				global.inventory_two[_i] = noone;
				return true;
			}
		}
	} else {
		return true;
	}
}