{
    "id": "065da290-8692-4900-b627-579acd5e82ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dirtwall_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 32,
    "bbox_right": 287,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "227e9811-f3ad-4c43-a80b-46f7cd9a6f5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "065da290-8692-4900-b627-579acd5e82ef",
            "compositeImage": {
                "id": "65f084d1-3388-484c-afc2-ce6880a74461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "227e9811-f3ad-4c43-a80b-46f7cd9a6f5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8f142c9-8e67-4380-9e67-76da94e5dad5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "227e9811-f3ad-4c43-a80b-46f7cd9a6f5e",
                    "LayerId": "61179f35-6fed-4ca6-9880-64520fad8fe1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "61179f35-6fed-4ca6-9880-64520fad8fe1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "065da290-8692-4900-b627-579acd5e82ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 288,
    "xorig": 0,
    "yorig": 0
}