{
    "id": "f671701b-02d0-4498-88af-72a05ed9f2e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_palm_tree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 16,
    "bbox_right": 28,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd6742ae-5511-4980-8b5f-e80dec9ef5ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f671701b-02d0-4498-88af-72a05ed9f2e8",
            "compositeImage": {
                "id": "609db532-e53f-4c97-b417-0db1ba550f7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd6742ae-5511-4980-8b5f-e80dec9ef5ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "048e94fe-c2e1-4334-a200-1ad6cd76b648",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd6742ae-5511-4980-8b5f-e80dec9ef5ea",
                    "LayerId": "2e3b08ba-9b69-4508-abe0-a60e560b00e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "2e3b08ba-9b69-4508-abe0-a60e560b00e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f671701b-02d0-4498-88af-72a05ed9f2e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 23,
    "yorig": 57
}