/// @description RANGED STATE
image_speed = .8;

if animation_hit_frame(1) {
	var _stinger = instance_create_layer(x, y, "Instances", o_slingshot_dart);
	_stinger.direction = direction_;
	_stinger.image_angle = direction_;
	_stinger.speed = 2;
	state_ = player.move;
	audio_play_sound(a_stinger, 1, false);
}

//if animation_hit_frame(image_number - 1) {
//	state_ = player.move;	
//}