{
    "id": "caf9d160-9dff-4261-9f7f-0224f5bd23e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Elec_Genie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0fdc79f-25d4-4e4e-94e1-a52e5abd053a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caf9d160-9dff-4261-9f7f-0224f5bd23e7",
            "compositeImage": {
                "id": "e2f3b355-f213-4ce4-92cd-61a8a17e47bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0fdc79f-25d4-4e4e-94e1-a52e5abd053a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5de8ffb2-5066-4665-bef2-4394dc81e3e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0fdc79f-25d4-4e4e-94e1-a52e5abd053a",
                    "LayerId": "99957e07-b31a-4f64-8c1d-53933cc0f6a5"
                }
            ]
        },
        {
            "id": "e58c02cc-542e-472a-bd84-62e962635ec3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caf9d160-9dff-4261-9f7f-0224f5bd23e7",
            "compositeImage": {
                "id": "22639cf7-a94c-4ff7-a7d6-f59bbec1d450",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e58c02cc-542e-472a-bd84-62e962635ec3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5778a47-7141-4a8d-9568-74df757dccd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e58c02cc-542e-472a-bd84-62e962635ec3",
                    "LayerId": "99957e07-b31a-4f64-8c1d-53933cc0f6a5"
                }
            ]
        },
        {
            "id": "f5bb60ac-1d01-4f02-ae90-b431fd0453fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caf9d160-9dff-4261-9f7f-0224f5bd23e7",
            "compositeImage": {
                "id": "777aa6f0-baa3-43d8-82fb-46982c8940f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5bb60ac-1d01-4f02-ae90-b431fd0453fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f80864f0-b031-4b1b-b0e7-b1cc7e8be161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5bb60ac-1d01-4f02-ae90-b431fd0453fa",
                    "LayerId": "99957e07-b31a-4f64-8c1d-53933cc0f6a5"
                }
            ]
        },
        {
            "id": "2386ab5f-9973-4bab-bf22-ac3c06800396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caf9d160-9dff-4261-9f7f-0224f5bd23e7",
            "compositeImage": {
                "id": "1fa48aea-8d59-4938-8b61-ab128f77677b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2386ab5f-9973-4bab-bf22-ac3c06800396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "115acbdb-fb7f-4bef-81cd-95ef3b0c1cdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2386ab5f-9973-4bab-bf22-ac3c06800396",
                    "LayerId": "99957e07-b31a-4f64-8c1d-53933cc0f6a5"
                }
            ]
        },
        {
            "id": "ac7490e4-2e7a-4539-a752-a3e720bd3636",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caf9d160-9dff-4261-9f7f-0224f5bd23e7",
            "compositeImage": {
                "id": "0d2fb68c-aa95-42f9-ab0c-09789687d6b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac7490e4-2e7a-4539-a752-a3e720bd3636",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "859c503e-6293-4cef-afc3-16cc6ba5be2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac7490e4-2e7a-4539-a752-a3e720bd3636",
                    "LayerId": "99957e07-b31a-4f64-8c1d-53933cc0f6a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "99957e07-b31a-4f64-8c1d-53933cc0f6a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "caf9d160-9dff-4261-9f7f-0224f5bd23e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 19
}