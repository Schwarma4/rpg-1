{
    "id": "56e52978-7edc-48f4-9513-f4a6fd138a75",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_game_over",
    "eventList": [
        {
            "id": "73f61c60-643e-4fbc-a1cd-3a449e29f656",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "56e52978-7edc-48f4-9513-f4a6fd138a75"
        },
        {
            "id": "34e3c3bd-99d3-4c0f-a01f-cb59b52259b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "56e52978-7edc-48f4-9513-f4a6fd138a75"
        },
        {
            "id": "20f7d496-9fe2-4287-9094-0bc4770e1447",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "56e52978-7edc-48f4-9513-f4a6fd138a75"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}