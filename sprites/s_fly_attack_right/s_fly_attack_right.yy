{
    "id": "0730f91d-2d84-4c62-9e51-aab783edf7ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_fly_attack_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94759f99-4adf-4b19-8f55-bc7671ba57a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0730f91d-2d84-4c62-9e51-aab783edf7ba",
            "compositeImage": {
                "id": "ca433f24-aa25-4e05-bb81-d2c7da87a16e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94759f99-4adf-4b19-8f55-bc7671ba57a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "599429d5-a5ce-4378-a3f3-d928130e61c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94759f99-4adf-4b19-8f55-bc7671ba57a5",
                    "LayerId": "d334e865-d84f-4ac7-94a6-c4883c7edf34"
                }
            ]
        },
        {
            "id": "38b4bdaf-8f8f-49bf-b2c7-227672dbaa6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0730f91d-2d84-4c62-9e51-aab783edf7ba",
            "compositeImage": {
                "id": "4bfb8601-0393-4257-86c4-81837596d38a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38b4bdaf-8f8f-49bf-b2c7-227672dbaa6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "522336a6-d210-4b33-a500-5bfd7cabf065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38b4bdaf-8f8f-49bf-b2c7-227672dbaa6a",
                    "LayerId": "d334e865-d84f-4ac7-94a6-c4883c7edf34"
                }
            ]
        },
        {
            "id": "cc9d9122-c711-4f5f-bf06-ce71ddc1f250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0730f91d-2d84-4c62-9e51-aab783edf7ba",
            "compositeImage": {
                "id": "0150e9fb-251e-4037-9d92-a44e70680c9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc9d9122-c711-4f5f-bf06-ce71ddc1f250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0df118df-1fb7-4b46-ac04-0b9b7e085bc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc9d9122-c711-4f5f-bf06-ce71ddc1f250",
                    "LayerId": "d334e865-d84f-4ac7-94a6-c4883c7edf34"
                }
            ]
        },
        {
            "id": "d51edfde-b9be-4713-9346-15efb5d9e0c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0730f91d-2d84-4c62-9e51-aab783edf7ba",
            "compositeImage": {
                "id": "3222e6fd-19ce-4f7e-85f1-119e32aedca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d51edfde-b9be-4713-9346-15efb5d9e0c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33720bbd-a0c2-461e-86f7-dccc73fb4b86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d51edfde-b9be-4713-9346-15efb5d9e0c8",
                    "LayerId": "d334e865-d84f-4ac7-94a6-c4883c7edf34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d334e865-d84f-4ac7-94a6-c4883c7edf34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0730f91d-2d84-4c62-9e51-aab783edf7ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 14,
    "yorig": 23
}