{
    "id": "ca2b0822-9ffb-41e9-b3c5-8037845acf0f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_metal_door",
    "eventList": [
        {
            "id": "1e4e00d1-9006-4834-ad62-659128c8b945",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca2b0822-9ffb-41e9-b3c5-8037845acf0f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "be9594cd-d92a-432d-a3d4-7878fec717ee",
    "visible": true
}