{
    "id": "be9594cd-d92a-432d-a3d4-7878fec717ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_metal_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 8,
    "bbox_right": 57,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20137de8-0a3b-4930-aaf8-d6da406f345b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be9594cd-d92a-432d-a3d4-7878fec717ee",
            "compositeImage": {
                "id": "8f1a59bc-2191-494b-bacb-f98e9954da21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20137de8-0a3b-4930-aaf8-d6da406f345b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e0f6232-ed93-41a4-94a7-e71180b810cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20137de8-0a3b-4930-aaf8-d6da406f345b",
                    "LayerId": "ff290327-1f06-4c07-a68c-353ac6c8dc92"
                }
            ]
        },
        {
            "id": "dbf2c8af-c35b-4958-9d68-4dd724a275d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be9594cd-d92a-432d-a3d4-7878fec717ee",
            "compositeImage": {
                "id": "35b9b8a7-9986-47e9-99fd-ff91ec16b9aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbf2c8af-c35b-4958-9d68-4dd724a275d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b4523b9-e955-4b70-b63f-3112029a73b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbf2c8af-c35b-4958-9d68-4dd724a275d9",
                    "LayerId": "ff290327-1f06-4c07-a68c-353ac6c8dc92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ff290327-1f06-4c07-a68c-353ac6c8dc92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be9594cd-d92a-432d-a3d4-7878fec717ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}