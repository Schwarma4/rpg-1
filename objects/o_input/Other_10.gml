/// @description GET INPUT
right_ = keyboard_check(vk_right);
left_ = keyboard_check(vk_left);
up_ = keyboard_check(vk_up);
down_ = keyboard_check(vk_down);

action_one_pressed_ = keyboard_check_pressed(ord("Z"));
action_two_pressed_ = keyboard_check_pressed(ord("X"));

pause_pressed_ = keyboard_check_pressed(vk_enter);


right_pressed_ = keyboard_check_pressed(vk_right);
left_pressed_ = keyboard_check_pressed(vk_left);
up_pressed_ = keyboard_check_pressed(vk_up);
down_pressed_ = keyboard_check_pressed(vk_down);

dialogue_pressed_ = keyboard_check_pressed(vk_space);
switch_char_pressed_ = keyboard_check_pressed(ord("T"));
switch_item_pressed_ = keyboard_check_pressed(ord("Y"));
switch_follower_pressed_ = keyboard_check_pressed(ord("U"));

game_reset_pressed_ = keyboard_check_pressed(ord("P"));