{
    "id": "a94e8121-a00e-497c-a2bb-a47de79295c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Apple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 4,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a153faea-ee09-4e57-be54-893659b57fb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e8121-a00e-497c-a2bb-a47de79295c4",
            "compositeImage": {
                "id": "47292571-ef19-4a01-81d8-0afc2808ade7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a153faea-ee09-4e57-be54-893659b57fb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26a8a4ec-3c51-4054-9f88-fb90d4445f66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a153faea-ee09-4e57-be54-893659b57fb4",
                    "LayerId": "f9b844b5-21ad-4c5d-b1fe-25df1731de06"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f9b844b5-21ad-4c5d-b1fe-25df1731de06",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a94e8121-a00e-497c-a2bb-a47de79295c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}