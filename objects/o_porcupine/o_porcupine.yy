{
    "id": "5ec56bca-b070-4a79-87f3-cbbb13a8ca88",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_porcupine",
    "eventList": [
        {
            "id": "04eb1a45-57ef-4018-a015-d7ab4baebac2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ec56bca-b070-4a79-87f3-cbbb13a8ca88"
        },
        {
            "id": "18cff429-4afb-487e-ba6b-7acae39982f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "5ec56bca-b070-4a79-87f3-cbbb13a8ca88"
        },
        {
            "id": "34b28381-53f4-4fa8-b191-88b63c4bcea2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "5ec56bca-b070-4a79-87f3-cbbb13a8ca88"
        },
        {
            "id": "dccab56f-08c0-48f3-bdd6-d1be9c573703",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "5ec56bca-b070-4a79-87f3-cbbb13a8ca88"
        },
        {
            "id": "4f837f12-3616-4c70-ac37-dce781911cef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "5ec56bca-b070-4a79-87f3-cbbb13a8ca88"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e45b7d6c-9a43-4674-a7e3-af78639eee20",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b83c4a65-7d78-44fc-860e-4b0aa5853775",
    "visible": true
}