{
    "id": "1943bfe0-1ebe-4978-ac4b-9f504002246a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_death_effect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4e6825b-92de-425b-ab0e-65fcbe65ee58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bfe0-1ebe-4978-ac4b-9f504002246a",
            "compositeImage": {
                "id": "569bd135-50f5-4736-9452-e29497fb64b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4e6825b-92de-425b-ab0e-65fcbe65ee58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c2f0e05-31fb-4dc9-8d35-750c163a6a3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4e6825b-92de-425b-ab0e-65fcbe65ee58",
                    "LayerId": "996ebc51-2c8e-4bf5-953c-232da075e4d5"
                }
            ]
        },
        {
            "id": "8e5a43e4-1df8-4ff1-aee7-26ebee5b4533",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bfe0-1ebe-4978-ac4b-9f504002246a",
            "compositeImage": {
                "id": "1bdd373f-3cf2-433a-980e-7b98de2642a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e5a43e4-1df8-4ff1-aee7-26ebee5b4533",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ea7f395-1e23-4451-be0f-bb2c0b1ff3ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e5a43e4-1df8-4ff1-aee7-26ebee5b4533",
                    "LayerId": "996ebc51-2c8e-4bf5-953c-232da075e4d5"
                }
            ]
        },
        {
            "id": "2683011c-02fb-4f35-a665-1db14bcbcecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bfe0-1ebe-4978-ac4b-9f504002246a",
            "compositeImage": {
                "id": "f5fce7ab-3b64-4a49-bd7d-4cb7762c2361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2683011c-02fb-4f35-a665-1db14bcbcecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c9e51a4-dfd6-4b93-8ecf-565016ca2f20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2683011c-02fb-4f35-a665-1db14bcbcecf",
                    "LayerId": "996ebc51-2c8e-4bf5-953c-232da075e4d5"
                }
            ]
        },
        {
            "id": "f9848cce-4fa8-4b4e-9044-8983523b21d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bfe0-1ebe-4978-ac4b-9f504002246a",
            "compositeImage": {
                "id": "ad95e2be-63fe-490c-8f99-bda22ebd4dd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9848cce-4fa8-4b4e-9044-8983523b21d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f572b117-dc53-4d21-a816-ab023ce50d63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9848cce-4fa8-4b4e-9044-8983523b21d2",
                    "LayerId": "996ebc51-2c8e-4bf5-953c-232da075e4d5"
                }
            ]
        },
        {
            "id": "e5e4f91f-8249-4fb0-9afa-363b79e3e050",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bfe0-1ebe-4978-ac4b-9f504002246a",
            "compositeImage": {
                "id": "21d68960-a2b3-4a04-b80c-5ad10ffef4a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5e4f91f-8249-4fb0-9afa-363b79e3e050",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eca304f6-f461-4b7c-85e0-87147f5f3ba9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5e4f91f-8249-4fb0-9afa-363b79e3e050",
                    "LayerId": "996ebc51-2c8e-4bf5-953c-232da075e4d5"
                }
            ]
        },
        {
            "id": "098b9688-a3c8-431d-80bd-2b892f6c23a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bfe0-1ebe-4978-ac4b-9f504002246a",
            "compositeImage": {
                "id": "6b94f35f-d0b7-4cf2-9a70-66173c6f9355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "098b9688-a3c8-431d-80bd-2b892f6c23a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce7c97e-7e2a-4a8d-9565-df7ba25392e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "098b9688-a3c8-431d-80bd-2b892f6c23a5",
                    "LayerId": "996ebc51-2c8e-4bf5-953c-232da075e4d5"
                }
            ]
        },
        {
            "id": "511f9fa6-09ea-461b-a104-4b3e25b37137",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bfe0-1ebe-4978-ac4b-9f504002246a",
            "compositeImage": {
                "id": "d656c13c-4862-438b-b841-1fc14673200f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "511f9fa6-09ea-461b-a104-4b3e25b37137",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fda96ca-319d-40ed-9d86-c0c2b8fab457",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "511f9fa6-09ea-461b-a104-4b3e25b37137",
                    "LayerId": "996ebc51-2c8e-4bf5-953c-232da075e4d5"
                }
            ]
        },
        {
            "id": "b7033663-a373-48f4-942a-dc923e024a5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bfe0-1ebe-4978-ac4b-9f504002246a",
            "compositeImage": {
                "id": "cb0e598a-e256-43a5-87f2-656e1cc000fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7033663-a373-48f4-942a-dc923e024a5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cd77217-32f4-4402-a06d-21bcf05a3291",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7033663-a373-48f4-942a-dc923e024a5a",
                    "LayerId": "996ebc51-2c8e-4bf5-953c-232da075e4d5"
                }
            ]
        },
        {
            "id": "27800141-f6c2-46b0-a529-eda72419e10a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bfe0-1ebe-4978-ac4b-9f504002246a",
            "compositeImage": {
                "id": "bf33615c-f1f6-4234-825f-2a903828ff0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27800141-f6c2-46b0-a529-eda72419e10a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "158b41e5-edc9-4e9a-bd30-22b2e168ccd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27800141-f6c2-46b0-a529-eda72419e10a",
                    "LayerId": "996ebc51-2c8e-4bf5-953c-232da075e4d5"
                }
            ]
        },
        {
            "id": "aee687fe-0f8e-46ce-98da-8cbc9744d7fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bfe0-1ebe-4978-ac4b-9f504002246a",
            "compositeImage": {
                "id": "7447d5ef-782f-43a0-ad26-87ad972be3d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aee687fe-0f8e-46ce-98da-8cbc9744d7fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "febef914-d34a-444f-b2d3-31c1b671980d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aee687fe-0f8e-46ce-98da-8cbc9744d7fc",
                    "LayerId": "996ebc51-2c8e-4bf5-953c-232da075e4d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "996ebc51-2c8e-4bf5-953c-232da075e4d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1943bfe0-1ebe-4978-ac4b-9f504002246a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}