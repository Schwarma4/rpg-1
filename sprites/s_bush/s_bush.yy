{
    "id": "ffaef82b-5efb-4be1-a2ba-919ff82874f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bush",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e4cfca7-1fe7-42ef-bd26-5cefc44525fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffaef82b-5efb-4be1-a2ba-919ff82874f5",
            "compositeImage": {
                "id": "7898e950-33b6-4190-8e4e-0b60b0b12a82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e4cfca7-1fe7-42ef-bd26-5cefc44525fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88c0ab46-d33d-43da-b06c-f6efe7a56441",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e4cfca7-1fe7-42ef-bd26-5cefc44525fb",
                    "LayerId": "edf162d2-0a98-4259-a7e0-6f971e9b637a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "edf162d2-0a98-4259-a7e0-6f971e9b637a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffaef82b-5efb-4be1-a2ba-919ff82874f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}