{
    "id": "adb609ed-1728-4a2b-9efb-d2c658d0ddd1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bomb",
    "eventList": [
        {
            "id": "477d943a-881d-4b34-b419-d4fb42384e96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "adb609ed-1728-4a2b-9efb-d2c658d0ddd1"
        },
        {
            "id": "46350a81-a8ea-4663-880b-4d0b3571f625",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "adb609ed-1728-4a2b-9efb-d2c658d0ddd1"
        },
        {
            "id": "6fdc30b6-3ec2-421b-8b2c-8059954602c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "adb609ed-1728-4a2b-9efb-d2c658d0ddd1"
        },
        {
            "id": "61e04fbe-f556-4e4a-b38a-a37f6ad7301c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "adb609ed-1728-4a2b-9efb-d2c658d0ddd1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "91024126-54cf-4023-ab7e-676f1c19a5ce",
    "visible": true
}