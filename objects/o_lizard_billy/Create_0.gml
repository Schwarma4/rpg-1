event_inherited();
initialize_movement_entity(.5, .5, o_solid);


starting_state_ = lizard.idle;
state_ = starting_state_;

image_index = 0;
image_xscale = choose(1, -1);

alarm[1] = random_range(0, 1) * global.one_second;

myText_[0] = "Don't touch me. I hate foxes.";


MyName_ = "Billy";