/// @arg input
/// @arg item
/// @arg inventory_number
var _input = argument0;
var _item = argument1;
var _inventory_number = argument2;

if _inventory_number == 1 {
	if _input {
		if instance_exists(_item) && global.player_one_stamina >= _item.cost_ {
			state_ = _item.action_;
			global.player_one_stamina -= _item.cost_;
			global.player_one_stamina = max(0, global.player_one_stamina);		
			alarm[1] = global.one_second;
			image_index = 0;
		}
	}	
} else {
	if _input {
		if instance_exists(_item) && global.player_two_stamina >= _item.cost_ {
			state_ = _item.action_;
			global.player_two_stamina -= _item.cost_;
			global.player_two_stamina = max(0, global.player_two_stamina);		
			alarm[1] = global.one_second;
			image_index = 0;
		}
	}	
}