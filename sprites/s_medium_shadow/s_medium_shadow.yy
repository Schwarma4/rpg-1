{
    "id": "1eee2874-f3cf-4e9f-a1d9-0a56bb9e8a34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_medium_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dfa0d13-1835-4b48-b318-dd4735a6c560",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1eee2874-f3cf-4e9f-a1d9-0a56bb9e8a34",
            "compositeImage": {
                "id": "34f31601-fb09-48ee-b704-c6367826c751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dfa0d13-1835-4b48-b318-dd4735a6c560",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "302347b6-d37b-43a7-b545-108d658af3b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dfa0d13-1835-4b48-b318-dd4735a6c560",
                    "LayerId": "2464cd2c-c3f1-4a7b-b851-29c39e311598"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "2464cd2c-c3f1-4a7b-b851-29c39e311598",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1eee2874-f3cf-4e9f-a1d9-0a56bb9e8a34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 5
}