{
    "id": "a2c2bd07-5878-44c1-813e-77ea6ee741ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_worm_pause",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d691ccad-e85b-430d-b604-06db2a5d9493",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2c2bd07-5878-44c1-813e-77ea6ee741ae",
            "compositeImage": {
                "id": "04caca44-e5e4-4302-bd2a-ee0a5de88d15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d691ccad-e85b-430d-b604-06db2a5d9493",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52d9581f-5125-425e-b0f2-1d55bee8d48b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d691ccad-e85b-430d-b604-06db2a5d9493",
                    "LayerId": "7b0a48f4-5545-441f-bc70-33a813f9bf24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "7b0a48f4-5545-441f-bc70-33a813f9bf24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2c2bd07-5878-44c1-813e-77ea6ee741ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 24
}