/// @description STAMINA ALARM RECHARGE
global.player_one_stamina = min(global.player_one_stamina + 1, global.player_one_max_stamina);
if global.player_one_stamina < global.player_one_max_stamina {
	alarm[1] = global.one_second;
}