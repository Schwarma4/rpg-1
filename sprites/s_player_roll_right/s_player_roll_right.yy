{
    "id": "189a88bb-5e59-4c39-8614-61e035f2e9ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_roll_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa585113-a9ae-4a43-a31d-1e8578a3630f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189a88bb-5e59-4c39-8614-61e035f2e9ad",
            "compositeImage": {
                "id": "a2264ac4-b466-4474-ad2e-9c828355ac61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa585113-a9ae-4a43-a31d-1e8578a3630f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f9e1b07-cf47-4240-a71a-bc710571560a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa585113-a9ae-4a43-a31d-1e8578a3630f",
                    "LayerId": "ea18bf9e-15fa-4103-be1f-459134212e20"
                }
            ]
        },
        {
            "id": "5c72ae9b-a110-4fcd-887a-dda615cd6b50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189a88bb-5e59-4c39-8614-61e035f2e9ad",
            "compositeImage": {
                "id": "e23f0589-1e0a-4c92-95ed-ab69adb11b00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c72ae9b-a110-4fcd-887a-dda615cd6b50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d7818a2-fedc-4c59-88a0-fe39a151503e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c72ae9b-a110-4fcd-887a-dda615cd6b50",
                    "LayerId": "ea18bf9e-15fa-4103-be1f-459134212e20"
                }
            ]
        },
        {
            "id": "c1e347d0-8723-475c-9511-5f9ee6708254",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189a88bb-5e59-4c39-8614-61e035f2e9ad",
            "compositeImage": {
                "id": "6b4178b7-9a31-46b5-87ec-71704ababfe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1e347d0-8723-475c-9511-5f9ee6708254",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6a6cb44-603b-42de-92bf-28b052e5d3db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1e347d0-8723-475c-9511-5f9ee6708254",
                    "LayerId": "ea18bf9e-15fa-4103-be1f-459134212e20"
                }
            ]
        },
        {
            "id": "2c3d2de3-f12d-414b-a88a-5cabbd3fe746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189a88bb-5e59-4c39-8614-61e035f2e9ad",
            "compositeImage": {
                "id": "d5933b68-3fc1-4a20-b0dd-b8d7e69c9288",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c3d2de3-f12d-414b-a88a-5cabbd3fe746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "775fd805-1936-4e4d-841f-426d85c0a44b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c3d2de3-f12d-414b-a88a-5cabbd3fe746",
                    "LayerId": "ea18bf9e-15fa-4103-be1f-459134212e20"
                }
            ]
        },
        {
            "id": "77de3f1d-2025-4e73-9c17-41134b5cf72b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189a88bb-5e59-4c39-8614-61e035f2e9ad",
            "compositeImage": {
                "id": "f4f637ee-fee5-4140-99ec-5e09b529f1a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77de3f1d-2025-4e73-9c17-41134b5cf72b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e24fd36c-bf7f-4e2e-a1bf-0da47fe64275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77de3f1d-2025-4e73-9c17-41134b5cf72b",
                    "LayerId": "ea18bf9e-15fa-4103-be1f-459134212e20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ea18bf9e-15fa-4103-be1f-459134212e20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "189a88bb-5e59-4c39-8614-61e035f2e9ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 26
}