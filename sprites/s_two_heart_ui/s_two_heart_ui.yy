{
    "id": "678637ea-7ee0-4a1e-8e6f-e2fd8016dce9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_two_heart_ui",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2ef82e9-ff33-4410-ac68-d7b5c25e8c6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678637ea-7ee0-4a1e-8e6f-e2fd8016dce9",
            "compositeImage": {
                "id": "0c7ad684-02c3-4819-bb12-8d6c871ef413",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2ef82e9-ff33-4410-ac68-d7b5c25e8c6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39c1e369-9de4-461d-827e-5db21c204a26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2ef82e9-ff33-4410-ac68-d7b5c25e8c6f",
                    "LayerId": "ce2d9e98-d6e0-498b-b07e-0c7643c271f3"
                }
            ]
        },
        {
            "id": "63be3bab-bdda-4a76-a3c3-93b9b1860da5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678637ea-7ee0-4a1e-8e6f-e2fd8016dce9",
            "compositeImage": {
                "id": "06c8d133-3880-4a5f-a4fc-93efea614d20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63be3bab-bdda-4a76-a3c3-93b9b1860da5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "462c3ecb-b46b-4b61-ae92-8e79c3ca6d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63be3bab-bdda-4a76-a3c3-93b9b1860da5",
                    "LayerId": "ce2d9e98-d6e0-498b-b07e-0c7643c271f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "ce2d9e98-d6e0-498b-b07e-0c7643c271f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "678637ea-7ee0-4a1e-8e6f-e2fd8016dce9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}