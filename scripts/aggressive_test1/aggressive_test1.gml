///// @arg player
//var _player = argument0;
//var _stamina = global.player_one_stamina;
//var _max_stamina = global.player_one_max_stamina;
//var _health = global.player_one_health;
//var _max_health = global.player_one_max_health;
//var _player_object = o_player_two;
//var _item_0 = global.item[0];
//var _item_1 = global.item[1];

//if _player == 2 {
//	_stamina = global.player_two_stamina;
//	_max_stamina = global.player_two_max_stamina;
//	_health = global.player_two_health;
//	_max_health = global.player_two_max_health;
//	_player_object = o_player_one;
//	_item_0 = global.item[2];
//	_item_1 = global.item[3];
//}

//if !instance_exists(_player_object) {
//	if _player = 1 {
//		global.current_player = 2;	
//	} else {
//		global.current_player = 1;	
//	}
//	exit;
//}

////run towards main player
//image_speed = 0.6;
//direction_ = point_direction(x, y, _player_object.x, _player_object.y);	
//roll_direction_ = point_direction(x, y, _player_object.x, _player_object.y);
//max_speed_ = 1.5;
//acceleration_ = 0.5;
//var px, py;
//px = instance_nearest(x, y, _player_object).x;
//py = instance_nearest(x, y, _player_object).y;

////if main character is more than 42 pixels away and follower has evade equipped, roll towards main character
//if point_distance(x, y, px, py) > 42 && point_distance(x, y, px, py) < 24 && ((instance_exists(_item_0) && _item_0.action_ == player.evade && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.evade && _item_1.cost_ <= _stamina)) && point_distance(x + lengthdir_x(roll_speed_,roll_direction_), y + lengthdir_y(roll_speed_,roll_direction_), o_enemy.x, o_enemy.y) > 64 {
//	set_movement(roll_direction_, 0.5);
//	set_sprite_facing();
//	get_direction_facing(roll_direction_);
//	if _item_0.action_ == player.evade {
//		state_ = _item_0.action_;
//		_stamina -= _item_0.cost_;
//		_stamina = max(0, _stamina);		
//		alarm[1] = global.one_second;
//	} else if _item_1.action_ == player.evade {
//		state_ = _item_1.action_;
//		_stamina -= _item_1.cost_;
//		_stamina = max(0, _stamina);		
//		alarm[1] = global.one_second;
//	}
//} else {
//	set_movement(direction_, 0.5);
//	set_sprite_facing();
//	get_direction_facing(direction_);	
//	if point_distance(x, y, px, py) > 24 {	
//		add_movement_maxspeed(direction_, acceleration_, max_speed_);
//		move_movement_entity(true);
//	} else {
//		apply_friction_to_movement_entity();
//		image_speed = 0;
//	}
//}

////if enemy within range (64 pixels) move closer to attack
//if instance_exists(o_enemy) {
//	var ex, ey;
//	ex = ex;
//	ey = ey;
//	if point_distance(x, y, ex, ey) < 64 && ((instance_exists(_item_0) && _item_0.action_ == player.sword && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.sword && _item_1.cost_ <= _stamina) || (instance_exists(_item_0) && _item_0.action_ == player.melee && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.melee && _item_1.cost_ <= _stamina)) {
		
//		// fire ranged before moving closer to enemy
//		if (instance_exists(_item_0) && _item_0.action_ == player.ranged && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.ranged && _item_1.cost_ <= _stamina) {	
//			direction_ = point_direction(x, y, ex, ey);
//			set_movement(direction_, 0.5);
//			set_sprite_facing();
//			get_direction_facing(direction_);	
//			if _item_0.action_ == player.ranged {
//				state_ = _item_0.action_;
//				_stamina -= _item_0.cost_;
//				_stamina = max(0, _stamina);		
//				alarm[1] = global.one_second;	
//			} else {
//				state_ = _item_1.action_;
//				_stamina -= _item_1.cost_;
//				_stamina = max(0, _stamina);		
//				alarm[1] = global.one_second;	
//			}
//		}
		
//		//roll closer to enemy if available, otherwise walk
//		if (instance_exists(_item_0) && _item_0.action_ == player.evade && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.evade && _item_1.cost_ <= _stamina) {
//			roll_direction_ = point_direction(x, y, ex, ey);
//			set_movement(roll_direction_, 0.5);
//			set_sprite_facing();
//			get_direction_facing(roll_direction_);
//			if _item_0.action_ == player.evade {
//				state_ = _item_0.action_;
//				_stamina -= _item_0.cost_;
//				_stamina = max(0, _stamina);		
//				alarm[1] = global.one_second;
//			} else if _item_1.action_ == player.evade {
//				state_ = _item_1.action_;
//				_stamina -= _item_1.cost_;
//				_stamina = max(0, _stamina);		
//				alarm[1] = global.one_second;
//			}
//		} else {
//			direction_ = point_direction(x, y, ex, ey);
//			set_movement(direction_, 0.5);
//			set_sprite_facing();
//			get_direction_facing(direction_);
//			add_movement_maxspeed(direction_, acceleration_, max_speed_);
//		}
		
//		//Once close enough to enemy for melee, attack
//		if point_distance(x, y, ex, ey) < 16 {
//			direction_ = point_direction(x, y, ex, ey);
//			set_movement(direction_, 0.5);
//			set_sprite_facing();
//			get_direction_facing(direction_);	
//			if _item_0.action_ == player.sword {
//				state_ = _item_0.action_;
//				_stamina -= _item_0.cost_;
//				_stamina = max(0, _stamina);		
//				alarm[1] = global.one_second;
//			} else if _item_1.action_ == player.sword {
//				state_ = _item_1.action_;
//				_stamina -= _item_1.cost_;
//				_stamina = max(0, _stamina);		
//				alarm[1] = global.one_second;
//			} else if _item_0.action_ == player.melee {
//				state_ = _item_0.action_;
//				_stamina -= _item_0.cost_;
//				_stamina = max(0, _stamina);		
//				alarm[1] = global.one_second;
//			} else if _item_1.action_ == player.melee {
//				state_ = _item_1.action_;
//				_stamina -= _item_1.cost_;
//				_stamina = max(0, _stamina);		
//				alarm[1] = global.one_second;
//			}
//		}
//	}
//}

//if _player == 2 {
//	global.player_two_stamina = _stamina;
//	global.player_two_health = _health;
//} else {
//	global.player_one_stamina = _stamina;
//	global.player_one_health = _health;
//}



/// @arg player
var _player = argument0;
var _stamina = global.player_one_stamina;
var _max_stamina = global.player_one_max_stamina;
var _health = global.player_one_health;
var _max_health = global.player_one_max_health;
var _player_object = o_player_two;
var _item_0 = global.item[0];
var _item_1 = global.item[1];

if _player == 2 {
	_stamina = global.player_two_stamina;
	_max_stamina = global.player_two_max_stamina;
	_health = global.player_two_health;
	_max_health = global.player_two_max_health;
	_player_object = o_player_one;
	_item_0 = global.item[2];
	_item_1 = global.item[3];
}

if !instance_exists(_player_object) {
	if _player = 1 {
		global.current_player = 2;	
	} else {
		global.current_player = 1;	
	}
	exit;
}

//run towards main player
image_speed = 0.6;
//direction_ = point_direction(x, y, _player_object.x, _player_object.y);	
//roll_direction_ = point_direction(x, y, _player_object.x, _player_object.y);
//max_speed_ = 1.5;
//acceleration_ = 0.5;
var px, py;
px = instance_nearest(x, y, _player_object).x;
py = instance_nearest(x, y, _player_object).y;

var hx, hy;
hx = noone;
hy = noone;
if instance_exists(o_heart_pickup).x && instance_exists(o_heart_pickup).y && instance_exists(o_heart_pickup) {
	hx = instance_nearest(x, y, o_heart_pickup).x;
	hy = instance_nearest(x, y, o_heart_pickup).y;
}
////if main character is more than 42 pixels away and follower has evade equipped, roll towards main character
//if point_distance(x, y, px, py) > 42 && point_distance(x, y, px, py) < 24 && ((instance_exists(_item_0) && _item_0.action_ == player.evade && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.evade && _item_1.cost_ <= _stamina)) && point_distance(x + lengthdir_x(roll_speed_,roll_direction_), y + lengthdir_y(roll_speed_,roll_direction_), o_enemy.x + lengthdir_x(o_enemy.speed_, o_enemy.direction_), o_enemy.y + lengthdir_y(o_enemy.speed_, o_enemy.direction_)) > 64 {
//	set_movement(roll_direction_, 0.5);
//	set_sprite_facing();
//	get_direction_facing(roll_direction_);
//	if _item_0.action_ == player.evade {
//		state_ = _item_0.action_;
//		_stamina -= _item_0.cost_;
//		_stamina = max(0, _stamina);		
//		alarm[1] = global.one_second;
//	} else if _item_1.action_ == player.evade {
//		state_ = _item_1.action_;
//		_stamina -= _item_1.cost_;
//		_stamina = max(0, _stamina);		
//		alarm[1] = global.one_second;
//	}
//} else {
//	set_movement(direction_, 0.5);
//	set_sprite_facing();
//	get_direction_facing(direction_);	
//	if point_distance(x, y, px, py) > 24 || point_distance(x + lengthdir_x(speed_,direction_), y + lengthdir_y(speed_,direction_), o_enemy.x + lengthdir_x(o_enemy.speed_, o_enemy.direction_), o_enemy.y + lengthdir_y(o_enemy.speed_, o_enemy.direction_)) > 42 {	
//		add_movement_maxspeed(direction_, acceleration_, max_speed_);
//		move_movement_entity(true);
//	} else {
//		apply_friction_to_movement_entity();
//		image_speed = 0;
//	}
//}



//if enemy is closeby, attack or roll/run away if no weapons equipped
//fire ranged attack if enemy further away and character not moving
if instance_exists(o_enemy) {
	var ex, ey, e;
	ex = instance_nearest(x, y, o_enemy).x;
	ey = instance_nearest(x, y, o_enemy).y;		
	espeed_ = instance_nearest(x, y, o_enemy).speed_;
	edirection_ = instance_nearest(x, y, o_enemy).direction_;
	direction_ = point_direction(x, y, ex, ey);
	roll_direction_ = point_direction(x, y, _player_object.x, _player_object.y);		
	//pick up heart if nearby and low on health
	if point_distance(x, y, ex, ey) < 120 && point_distance(x, y, ex, ey) > 72 && ((instance_exists(_item_0) && _item_0.action_ == player.ranged && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.ranged && _item_1.cost_ <= _stamina)) {	
		direction_ = point_direction(x, y, ex, ey);
		set_sprite_facing();
		get_direction_facing(direction_);	
		move_movement_entity(true);	
		if _item_0.action_ == player.ranged {
			image_index = 0;
			state_ = _item_0.action_;
			_stamina -= _item_0.cost_;
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		} else {
			image_index = 0;
			state_ = _item_1.action_;
			_stamina -= _item_1.cost_;
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		}	
	} else if ((instance_exists(_item_0) && _item_0.action_ == player.sword || instance_exists(_item_1) && _item_1.action_ == player.sword) || (instance_exists(_item_0) && _item_0.action_ == player.melee || instance_exists(_item_1) && _item_1.action_ == player.melee)) && point_distance(x, y, ex + lengthdir_x(espeed_, edirection_), ey + lengthdir_y(espeed_, edirection_)) < 94 {
		if _stamina >= 3 && ((instance_exists(_item_0) && _item_0.action_ == player.evade && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.evade && _item_1.cost_ <= _stamina)) && point_distance(x + lengthdir_x(roll_speed_,roll_direction_), y + lengthdir_y(roll_speed_,roll_direction_), ex + lengthdir_x(espeed_, edirection_), ey + lengthdir_y(espeed_, edirection_)) > 36  {	
			//set_movement(roll_direction_, speed_);
			set_sprite_facing();
			get_direction_facing(roll_direction_);
			if _item_0.action_ == player.evade {
				image_index = 0;
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else if _item_1.action_ == player.evade {
				image_index = 0;
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			}
		} else if point_distance(x + lengthdir_x(speed_,direction_), y + lengthdir_y(speed_,direction_), ex + lengthdir_x(espeed_, edirection_), ey + lengthdir_y(espeed_, edirection_)) > 24 {
			image_speed = 0.6;
			direction_ = point_direction(x, y, ex, ey);
			set_movement(direction_, 1);
			set_sprite_facing();
			get_direction_facing(direction_);	
			add_movement_maxspeed(direction_, acceleration_, max_speed_);
			move_movement_entity(true);	
		} else if point_distance(x + lengthdir_x(speed_,direction_), y + lengthdir_y(speed_,direction_), ex + lengthdir_x(espeed_, edirection_), ey + lengthdir_y(espeed_, edirection_)) <= 24 {
			image_speed = 0.6;
			direction_ = point_direction(x, y, ex, ey);
			set_movement(direction_, 1);
			set_sprite_facing();
			get_direction_facing(direction_);
			apply_friction_to_movement_entity();
			if _item_0.action_ == player.sword && _item_0.cost_ <= _stamina {
				image_index = 0;
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else if _item_1.action_ == player.sword  && _item_1.cost_ <= _stamina {
				image_index = 0;
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else if _item_0.action_ == player.melee && _item_0.cost_ <= _stamina {
				image_index = 0;
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else if _item_1.action_ == player.melee && _item_1.cost_ <= _stamina {
				image_index = 0;
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			}
		}
	} else if  point_distance(x, y, px, py) > 52 && _stamina = _max_stamina && ((instance_exists(_item_0) && _item_0.action_ == player.evade && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.evade && _item_1.cost_ <= _stamina)) && point_distance(x + lengthdir_x(roll_speed_,roll_direction_), y + lengthdir_y(roll_speed_,roll_direction_), ex + lengthdir_x(espeed, edir), ey + lengthdir_y(espeed, edir)) > 72 {	
		//set_movement(roll_direction_, 0.5);
		set_sprite_facing();
		get_direction_facing(roll_direction_);
		if _item_0.action_ == player.evade {
			image_index = 0;
			state_ = _item_0.action_;
			_stamina -= _item_0.cost_;
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		} else if _item_1.action_ == player.evade {
			image_index = 0;
			state_ = _item_1.action_;
			_stamina -= _item_1.cost_;
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		}
	} else {
		direction_ = point_direction(x, y, _player_object.x, _player_object.y);
		set_sprite_facing();
		get_direction_facing(direction_);
		if point_distance(x, y, px, py) > 36 {	
			add_movement_maxspeed(direction_, acceleration_, max_speed_);
			move_movement_entity(true);
		} else {
			apply_friction_to_movement_entity();
			image_speed = 0;
			image_index = 0;
		}
	}
} else if hx != -4 {
	if point_distance(x, y, hx, hy) < 36 {
		roll_direction_ = point_direction(x, y, hx, hy);		
		direction_ = point_direction(x, y, hx, hy);
		if ((instance_exists(_item_0) && _item_0.action_ == player.evade && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.evade && _item_1.cost_ <= _stamina)) {
			set_sprite_facing();
			get_direction_facing(roll_direction_);
			if _item_0.action_ == player.evade {
				image_index = 0;
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
			} else {
				image_index = 0;
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
			}
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		} else {
			set_sprite_facing();
			get_direction_facing(direction_);
			add_movement_maxspeed(direction_, acceleration_, max_speed_);
			move_movement_entity(true);
		}
	} else {
		direction_ = point_direction(x, y, px, py);	
		set_sprite_facing();
		get_direction_facing(direction_);
		if point_distance(x, y, px, py) > 36 {	
			add_movement_maxspeed(direction_, acceleration_, max_speed_);
			move_movement_entity(true);
		} else {
			apply_friction_to_movement_entity();
			image_speed = 0;
			image_index = 0;
		}
	}
} else {
	if point_distance(x, y, px, py) > 52 && ((instance_exists(_item_0) && _item_0.action_ == player.evade && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.evade && _item_1.cost_ <= _stamina)) {		
		roll_direction_ = point_direction(x, y, _player_object.x, _player_object.y);
		set_sprite_facing();
		get_direction_facing(roll_direction_);
		if _item_0.action_ == player.evade {
				image_index = 0;
			state_ = _item_0.action_;
			_stamina -= _item_0.cost_;
		} else {
				image_index = 0;
			state_ = _item_1.action_;
			_stamina -= _item_1.cost_;
		}
		_stamina = max(0, _stamina);		
		alarm[1] = global.one_second;
	} else {
		direction_ = point_direction(x, y, _player_object.x, _player_object.y);
		//set_movement(direction_, 0.5);
		set_sprite_facing();
		get_direction_facing(direction_);
		if point_distance(x, y, px, py) > 36 {	
			add_movement_maxspeed(direction_, acceleration_, max_speed_);
			move_movement_entity(true);
		} else {
			apply_friction_to_movement_entity();
			image_speed = 0;
			image_index = 0;
		}
	}
}

if _player == 2 {
	global.player_two_stamina = _stamina;
	global.player_two_health = _health;
} else {
	global.player_one_stamina = _stamina;
	global.player_one_health = _health;
}


