{
    "id": "2246c880-016b-4abb-8540-4658b3157e57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_run_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a68398d6-7e6a-40d5-a9e6-557dba84f1a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2246c880-016b-4abb-8540-4658b3157e57",
            "compositeImage": {
                "id": "181afd85-da65-4674-9479-58af868a1d88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a68398d6-7e6a-40d5-a9e6-557dba84f1a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93931c1d-74cb-41aa-8a25-2f0032a584a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a68398d6-7e6a-40d5-a9e6-557dba84f1a5",
                    "LayerId": "84821914-4278-490f-b300-441c54769633"
                }
            ]
        },
        {
            "id": "028fffd1-bbb4-4d7b-96a4-c8165763b044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2246c880-016b-4abb-8540-4658b3157e57",
            "compositeImage": {
                "id": "92da0834-bc47-48bd-a9c5-62b15043c962",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "028fffd1-bbb4-4d7b-96a4-c8165763b044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfdac4bc-979e-4a02-8319-8da751e34cfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "028fffd1-bbb4-4d7b-96a4-c8165763b044",
                    "LayerId": "84821914-4278-490f-b300-441c54769633"
                }
            ]
        },
        {
            "id": "19261076-a8cc-4c0e-acfa-4300d4bb79eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2246c880-016b-4abb-8540-4658b3157e57",
            "compositeImage": {
                "id": "7c1f665f-6485-4132-b34c-ad283a9d8a40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19261076-a8cc-4c0e-acfa-4300d4bb79eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bd8a0d9-0a6b-46f5-b3d6-cd5a6dda5964",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19261076-a8cc-4c0e-acfa-4300d4bb79eb",
                    "LayerId": "84821914-4278-490f-b300-441c54769633"
                }
            ]
        },
        {
            "id": "a94fc262-ef10-44e6-bf49-35d6e0d50ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2246c880-016b-4abb-8540-4658b3157e57",
            "compositeImage": {
                "id": "4226c147-54ed-4415-a69d-65844daf9820",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a94fc262-ef10-44e6-bf49-35d6e0d50ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85c6c10e-89ff-4740-ae79-3e46fa0fdf5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a94fc262-ef10-44e6-bf49-35d6e0d50ce3",
                    "LayerId": "84821914-4278-490f-b300-441c54769633"
                }
            ]
        },
        {
            "id": "016299bd-35e2-41c2-8ffd-3fec765507dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2246c880-016b-4abb-8540-4658b3157e57",
            "compositeImage": {
                "id": "55905e16-aaa3-455c-933b-cb9e97c889f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "016299bd-35e2-41c2-8ffd-3fec765507dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f069e5f-7b6c-4b28-a523-2f4af7a85e2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "016299bd-35e2-41c2-8ffd-3fec765507dd",
                    "LayerId": "84821914-4278-490f-b300-441c54769633"
                }
            ]
        },
        {
            "id": "26970844-c3e6-4166-b333-d96b25b6e1b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2246c880-016b-4abb-8540-4658b3157e57",
            "compositeImage": {
                "id": "2f968b91-a8f1-47ce-b6c3-d998cb7b9de2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26970844-c3e6-4166-b333-d96b25b6e1b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26617a93-88d0-4a7f-9e05-fea0ef25b4ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26970844-c3e6-4166-b333-d96b25b6e1b1",
                    "LayerId": "84821914-4278-490f-b300-441c54769633"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "84821914-4278-490f-b300-441c54769633",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2246c880-016b-4abb-8540-4658b3157e57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 24
}