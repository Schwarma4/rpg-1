{
    "id": "4858c63e-d3b3-4ab1-b40b-cefa40697487",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_grass_effect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eafd8379-cb75-4aed-9108-ab3c8cce8826",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4858c63e-d3b3-4ab1-b40b-cefa40697487",
            "compositeImage": {
                "id": "d158cdbb-c9ef-497e-8239-6789a626efad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eafd8379-cb75-4aed-9108-ab3c8cce8826",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b198a80a-0f6a-4842-95d3-afb6fba1dd9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eafd8379-cb75-4aed-9108-ab3c8cce8826",
                    "LayerId": "1ffbbf10-16a2-43ac-a81c-21758cb7a656"
                }
            ]
        },
        {
            "id": "3cf0f253-76dc-4880-871c-95eec0149d9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4858c63e-d3b3-4ab1-b40b-cefa40697487",
            "compositeImage": {
                "id": "6d65287c-e84b-4d71-8e9b-61322233d283",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf0f253-76dc-4880-871c-95eec0149d9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6256a07-28c9-4b1c-b98f-2c2d0babcdb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf0f253-76dc-4880-871c-95eec0149d9a",
                    "LayerId": "1ffbbf10-16a2-43ac-a81c-21758cb7a656"
                }
            ]
        },
        {
            "id": "a7e9f220-53f4-47d6-9168-c97d3f3d224b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4858c63e-d3b3-4ab1-b40b-cefa40697487",
            "compositeImage": {
                "id": "7e10b29e-28c1-4614-891e-bc875126e617",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7e9f220-53f4-47d6-9168-c97d3f3d224b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2c7d88a-6778-4870-b1d7-52069966942b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7e9f220-53f4-47d6-9168-c97d3f3d224b",
                    "LayerId": "1ffbbf10-16a2-43ac-a81c-21758cb7a656"
                }
            ]
        },
        {
            "id": "8a2095de-eeb4-489a-be02-e6f399b7445d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4858c63e-d3b3-4ab1-b40b-cefa40697487",
            "compositeImage": {
                "id": "e8f8c404-fbb2-4365-bac4-1f3e1beb30dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a2095de-eeb4-489a-be02-e6f399b7445d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89a89cac-6d62-4395-957d-e8289df2d5b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a2095de-eeb4-489a-be02-e6f399b7445d",
                    "LayerId": "1ffbbf10-16a2-43ac-a81c-21758cb7a656"
                }
            ]
        },
        {
            "id": "a47a7f11-2cf8-47a7-877d-c613bdb2490b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4858c63e-d3b3-4ab1-b40b-cefa40697487",
            "compositeImage": {
                "id": "c5791528-8cbe-4246-b467-7ccc9f2885e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a47a7f11-2cf8-47a7-877d-c613bdb2490b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37463a49-18c6-472e-aa1f-83e79aa369b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a47a7f11-2cf8-47a7-877d-c613bdb2490b",
                    "LayerId": "1ffbbf10-16a2-43ac-a81c-21758cb7a656"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1ffbbf10-16a2-43ac-a81c-21758cb7a656",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4858c63e-d3b3-4ab1-b40b-cefa40697487",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 8,
    "yorig": 8
}