{
    "id": "91024126-54cf-4023-ab7e-676f1c19a5ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bomb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3b2d2e0-065f-4372-a417-c7f2408c93e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91024126-54cf-4023-ab7e-676f1c19a5ce",
            "compositeImage": {
                "id": "6b1d29db-6295-41ad-a714-1581dc20dcf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3b2d2e0-065f-4372-a417-c7f2408c93e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6023f5a4-cf98-468c-8dd8-29cc31ab8782",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3b2d2e0-065f-4372-a417-c7f2408c93e0",
                    "LayerId": "fdb4c2a0-859b-4739-8a83-8db7f2b35302"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "fdb4c2a0-859b-4739-8a83-8db7f2b35302",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91024126-54cf-4023-ab7e-676f1c19a5ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 9
}