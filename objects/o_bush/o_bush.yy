{
    "id": "f5751835-a82b-4d1e-99d0-bee357988164",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bush",
    "eventList": [
        {
            "id": "0ff78cd0-5c17-45d4-8557-7f04cb860a5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f5751835-a82b-4d1e-99d0-bee357988164"
        },
        {
            "id": "4547b97a-bfb7-4f5c-b3f0-79504942bc48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8704a8a6-2273-4874-8986-2f1e41dc5331",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f5751835-a82b-4d1e-99d0-bee357988164"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ffaef82b-5efb-4be1-a2ba-919ff82874f5",
    "visible": true
}