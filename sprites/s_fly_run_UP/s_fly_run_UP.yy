{
    "id": "6a191e41-aa69-4637-9eb2-3a66f414b604",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_fly_run_UP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 23,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3dfb8725-c625-4534-8680-9c7297e2342c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "a279c16e-dc02-4b30-9086-dbf9f2f9b417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dfb8725-c625-4534-8680-9c7297e2342c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a692faa-9631-4c21-9480-b25f62ae4f5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dfb8725-c625-4534-8680-9c7297e2342c",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        },
        {
            "id": "7a9c747e-77c2-4baf-84f0-aed8fed682ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "b4929cc6-b40f-4db6-a376-31d0dfe624e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a9c747e-77c2-4baf-84f0-aed8fed682ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6af69c17-386f-4ade-956a-1ad982b24f59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a9c747e-77c2-4baf-84f0-aed8fed682ea",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        },
        {
            "id": "4c82f027-57f6-401c-8a5a-8cedd3313537",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "de977be9-e34f-453e-a975-8133bef08557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c82f027-57f6-401c-8a5a-8cedd3313537",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d6d213f-6dab-4536-a08f-ec327fee8ff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c82f027-57f6-401c-8a5a-8cedd3313537",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        },
        {
            "id": "cc7c5d49-dd3d-449e-96b1-1dbf89c7e2cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "d1e9b475-babd-4122-973a-ad736db670ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc7c5d49-dd3d-449e-96b1-1dbf89c7e2cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "429aad86-0dbe-40ea-8d79-d518c048e288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc7c5d49-dd3d-449e-96b1-1dbf89c7e2cb",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        },
        {
            "id": "d27d6661-7658-438b-b7e6-7f46f5c847f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "a9dca7a7-3f35-4d82-860f-04f8350173c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d27d6661-7658-438b-b7e6-7f46f5c847f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9fc177b-4331-44f3-8724-43008d749f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d27d6661-7658-438b-b7e6-7f46f5c847f8",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        },
        {
            "id": "6b55af40-7e84-4544-bb1a-0a4ca0c1827e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "2705eb0d-b695-4b76-bd22-0e598c627e24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b55af40-7e84-4544-bb1a-0a4ca0c1827e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aa237a1-98e9-4d8a-83e1-b4b22e26c7ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b55af40-7e84-4544-bb1a-0a4ca0c1827e",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        },
        {
            "id": "5b6f2867-33e9-4226-b784-e174288b70bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "ecf7844e-cf31-4e73-a6b5-e4cfa004051f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b6f2867-33e9-4226-b784-e174288b70bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "529270ca-0c9b-43f7-993a-13226c54b50a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b6f2867-33e9-4226-b784-e174288b70bd",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        },
        {
            "id": "19d022cd-b2a8-4620-8398-89133011de80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "dfb82bb8-2ecb-4351-85cd-a686816c16f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19d022cd-b2a8-4620-8398-89133011de80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c971dfc8-eca7-49c4-bd97-2817a5469ffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19d022cd-b2a8-4620-8398-89133011de80",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        },
        {
            "id": "c655b9c8-f1f7-4ea7-a3b5-241630a34a8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "88d47047-f126-41d9-afd7-4dce07769f6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c655b9c8-f1f7-4ea7-a3b5-241630a34a8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27739784-70af-49e9-bce5-836aba07b56c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c655b9c8-f1f7-4ea7-a3b5-241630a34a8e",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        },
        {
            "id": "6c3895fa-7f91-414f-b782-82f02e6cee75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "dba8f809-ebce-401b-b769-6a4f253bd80b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c3895fa-7f91-414f-b782-82f02e6cee75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60f34480-7aff-48d8-8334-3a477d183021",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c3895fa-7f91-414f-b782-82f02e6cee75",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        },
        {
            "id": "d5c41740-78ff-4882-9062-6fd83ccd4bed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "b84da56c-678b-480e-b827-2e86e271e555",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5c41740-78ff-4882-9062-6fd83ccd4bed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "830c729a-613b-4e38-ae94-328a1ea80f19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5c41740-78ff-4882-9062-6fd83ccd4bed",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        },
        {
            "id": "3290c87c-1fe6-48b0-9ac0-a3b8c4734039",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "82f5ebf5-98b0-4f76-9040-a29d525cc2cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3290c87c-1fe6-48b0-9ac0-a3b8c4734039",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8562188c-7b6f-411a-8756-78066cd343b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3290c87c-1fe6-48b0-9ac0-a3b8c4734039",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        },
        {
            "id": "f8c7c769-895b-4ac1-a86b-b1456aa26680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "127b968f-674c-4f51-93b9-2505b0bdc35d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8c7c769-895b-4ac1-a86b-b1456aa26680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "916ddfd9-a10b-4d34-b9ed-b783e063450c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8c7c769-895b-4ac1-a86b-b1456aa26680",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        },
        {
            "id": "edd48c4a-e4b7-4779-8563-6da94ef49b96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "compositeImage": {
                "id": "0bf54e2b-876e-4d5d-b5a0-c9088d426b3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edd48c4a-e4b7-4779-8563-6da94ef49b96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8665a06-919c-4d26-be17-7f247965de26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edd48c4a-e4b7-4779-8563-6da94ef49b96",
                    "LayerId": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "adbd4245-7f33-4ace-9bb8-f8f9e5bce7d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a191e41-aa69-4637-9eb2-3a66f414b604",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 23
}