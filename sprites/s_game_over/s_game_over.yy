{
    "id": "aaad0f3c-38c9-41a6-b131-42724adead86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_game_over",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19c5ef3f-17fe-4b41-9015-f23cb8ae622a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aaad0f3c-38c9-41a6-b131-42724adead86",
            "compositeImage": {
                "id": "47119ffd-55d4-4dc8-ba57-0bf98ead560f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19c5ef3f-17fe-4b41-9015-f23cb8ae622a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fff5f39-1195-4b02-8710-1f40e91b80b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19c5ef3f-17fe-4b41-9015-f23cb8ae622a",
                    "LayerId": "10848963-a41c-452e-8592-d5d94b284f72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "10848963-a41c-452e-8592-d5d94b284f72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aaad0f3c-38c9-41a6-b131-42724adead86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}