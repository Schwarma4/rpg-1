{
    "id": "8704a8a6-2273-4874-8986-2f1e41dc5331",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_hitbox",
    "eventList": [
        {
            "id": "70cb0ea0-9a21-465b-bae3-fc6d708edf93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8704a8a6-2273-4874-8986-2f1e41dc5331"
        },
        {
            "id": "57295911-0968-4010-87cc-0d913bbda7a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8704a8a6-2273-4874-8986-2f1e41dc5331"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d872fc10-5d61-48ea-841a-c27672c3381d",
    "visible": false
}