{
    "id": "c8db8f59-96f0-4f63-bd25-46804e673b61",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Metal_Thing_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "409f7b93-5dae-48d6-8bf8-d65706e99c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8db8f59-96f0-4f63-bd25-46804e673b61",
            "compositeImage": {
                "id": "46486767-05ab-4649-8394-e348513df903",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "409f7b93-5dae-48d6-8bf8-d65706e99c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48732163-6de7-42a7-8ec8-8e7c398928f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "409f7b93-5dae-48d6-8bf8-d65706e99c56",
                    "LayerId": "6e509760-6dfe-44a8-b215-0ac191dbe15b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6e509760-6dfe-44a8-b215-0ac191dbe15b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8db8f59-96f0-4f63-bd25-46804e673b61",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}