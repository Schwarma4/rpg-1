{
    "id": "0f5cb83b-4a3d-4bd8-b2d3-1cd95e13b085",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Metal_Thing_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a84d351c-8ac1-4620-8d9b-3eb93e8f9f19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f5cb83b-4a3d-4bd8-b2d3-1cd95e13b085",
            "compositeImage": {
                "id": "49193107-3482-4b69-b559-4a9d214390cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a84d351c-8ac1-4620-8d9b-3eb93e8f9f19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a322dcdf-f626-418f-9978-b96d7007c6b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a84d351c-8ac1-4620-8d9b-3eb93e8f9f19",
                    "LayerId": "1e3ef1f1-520c-40b2-a707-0c1db5323ff7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1e3ef1f1-520c-40b2-a707-0c1db5323ff7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f5cb83b-4a3d-4bd8-b2d3-1cd95e13b085",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}