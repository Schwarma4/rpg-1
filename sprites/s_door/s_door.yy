{
    "id": "dc08717f-f862-40dc-ab8e-87c2b8e38533",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5f21bdc-5428-4d93-bb2f-4abef9e39528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc08717f-f862-40dc-ab8e-87c2b8e38533",
            "compositeImage": {
                "id": "e84ed424-412a-437b-8380-e55856ee3a9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5f21bdc-5428-4d93-bb2f-4abef9e39528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ac6d638-02b9-43b8-b415-2cea17182ef7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5f21bdc-5428-4d93-bb2f-4abef9e39528",
                    "LayerId": "997d2873-d5fc-4f96-9e13-b7a5a191ea27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "997d2873-d5fc-4f96-9e13-b7a5a191ea27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc08717f-f862-40dc-ab8e-87c2b8e38533",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}