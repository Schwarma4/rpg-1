{
    "id": "fdfecec8-8a40-4b3a-8b36-c3013601987c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_cat_roper",
    "eventList": [
        {
            "id": "b2c59306-497c-49f4-9147-26eb2a23a50f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fdfecec8-8a40-4b3a-8b36-c3013601987c"
        },
        {
            "id": "516f5981-9c47-4c6a-b9db-d3112145f845",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "fdfecec8-8a40-4b3a-8b36-c3013601987c"
        },
        {
            "id": "ce61de58-e334-443b-839f-a46d3f6540a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "fdfecec8-8a40-4b3a-8b36-c3013601987c"
        },
        {
            "id": "90404c2a-1ac5-4e04-9c15-e31093fa7aa0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fdfecec8-8a40-4b3a-8b36-c3013601987c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "046d396d-2913-4110-9e0b-8ba8614cd420",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "826bc2d7-6180-4d41-8755-e8cab20a7448",
    "visible": true
}