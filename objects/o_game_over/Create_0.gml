enum gameover_options {
	continue_game,
	restart,
	quit
}

menu_color_ = make_color_rgb(247, 243, 143);
menu_dark_color_ = make_color_rgb(126, 127, 81);

gameover_options_[gameover_options.continue_game] = "Continue";
gameover_options_[gameover_options.restart] = "Restart";
gameover_options_[gameover_options.quit] = "Quit";

gameover_options_length_ = array_length_1d(gameover_options_);

index_ = gameover_options.continue_game;

