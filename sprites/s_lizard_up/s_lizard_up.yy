{
    "id": "04cc1c8b-7eec-49e0-bc40-62ad4f9e2622",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_lizard_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 22,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ca10c4b-3215-49f8-837c-9ef978f94c53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04cc1c8b-7eec-49e0-bc40-62ad4f9e2622",
            "compositeImage": {
                "id": "13746e10-27b9-4beb-a062-3fd528cc14fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ca10c4b-3215-49f8-837c-9ef978f94c53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e47fb0b-bafb-4560-a5d6-7f56daf898e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ca10c4b-3215-49f8-837c-9ef978f94c53",
                    "LayerId": "54c1ef3b-586b-4ba7-8141-170843bd311f"
                }
            ]
        },
        {
            "id": "fbd7e619-5169-4048-b6bc-05c18043cc04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04cc1c8b-7eec-49e0-bc40-62ad4f9e2622",
            "compositeImage": {
                "id": "07c014d9-4c0c-4a62-b2f9-42e17a6b1a35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbd7e619-5169-4048-b6bc-05c18043cc04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "228ea552-f78b-4f2b-ab5c-9d1043aa9dc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbd7e619-5169-4048-b6bc-05c18043cc04",
                    "LayerId": "54c1ef3b-586b-4ba7-8141-170843bd311f"
                }
            ]
        },
        {
            "id": "00c9f4b4-64c9-4456-baba-b03a84a6c23b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04cc1c8b-7eec-49e0-bc40-62ad4f9e2622",
            "compositeImage": {
                "id": "cf95fc63-bf06-4f42-8b10-93da6f3252dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00c9f4b4-64c9-4456-baba-b03a84a6c23b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15ffcac5-e16e-4093-b87b-6e1363cdc4ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00c9f4b4-64c9-4456-baba-b03a84a6c23b",
                    "LayerId": "54c1ef3b-586b-4ba7-8141-170843bd311f"
                }
            ]
        },
        {
            "id": "da9127f5-05c9-48d2-a286-4fa4189a5ac5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04cc1c8b-7eec-49e0-bc40-62ad4f9e2622",
            "compositeImage": {
                "id": "b20dbae1-d655-4271-859c-5036dbc29bb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da9127f5-05c9-48d2-a286-4fa4189a5ac5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "345d2438-e12d-478a-aa78-bc99ad4eb0dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da9127f5-05c9-48d2-a286-4fa4189a5ac5",
                    "LayerId": "54c1ef3b-586b-4ba7-8141-170843bd311f"
                }
            ]
        },
        {
            "id": "1593f84f-5e6b-4bfc-8954-c9d88240ead0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04cc1c8b-7eec-49e0-bc40-62ad4f9e2622",
            "compositeImage": {
                "id": "0a7ed4ce-3227-44ae-805d-5c7841af61b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1593f84f-5e6b-4bfc-8954-c9d88240ead0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77d9e75b-84dc-46c4-b592-0c03af65084c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1593f84f-5e6b-4bfc-8954-c9d88240ead0",
                    "LayerId": "54c1ef3b-586b-4ba7-8141-170843bd311f"
                }
            ]
        },
        {
            "id": "2f721f89-c2ce-427c-90ca-5b0b88b987c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04cc1c8b-7eec-49e0-bc40-62ad4f9e2622",
            "compositeImage": {
                "id": "8b7fbb5f-078c-4324-8660-5a64a33e25a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f721f89-c2ce-427c-90ca-5b0b88b987c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d348f3c-a889-4ba3-b4d4-7281131576ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f721f89-c2ce-427c-90ca-5b0b88b987c5",
                    "LayerId": "54c1ef3b-586b-4ba7-8141-170843bd311f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "54c1ef3b-586b-4ba7-8141-170843bd311f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04cc1c8b-7eec-49e0-bc40-62ad4f9e2622",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}