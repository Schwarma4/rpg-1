{
    "id": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_FLY_run_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e1dab2c-d4f0-460a-ab75-b48246c78ac5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "7e225528-09cc-4328-b0a2-b68347e0108b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e1dab2c-d4f0-460a-ab75-b48246c78ac5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "216dfefe-e188-4477-9b59-d5ab833a1928",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e1dab2c-d4f0-460a-ab75-b48246c78ac5",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        },
        {
            "id": "8b0d98d7-72ec-4041-886a-0b76ef2ef056",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "48a9ea57-b3a0-474a-a782-5c42e50f5f29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b0d98d7-72ec-4041-886a-0b76ef2ef056",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "504dbeaa-5b62-48e1-98f0-fa7b3ab2e16f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b0d98d7-72ec-4041-886a-0b76ef2ef056",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        },
        {
            "id": "15a132d7-e4ca-4858-9a15-74fc21fbb31c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "66cdd5bd-326f-4549-ab23-7752705d9bf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15a132d7-e4ca-4858-9a15-74fc21fbb31c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "881f6996-6387-4e74-9446-f9a3891d3267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15a132d7-e4ca-4858-9a15-74fc21fbb31c",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        },
        {
            "id": "dce27485-4d16-48d9-b935-115d90d70304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "c9baf9fd-d954-4fbd-b857-3ed109f1c838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce27485-4d16-48d9-b935-115d90d70304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fc32aff-9641-4eee-b3d6-954bfe8d38d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce27485-4d16-48d9-b935-115d90d70304",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        },
        {
            "id": "ad2c6530-abed-403f-ae43-a74ff19a2558",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "960ddaa3-624f-46e7-9dcc-86f3ff58b661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad2c6530-abed-403f-ae43-a74ff19a2558",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abb1655a-10d9-4ed8-9c9f-dcebad67614a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad2c6530-abed-403f-ae43-a74ff19a2558",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        },
        {
            "id": "eb04fe9b-fcc3-4b5f-90ec-cbefb6169bd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "2b4cd2e2-9a0d-44c1-944d-48af364da499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb04fe9b-fcc3-4b5f-90ec-cbefb6169bd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbd33254-fbd8-454b-bf50-6e8cd6eff890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb04fe9b-fcc3-4b5f-90ec-cbefb6169bd4",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        },
        {
            "id": "6365ee0b-377c-4f1d-8752-8e44c9254a72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "14401703-830c-48f4-924c-f82bc33b95e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6365ee0b-377c-4f1d-8752-8e44c9254a72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eb8f06b-d1e2-4152-9b17-11c323f98959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6365ee0b-377c-4f1d-8752-8e44c9254a72",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        },
        {
            "id": "32ee721c-2a67-4c1a-86f4-b1a655227eb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "ab812432-71a7-47a3-92d9-cb6af33a5f9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32ee721c-2a67-4c1a-86f4-b1a655227eb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "864e4734-35a7-40db-8994-03b9ced78fa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32ee721c-2a67-4c1a-86f4-b1a655227eb7",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        },
        {
            "id": "86552c0b-4677-4142-92ea-4e4eb5aeba9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "d0569f7d-5e21-4089-89ba-406bb9d8631e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86552c0b-4677-4142-92ea-4e4eb5aeba9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ac84907-abe5-4b6f-a49b-8178afa343dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86552c0b-4677-4142-92ea-4e4eb5aeba9b",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        },
        {
            "id": "8cf82b7f-1ec3-4d4d-ba2d-40e7f9a1ef5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "5fb34822-a792-4b04-b3fd-78541841eb07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cf82b7f-1ec3-4d4d-ba2d-40e7f9a1ef5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9be1be11-405a-453a-ba76-beeca0fb4730",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cf82b7f-1ec3-4d4d-ba2d-40e7f9a1ef5b",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        },
        {
            "id": "8dbd95aa-b2d5-4f1d-95d2-4a41fe4784a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "682f0702-a953-459f-83e0-9cb6491d371f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dbd95aa-b2d5-4f1d-95d2-4a41fe4784a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea80e00d-9255-4d17-9054-acd2fb267f7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dbd95aa-b2d5-4f1d-95d2-4a41fe4784a1",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        },
        {
            "id": "ff259032-e093-47d1-87ad-c60c0f04f25e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "3adc033a-aa51-4d0a-916e-99300ddcf6c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff259032-e093-47d1-87ad-c60c0f04f25e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3874581c-038a-4310-9217-d60af022bc75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff259032-e093-47d1-87ad-c60c0f04f25e",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        },
        {
            "id": "81cdc2f8-2830-469c-89fa-a45b04d24d52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "b353eede-945b-493b-90c7-022da3963305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81cdc2f8-2830-469c-89fa-a45b04d24d52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6276f174-9c3d-44db-befa-9bba64f22517",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81cdc2f8-2830-469c-89fa-a45b04d24d52",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        },
        {
            "id": "7f02d44e-ce76-4103-a668-56eb014d533a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "compositeImage": {
                "id": "3c74b793-a62e-4254-be89-2e8d32e446cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f02d44e-ce76-4103-a668-56eb014d533a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "134bd36a-a446-42b9-a46b-311872e7954f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f02d44e-ce76-4103-a668-56eb014d533a",
                    "LayerId": "e16080f8-b902-418f-9429-627c50e6d24e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "e16080f8-b902-418f-9429-627c50e6d24e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2d8ba92-7b2a-4ad2-9f19-acf8946d2e76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 23
}