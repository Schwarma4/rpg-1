{
    "id": "8d9b3de7-7f05-41d6-a10f-fa09a6d36aed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_lizard_frank",
    "eventList": [
        {
            "id": "0edd7936-3000-4c3a-a4a1-90b96d853b1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d9b3de7-7f05-41d6-a10f-fa09a6d36aed"
        },
        {
            "id": "a9dc42fb-20ea-483c-96a9-aad1812b14f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "8d9b3de7-7f05-41d6-a10f-fa09a6d36aed"
        },
        {
            "id": "fc02f4cf-875c-47d1-bad9-751a3558e2c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "8d9b3de7-7f05-41d6-a10f-fa09a6d36aed"
        },
        {
            "id": "86abd33c-42a8-4e9c-8966-70368e3bebf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8d9b3de7-7f05-41d6-a10f-fa09a6d36aed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "046d396d-2913-4110-9e0b-8ba8614cd420",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2391dae9-336d-46fc-98d7-108839af4683",
    "visible": true
}