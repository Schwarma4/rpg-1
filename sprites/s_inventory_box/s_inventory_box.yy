{
    "id": "ef223739-82bc-48cc-91ef-017db696fefc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_inventory_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a568018-80f0-4e8b-80b5-5ce131e2e695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef223739-82bc-48cc-91ef-017db696fefc",
            "compositeImage": {
                "id": "add1981d-8e62-4e0f-bea0-5cd860352abf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a568018-80f0-4e8b-80b5-5ce131e2e695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a986eff1-dc26-416d-839b-d7750b8d9151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a568018-80f0-4e8b-80b5-5ce131e2e695",
                    "LayerId": "c1927a57-581c-4afc-bdc5-32151e632e4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c1927a57-581c-4afc-bdc5-32151e632e4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef223739-82bc-48cc-91ef-017db696fefc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}