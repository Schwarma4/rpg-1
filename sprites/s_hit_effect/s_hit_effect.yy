{
    "id": "599f267b-1dd9-4022-be1d-72d0a9a1078e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hit_effect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1beb4cb3-1b9a-436d-ab42-8a1477f62fb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "599f267b-1dd9-4022-be1d-72d0a9a1078e",
            "compositeImage": {
                "id": "6c93fc59-5847-45ac-a5e9-5a23c0af43f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1beb4cb3-1b9a-436d-ab42-8a1477f62fb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49476bc0-ada7-420d-912e-b23f75f916c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1beb4cb3-1b9a-436d-ab42-8a1477f62fb4",
                    "LayerId": "b3e0973a-20b5-4fbe-b3da-b0013d89a22b"
                }
            ]
        },
        {
            "id": "a7cc5fcf-dce3-489a-bf82-c19148383bac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "599f267b-1dd9-4022-be1d-72d0a9a1078e",
            "compositeImage": {
                "id": "4a80d514-83a0-4e30-9db6-a123abf177ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7cc5fcf-dce3-489a-bf82-c19148383bac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8375fdaa-ad49-41ef-8664-a40cb2352fcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7cc5fcf-dce3-489a-bf82-c19148383bac",
                    "LayerId": "b3e0973a-20b5-4fbe-b3da-b0013d89a22b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "b3e0973a-20b5-4fbe-b3da-b0013d89a22b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "599f267b-1dd9-4022-be1d-72d0a9a1078e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}