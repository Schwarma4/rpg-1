{
    "id": "50bc1eba-82e6-4fc4-809f-fa0f9a206a96",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_forest_tree",
    "eventList": [
        {
            "id": "703c76f6-b815-4c1e-96d7-3b9693fd053d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "50bc1eba-82e6-4fc4-809f-fa0f9a206a96"
        },
        {
            "id": "68d2a68d-8e31-403b-99fa-3e2a90e35f75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8704a8a6-2273-4874-8986-2f1e41dc5331",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "50bc1eba-82e6-4fc4-809f-fa0f9a206a96"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9b5fe0a9-1b00-4359-b606-f9fcc08bed19",
    "visible": true
}