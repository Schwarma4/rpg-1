{
    "id": "2adaec66-ad59-4f18-a19c-856caaf707d0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_stinger",
    "eventList": [
        {
            "id": "3ebe8cf6-d749-47be-a668-fe5a81f11e98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2adaec66-ad59-4f18-a19c-856caaf707d0"
        },
        {
            "id": "12f2838c-e868-48e7-8de4-90dabf86d02d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2adaec66-ad59-4f18-a19c-856caaf707d0"
        },
        {
            "id": "ece4ea58-1703-44c1-a400-fa76c7d52ddf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2adaec66-ad59-4f18-a19c-856caaf707d0"
        },
        {
            "id": "cb52f050-ebe8-485e-8dc2-3cd5a6db9e85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8b6eac83-532b-4339-b7ae-34b6c460fbbd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2adaec66-ad59-4f18-a19c-856caaf707d0"
        },
        {
            "id": "ae557023-d30f-48dd-a75c-855e55cc0e35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "65171c23-635c-480e-ab31-eaea4af5313f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2adaec66-ad59-4f18-a19c-856caaf707d0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8704a8a6-2273-4874-8986-2f1e41dc5331",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "95cb6e04-362d-474f-88e7-727552b9a2a5",
    "visible": true
}