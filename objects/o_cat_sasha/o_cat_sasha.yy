{
    "id": "beb039e2-f878-4bac-95c0-2b53a201db93",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_cat_sasha",
    "eventList": [
        {
            "id": "12372c97-6884-4510-9a53-a19c5652a13c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "beb039e2-f878-4bac-95c0-2b53a201db93"
        },
        {
            "id": "043c73a6-4692-42e8-9f50-f9cf2d5e0db7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "beb039e2-f878-4bac-95c0-2b53a201db93"
        },
        {
            "id": "977e32b4-e881-41dd-b69a-b1a5934dfa6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "beb039e2-f878-4bac-95c0-2b53a201db93"
        },
        {
            "id": "fe7c91bb-c29a-42b2-8591-7624932004ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "beb039e2-f878-4bac-95c0-2b53a201db93"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "046d396d-2913-4110-9e0b-8ba8614cd420",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6fb4bf5d-e836-4f0b-b277-99c48fce6f4b",
    "visible": true
}