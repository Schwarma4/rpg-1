{
    "id": "139322a5-2ec5-4c61-a3a9-78e4162ec82f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_door",
    "eventList": [
        {
            "id": "35dbe182-2426-484a-b9b6-860e7bf8d0f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "139322a5-2ec5-4c61-a3a9-78e4162ec82f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dc08717f-f862-40dc-ab8e-87c2b8e38533",
    "visible": false
}