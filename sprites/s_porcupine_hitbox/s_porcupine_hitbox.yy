{
    "id": "ce4deecc-099d-417c-a303-c4d7b892ebb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_porcupine_hitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f204b67c-18c2-4c60-8b6f-faf45bb0baef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce4deecc-099d-417c-a303-c4d7b892ebb9",
            "compositeImage": {
                "id": "4cc8c601-4f45-4905-bb88-429c9b088511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f204b67c-18c2-4c60-8b6f-faf45bb0baef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af3b7406-a218-4f50-8efe-5b6ba3ad3571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f204b67c-18c2-4c60-8b6f-faf45bb0baef",
                    "LayerId": "a061e7f0-50ec-4780-816c-fb83be84c491"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "a061e7f0-50ec-4780-816c-fb83be84c491",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce4deecc-099d-417c-a303-c4d7b892ebb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}