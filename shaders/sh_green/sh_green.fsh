//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
	//Get original color of the pixel
	vec4 OriginalColor = texture2D( gm_BaseTexture, v_vTexcoord );
	
	float Red = 0.0;
	float Blue = 0.0;
	float Alpha = 1.0;
	float Green = OriginalColor.g;
	
	vec4 OutputColor = vec4(Red, Green, Blue, Alpha);
	
    gl_FragColor = OutputColor; 
}
