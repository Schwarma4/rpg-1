{
    "id": "9b5fe0a9-1b00-4359-b606-f9fcc08bed19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dark_tree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 16,
    "bbox_right": 28,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72a2c3a8-a7a0-440e-a627-9c5e8b00ff81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b5fe0a9-1b00-4359-b606-f9fcc08bed19",
            "compositeImage": {
                "id": "eb4891cf-8ff4-47d6-8326-d007f08c86a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72a2c3a8-a7a0-440e-a627-9c5e8b00ff81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "043941de-3d43-4f1c-86a7-7524e6578748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72a2c3a8-a7a0-440e-a627-9c5e8b00ff81",
                    "LayerId": "97ce44ce-f595-41c6-9b0e-d6b573b1644a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "97ce44ce-f595-41c6-9b0e-d6b573b1644a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b5fe0a9-1b00-4359-b606-f9fcc08bed19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 23,
    "yorig": 57
}