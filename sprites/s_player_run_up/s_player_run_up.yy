{
    "id": "c75cef2e-63f8-4caf-9c08-5f347529b283",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_run_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2938b4e3-b9fe-4032-9a88-a26bcae1453a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c75cef2e-63f8-4caf-9c08-5f347529b283",
            "compositeImage": {
                "id": "e06a0471-379e-4bc6-af20-6b76d2119867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2938b4e3-b9fe-4032-9a88-a26bcae1453a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f7395ff-4f3b-4203-bcdb-69cfb32fa0a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2938b4e3-b9fe-4032-9a88-a26bcae1453a",
                    "LayerId": "447968f1-58e2-4b6d-98f5-35c408e660c6"
                }
            ]
        },
        {
            "id": "099bc387-4890-4918-9632-b9de835082b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c75cef2e-63f8-4caf-9c08-5f347529b283",
            "compositeImage": {
                "id": "045f8793-3852-4594-a458-d64153071ef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "099bc387-4890-4918-9632-b9de835082b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36d36290-4b3f-44b5-b339-ed854ca7766b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "099bc387-4890-4918-9632-b9de835082b8",
                    "LayerId": "447968f1-58e2-4b6d-98f5-35c408e660c6"
                }
            ]
        },
        {
            "id": "c20060ab-bf15-4af4-86b6-65c253bf5ade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c75cef2e-63f8-4caf-9c08-5f347529b283",
            "compositeImage": {
                "id": "bda36b13-2d2a-4c8c-b23e-ea734e9af39e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c20060ab-bf15-4af4-86b6-65c253bf5ade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d113a66-8bb2-48b1-bfcf-cd611d125edf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c20060ab-bf15-4af4-86b6-65c253bf5ade",
                    "LayerId": "447968f1-58e2-4b6d-98f5-35c408e660c6"
                }
            ]
        },
        {
            "id": "f52f0189-6781-4995-a500-5849c2474234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c75cef2e-63f8-4caf-9c08-5f347529b283",
            "compositeImage": {
                "id": "f43f4703-4220-4df1-8787-287a2539d9f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f52f0189-6781-4995-a500-5849c2474234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8056acb-72e5-45ad-9a9d-e85f4274f40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f52f0189-6781-4995-a500-5849c2474234",
                    "LayerId": "447968f1-58e2-4b6d-98f5-35c408e660c6"
                }
            ]
        },
        {
            "id": "bdadb3d1-2a0d-4194-8feb-b162d24c21d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c75cef2e-63f8-4caf-9c08-5f347529b283",
            "compositeImage": {
                "id": "9082318b-2d53-4d54-b006-b2a1655bfe36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdadb3d1-2a0d-4194-8feb-b162d24c21d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce1a6465-fdb2-48bd-8a3f-00aa0c880eab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdadb3d1-2a0d-4194-8feb-b162d24c21d7",
                    "LayerId": "447968f1-58e2-4b6d-98f5-35c408e660c6"
                }
            ]
        },
        {
            "id": "59b14ffd-f2f3-457a-8790-0381eb282f1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c75cef2e-63f8-4caf-9c08-5f347529b283",
            "compositeImage": {
                "id": "eb373b7c-bda9-4dc5-a8a3-edbc38ad9a3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59b14ffd-f2f3-457a-8790-0381eb282f1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cd4fede-6ec7-4d52-9671-4f8e1d1b5283",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59b14ffd-f2f3-457a-8790-0381eb282f1e",
                    "LayerId": "447968f1-58e2-4b6d-98f5-35c408e660c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "447968f1-58e2-4b6d-98f5-35c408e660c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c75cef2e-63f8-4caf-9c08-5f347529b283",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 24
}