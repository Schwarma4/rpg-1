{
    "id": "03be83e6-953f-4a09-876b-6312c0410bea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_chamelen_stand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2749a4cd-66c2-4858-b420-cb50f7516817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03be83e6-953f-4a09-876b-6312c0410bea",
            "compositeImage": {
                "id": "2b0d2342-714c-4b81-ae0c-c4d627d0f80b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2749a4cd-66c2-4858-b420-cb50f7516817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b290c5e8-7c89-4baa-9609-55f85c2eed28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2749a4cd-66c2-4858-b420-cb50f7516817",
                    "LayerId": "adac4634-791d-47b2-85e4-7aaffc18a061"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "adac4634-791d-47b2-85e4-7aaffc18a061",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03be83e6-953f-4a09-876b-6312c0410bea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 23
}