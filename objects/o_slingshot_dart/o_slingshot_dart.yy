{
    "id": "f9d700fa-d555-4303-bbcf-44d02d9c30a4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_slingshot_dart",
    "eventList": [
        {
            "id": "0fe42ff6-a671-4b21-925d-88629f8e09c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f9d700fa-d555-4303-bbcf-44d02d9c30a4"
        },
        {
            "id": "ae5eac7c-dd25-40d0-b7f0-e73767b144d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f9d700fa-d555-4303-bbcf-44d02d9c30a4"
        },
        {
            "id": "d30aa545-a268-4ea2-87a5-07fb1ee8bbff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f9d700fa-d555-4303-bbcf-44d02d9c30a4"
        },
        {
            "id": "339686ec-7f18-4a37-80d4-46851cf7d257",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8b6eac83-532b-4339-b7ae-34b6c460fbbd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f9d700fa-d555-4303-bbcf-44d02d9c30a4"
        },
        {
            "id": "2794c09e-ff0c-474c-884a-ed921e68bef9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "65171c23-635c-480e-ab31-eaea4af5313f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f9d700fa-d555-4303-bbcf-44d02d9c30a4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8704a8a6-2273-4874-8986-2f1e41dc5331",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f8a70356-9f7d-4b34-88f4-feb3f4b44fd5",
    "visible": true
}