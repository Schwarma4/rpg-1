{
    "id": "dee2a4a0-06c4-46b2-a0c0-38c7db1bf366",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_sword_item",
    "eventList": [
        {
            "id": "07c4b0af-fc37-428b-a412-bbebe0890f00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dee2a4a0-06c4-46b2-a0c0-38c7db1bf366"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d43e2dfd-4c7c-4148-8835-6544f350ca47",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6a6ee44c-58fe-49eb-a11a-8d0073d99602",
    "visible": false
}