{
    "id": "0aca68cb-5bc8-466d-bcbb-c9245a962434",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dark_bush",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3891e8f1-57c0-45ad-a30e-6f9b55bdc460",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aca68cb-5bc8-466d-bcbb-c9245a962434",
            "compositeImage": {
                "id": "5097a04f-3985-412b-9d12-4d05bdbdd091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3891e8f1-57c0-45ad-a30e-6f9b55bdc460",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d5493d2-f93e-47a8-9757-3aad29877691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3891e8f1-57c0-45ad-a30e-6f9b55bdc460",
                    "LayerId": "015ea849-ea7e-4c49-932f-173675c789b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "015ea849-ea7e-4c49-932f-173675c789b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0aca68cb-5bc8-466d-bcbb-c9245a962434",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}