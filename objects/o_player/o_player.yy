{
    "id": "151ae2c0-769b-41f3-a731-ed08995252b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player",
    "eventList": [
        {
            "id": "340a03cc-1015-4f81-86e0-4798157a8b45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "151ae2c0-769b-41f3-a731-ed08995252b2"
        },
        {
            "id": "91e6bac4-1a24-4a7c-971b-b27eea356493",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "151ae2c0-769b-41f3-a731-ed08995252b2"
        },
        {
            "id": "3fce1e98-06f1-49a9-b2b7-9b7e831fae76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "151ae2c0-769b-41f3-a731-ed08995252b2"
        },
        {
            "id": "e4d1ecd5-5ccd-4f6b-bdc7-1d6d80075699",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 7,
            "m_owner": "151ae2c0-769b-41f3-a731-ed08995252b2"
        },
        {
            "id": "2cdda2b8-4b0a-4d6f-a33c-88d184e67ba4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "151ae2c0-769b-41f3-a731-ed08995252b2"
        },
        {
            "id": "0cf6decc-7846-4761-9d42-201b86b400ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "151ae2c0-769b-41f3-a731-ed08995252b2"
        },
        {
            "id": "b1ffac7d-8825-4c61-b991-6443561ca0b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "139322a5-2ec5-4c61-a3a9-78e4162ec82f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "151ae2c0-769b-41f3-a731-ed08995252b2"
        },
        {
            "id": "647d51b0-7ed5-4fbb-b3f0-731c15188a50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "151ae2c0-769b-41f3-a731-ed08995252b2"
        },
        {
            "id": "bc25ca36-7bf7-4a0b-ac6b-7863177fb1ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 9,
            "m_owner": "151ae2c0-769b-41f3-a731-ed08995252b2"
        },
        {
            "id": "b023ed96-fda3-4bf1-b6ee-ae035daa04e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "151ae2c0-769b-41f3-a731-ed08995252b2"
        },
        {
            "id": "1e8a571b-18b7-448d-99ff-f9f548ebacac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 7,
            "m_owner": "151ae2c0-769b-41f3-a731-ed08995252b2"
        },
        {
            "id": "a069ed48-7c0b-4db1-bdc7-dd5224541203",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e45b7d6c-9a43-4674-a7e3-af78639eee20",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "151ae2c0-769b-41f3-a731-ed08995252b2"
        }
    ],
    "maskSpriteId": "2246c880-016b-4abb-8540-4658b3157e57",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}