/// @arg player
var _player = argument0;
var _stamina = global.player_one_stamina;
var _max_stamina = global.player_one_max_stamina;
var _health = global.player_one_health;
var _max_health = global.player_one_max_health;
var _player_object = o_player_two;
var _item_0 = global.item[0];
var _item_1 = global.item[1];

if _player == 2 {
	_stamina = global.player_two_stamina;
	_max_stamina = global.player_two_max_stamina;
	_health = global.player_two_health;
	_max_health = global.player_two_max_health;
	_player_object = o_player_one;
	_item_0 = global.item[2];
	_item_1 = global.item[3];
}

image_speed = 0.6;
if !instance_exists(_player_object) {
	if _player = 1 {
		global.current_player = 2;	
	} else {
		global.current_player = 1;	
	}
	exit;
}

direction_ = point_direction(x, y, _player_object.x, _player_object.y);
set_sprite_facing();
get_direction_facing(direction_);	
set_movement(direction_, 0.5);
max_speed_ = 1.5;
acceleration_ = 0.5;
var px, py;
px = instance_nearest(x, y, _player_object).x;
py = instance_nearest(x, y, _player_object).y;
if point_distance(x, y, px, py) > 24 {	
	add_movement_maxspeed(direction_, acceleration_, max_speed_);
	move_movement_entity(true);
} else {
	apply_friction_to_movement_entity();
	image_speed = 0;
}
if instance_exists(o_enemy) {
	var ex, ey;
	ex = instance_nearest(x, y, o_enemy).x;
	ey = instance_nearest(x, y, o_enemy).y;
	if distance_to_object(o_enemy) < 18 && distance_to_object(o_enemy) >= 12 && instance_exists(o_bomb_item) && _health > 1 {
		if _stamina >= o_bomb_item.cost_ {
			if instance_exists(_item_0) && _item_0.action_ == player.bomb {
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else if instance_exists(_item_1) && _item_1.action_ == player.bomb {
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			}
		}
	} 
	if distance_to_object(o_enemy) < 14  && instance_exists(o_sword_item)  && _health > 1 {
		if _stamina >= o_sword_item.cost_ {
			if instance_exists(_item_0) && _item_0.action_ == player.sword {
				image_speed = 0.6;
				direction_ = point_direction(x, y, ex, ey);
				set_movement(direction_, 0.5);
				set_sprite_facing();
				get_direction_facing(direction_);	
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else if instance_exists(_item_1) && _item_1.action_ == player.sword {
				image_speed = 0.6;
				direction_ = point_direction(x, y, ex, ey);
				set_movement(direction_, 0.5);
				set_sprite_facing();
				get_direction_facing(direction_);	
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			}
		}
	}
	
		if instance_exists(o_heart_pickup) && distance_to_object(o_heart_pickup) < 32 && _health < 2 {			
		//var hx, hy;
		//hx = instance_nearest(x, y, o_heart_pickup).x;
		//hy = instance_nearest(x, y, o_heart_pickup).y;
		if _item_0.action_ == player.evade || _item_1.action_ == player.evade {
			if instance_exists(_item_0) && _item_0.action_ == player.evade {
				image_speed = 0.6;
				roll_direction_ = point_direction(x, y, o_heart_pickup.x, o_heart_pickup.y);
				var _x_speed = lengthdir_x(speed_, direction_);
				if _x_speed != 0 {
					image_xscale = sign(_x_speed);	
				}
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else if instance_exists(_item_1) && _item_1.action_ == player.evade {
				image_speed = 0.6;
				roll_direction_ = point_direction(x, y, o_heart_pickup.x, o_heart_pickup.y);
				var _x_speed = lengthdir_x(speed_, direction_);
				if _x_speed != 0 {
					image_xscale = sign(_x_speed);	
				}
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else {
				image_speed = 0.6;
				direction_ = point_direction(x, y, o_heart_pickup.x, o_heart_pickup.y);
				set_sprite_facing();
				get_direction_facing(direction_);	
				set_movement(direction_, 0.5);
				max_speed_ = 1.5;
				acceleration_ = 0.5;
				if point_distance(x, y, px, py) > 24 {	
					add_movement_maxspeed(direction_, acceleration_, max_speed_);
					move_movement_entity(true);
				} else {
					apply_friction_to_movement_entity();
				}
		}
	} else {
			image_speed = 0.6;
			direction_ = point_direction(x, y, o_heart_pickup.x, o_heart_pickup.y);
			set_sprite_facing();
			get_direction_facing(direction_);	
			set_movement(direction_, 0.5);
			max_speed_ = 1.5;
			acceleration_ = 0.5;
			add_movement_maxspeed(direction_, acceleration_, max_speed_);
			move_movement_entity(true);
		}
	}
	
	
	if distance_to_object(o_enemy) < 8 && instance_exists(o_ring_item) && _health <= 1 {
		if _stamina >= o_ring_item.cost_ {
			if instance_exists(_item_0) && _item_0.action_ == player.evade {
				roll_direction_ = point_direction(x, y, _player_object.x, _player_object.y);
				var _x_speed = lengthdir_x(speed_, direction_);
				if _x_speed != 0 {
					image_xscale = sign(_x_speed);	
				}
				if myTextbox_ = noone {
					myTextbox_ = instance_create_layer(x, y, "TEXT", o_transparent_textbox);	
					myTextbox_.text_ = myText_[0];
					myTextbox_.creator = self;
					myTextbox_.name_ = MyName_;
				} else {
					instance_destroy(myTextbox_);	
					myTextbox_ = noone;
				}
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else if instance_exists(_item_1) && _item_1.action_ == player.evade {
				roll_direction_ = point_direction(x, y, _player_object.x, _player_object.y);
				var _x_speed = lengthdir_x(speed_, direction_);
				if _x_speed != 0 {
					image_xscale = sign(_x_speed);	
				}
				if myTextbox_ = noone {
					myTextbox_ = instance_create_layer(x, y, "TEXT", o_transparent_textbox);	
					myTextbox_.text_ = myText_[0];
					myTextbox_.creator = self;
					myTextbox_.name_ = MyName_;
				} else {
					instance_destroy(myTextbox_);	
					myTextbox_ = noone;
				}
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			}
		}
	} 
} else {
	image_speed = 0.6;
	if instance_exists(o_heart_pickup) && distance_to_object(o_heart_pickup) < 32 && _health < 2 {			
		//var hx, hy;
		//hx = instance_nearest(x, y, o_heart_pickup).x;
		//hy = instance_nearest(x, y, o_heart_pickup).y;
		if _item_0.action_ == player.evade || _item_1.action_ == player.evade {
			if instance_exists(_item_0) && _item_0.action_ == player.evade {
				image_speed = 0.6;
				roll_direction_ = point_direction(x, y, o_heart_pickup.x, o_heart_pickup.y);
				var _x_speed = lengthdir_x(speed_, direction_);
				if _x_speed != 0 {
					image_xscale = sign(_x_speed);	
				}
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else if instance_exists(_item_1) && _item_1.action_ == player.evade {
				image_speed = 0.6;
				roll_direction_ = point_direction(x, y, o_heart_pickup.x, o_heart_pickup.y);
				var _x_speed = lengthdir_x(speed_, direction_);
				if _x_speed != 0 {
					image_xscale = sign(_x_speed);	
				}
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else {
				image_speed = 0.6;
				direction_ = point_direction(x, y, o_heart_pickup.x, o_heart_pickup.y);
				set_sprite_facing();
				get_direction_facing(direction_);	
				set_movement(direction_, 0.5);
				max_speed_ = 1.5;
				acceleration_ = 0.5;
				if point_distance(x, y, px, py) > 24 {	
					add_movement_maxspeed(direction_, acceleration_, max_speed_);
					move_movement_entity(true);
				} else {
					apply_friction_to_movement_entity();
				}
		}
	} else {
			image_speed = 0.6;
			direction_ = point_direction(x, y, o_heart_pickup.x, o_heart_pickup.y);
			set_sprite_facing();
			get_direction_facing(direction_);	
			set_movement(direction_, 0.5);
			max_speed_ = 1.5;
			acceleration_ = 0.5;
			add_movement_maxspeed(direction_, acceleration_, max_speed_);
			move_movement_entity(true);
		}
	} else {
		if instance_exists(o_ring_item) && distance_to_object(_player_object) > 48 {
			if instance_exists(_item_0) && _item_0.action_ == player.evade {
				image_speed = 0.6;
				roll_direction_ = point_direction(x, y, _player_object.x, _player_object.y);
				var _x_speed = lengthdir_x(speed_, direction_);
				if _x_speed != 0 {
					image_xscale = sign(_x_speed);	
				}
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else if instance_exists(_item_1) && _item_1.action_ == player.evade {
				image_speed = 0.6;
				roll_direction_ = point_direction(x, y, _player_object.x, _player_object.y);
				var _x_speed = lengthdir_x(speed_, direction_);
				if _x_speed != 0 {
					image_xscale = sign(_x_speed);	
				}
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else {
					
			image_speed = 0.6;
			direction_ = point_direction(x, y, _player_object.x, _player_object.y);
			set_sprite_facing();
			get_direction_facing(direction_);	
			set_movement(direction_, 0.5);
			max_speed_ = 1.5;
			acceleration_ = 0.5;
			if point_distance(x, y, px, py) > 24 {	
				add_movement_maxspeed(direction_, acceleration_, max_speed_);
				move_movement_entity(true);
			} else {
				apply_friction_to_movement_entity();
			}
		}
	} else {
			image_speed = 0.6;
			direction_ = point_direction(x, y, _player_object.x, _player_object.y);
			set_sprite_facing();
			get_direction_facing(direction_);	
			set_movement(direction_, 0.5);
			max_speed_ = 1.5;
			acceleration_ = 0.5;
			if point_distance(x, y, px, py) > 24 {	
				add_movement_maxspeed(direction_, acceleration_, max_speed_);
				move_movement_entity(true);
			} else {
				apply_friction_to_movement_entity();
			}
		}
	}
}

if _player == 2 {
	global.player_two_stamina = _stamina;
	global.player_two_health = _health;
} else {
	global.player_one_stamina = _stamina;
	global.player_one_health = _health;
}


