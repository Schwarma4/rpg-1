{
    "id": "619169bb-3e48-4b30-97ed-ea02a3c6796c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bush_effect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 6,
    "bbox_right": 57,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8264e195-0e9c-4288-b2d7-66d9234540db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "619169bb-3e48-4b30-97ed-ea02a3c6796c",
            "compositeImage": {
                "id": "74acaf12-03f4-4487-844b-9b09b7640b9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8264e195-0e9c-4288-b2d7-66d9234540db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d6df92d-99d2-45f9-805f-6393aeb8989e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8264e195-0e9c-4288-b2d7-66d9234540db",
                    "LayerId": "724ac94e-a913-4b40-ade9-bfa0943bafe2"
                }
            ]
        },
        {
            "id": "1f42e8bb-3186-4955-adba-985a595b6361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "619169bb-3e48-4b30-97ed-ea02a3c6796c",
            "compositeImage": {
                "id": "2bdd0020-ddaf-44ae-9e0e-fd57fce2be1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f42e8bb-3186-4955-adba-985a595b6361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e980a274-6ddc-4d11-89b0-1b0a547005db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f42e8bb-3186-4955-adba-985a595b6361",
                    "LayerId": "724ac94e-a913-4b40-ade9-bfa0943bafe2"
                }
            ]
        },
        {
            "id": "33ae540a-0bda-438b-a422-fe0efd078984",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "619169bb-3e48-4b30-97ed-ea02a3c6796c",
            "compositeImage": {
                "id": "622c93ee-1ce8-4fea-8056-c08492458e3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33ae540a-0bda-438b-a422-fe0efd078984",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5df5232-eaa6-4081-b8ad-f09d968be6c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33ae540a-0bda-438b-a422-fe0efd078984",
                    "LayerId": "724ac94e-a913-4b40-ade9-bfa0943bafe2"
                }
            ]
        },
        {
            "id": "8caac085-a069-497d-b73b-a69d8f5a6fb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "619169bb-3e48-4b30-97ed-ea02a3c6796c",
            "compositeImage": {
                "id": "98d53f45-8e45-4f3f-b330-852bcbe890f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8caac085-a069-497d-b73b-a69d8f5a6fb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8c275e6-2fa9-47c2-ab3a-fb7098b12302",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8caac085-a069-497d-b73b-a69d8f5a6fb6",
                    "LayerId": "724ac94e-a913-4b40-ade9-bfa0943bafe2"
                }
            ]
        },
        {
            "id": "986358d0-3574-403a-96db-2de891753d47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "619169bb-3e48-4b30-97ed-ea02a3c6796c",
            "compositeImage": {
                "id": "5f676831-17a6-434e-8c24-63f1a17dd1cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "986358d0-3574-403a-96db-2de891753d47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c0f212-7b1b-4437-873a-6706384706f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "986358d0-3574-403a-96db-2de891753d47",
                    "LayerId": "724ac94e-a913-4b40-ade9-bfa0943bafe2"
                }
            ]
        },
        {
            "id": "e66c1e77-d4f9-4c2e-a05e-e93e71ef09dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "619169bb-3e48-4b30-97ed-ea02a3c6796c",
            "compositeImage": {
                "id": "4cc75686-36c5-4aad-b16e-f78119d63ede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e66c1e77-d4f9-4c2e-a05e-e93e71ef09dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7666ede9-b010-4451-9afe-3dcf017fe5fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e66c1e77-d4f9-4c2e-a05e-e93e71ef09dc",
                    "LayerId": "724ac94e-a913-4b40-ade9-bfa0943bafe2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "724ac94e-a913-4b40-ade9-bfa0943bafe2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "619169bb-3e48-4b30-97ed-ea02a3c6796c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 24,
    "yorig": 24
}