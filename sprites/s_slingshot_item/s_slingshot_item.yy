{
    "id": "bab772ba-e2d1-4c5b-b745-47e0319fba93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_slingshot_item",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 4,
    "bbox_right": 26,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "daf5ba6f-52cd-49e3-8d16-395754707cd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bab772ba-e2d1-4c5b-b745-47e0319fba93",
            "compositeImage": {
                "id": "d0158890-2fae-4268-a8ef-1edc40cf212b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daf5ba6f-52cd-49e3-8d16-395754707cd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb542461-610b-4510-976d-44f2888e1ef3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daf5ba6f-52cd-49e3-8d16-395754707cd4",
                    "LayerId": "0a26157f-ef8b-4140-90ed-7305ea0323f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0a26157f-ef8b-4140-90ed-7305ea0323f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bab772ba-e2d1-4c5b-b745-47e0319fba93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}