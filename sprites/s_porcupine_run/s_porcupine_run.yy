{
    "id": "b83c4a65-7d78-44fc-860e-4b0aa5853775",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_porcupine_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 2,
    "bbox_right": 21,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9cd6b42e-6df9-43bb-8e5c-2a8d0da3b3d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b83c4a65-7d78-44fc-860e-4b0aa5853775",
            "compositeImage": {
                "id": "e54bd40c-aed5-4f37-83fa-a4e3add422e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd6b42e-6df9-43bb-8e5c-2a8d0da3b3d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c8bf457-f58e-4bb4-9b95-712334861f91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd6b42e-6df9-43bb-8e5c-2a8d0da3b3d7",
                    "LayerId": "7a6bf6d7-e048-4410-b4d8-f0ad19fa4173"
                }
            ]
        },
        {
            "id": "81847b00-5174-40b0-82e7-3fe469b43f05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b83c4a65-7d78-44fc-860e-4b0aa5853775",
            "compositeImage": {
                "id": "27016639-76e7-4ba5-9c32-892b691f66ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81847b00-5174-40b0-82e7-3fe469b43f05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25f37f3d-2a79-4f5d-80a0-8a5b60773e49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81847b00-5174-40b0-82e7-3fe469b43f05",
                    "LayerId": "7a6bf6d7-e048-4410-b4d8-f0ad19fa4173"
                }
            ]
        },
        {
            "id": "18cd2b33-0897-45cb-9881-7f96494e16cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b83c4a65-7d78-44fc-860e-4b0aa5853775",
            "compositeImage": {
                "id": "0c5f9024-9c09-4c01-88cb-81d1524b21c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18cd2b33-0897-45cb-9881-7f96494e16cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61e8d595-6ab1-4226-8ac1-71ba190b2464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18cd2b33-0897-45cb-9881-7f96494e16cc",
                    "LayerId": "7a6bf6d7-e048-4410-b4d8-f0ad19fa4173"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "7a6bf6d7-e048-4410-b4d8-f0ad19fa4173",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b83c4a65-7d78-44fc-860e-4b0aa5853775",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 19
}