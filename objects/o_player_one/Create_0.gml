event_inherited();
myTextbox_ = noone;
MyName_ = "Player 1";
myText_[0] = "Hey there"


//Sprite move lookup table
sprite_[player.move , dir.right] = s_player_WORM_run_right1;
sprite_[player.move , dir.left] = s_player_WORM_run_right1;
sprite_[player.move , dir.up] = s_player_WORM_run_up1;
sprite_[player.move , dir.down] = s_player_WORM_run_up1;

//Sprite attack lookup table
sprite_[player.sword , dir.right] = s_player_attack_right;
sprite_[player.sword , dir.left] = s_player_attack_right;
sprite_[player.sword , dir.up] = s_player_attack_up;
sprite_[player.sword , dir.down] = s_player_attack_down;

//Sprite evade lookup table
sprite_[player.evade , dir.right] = s_player_roll_right;
sprite_[player.evade , dir.left] = s_player_roll_right;
sprite_[player.evade , dir.up] = s_player_roll_up;
sprite_[player.evade , dir.down] = s_player_roll_down;

//Sprite hit lookup table
sprite_[player.hit , dir.right] = s_player_WORM_run_right1;
sprite_[player.hit , dir.left] = s_player_WORM_run_right1;
sprite_[player.hit , dir.up] = s_player_WORM_run_up1;
sprite_[player.hit , dir.down] = s_player_WORM_run_up1;


//Sprite bomb lookup table
sprite_[player.bomb , dir.right] = s_player_WORM_run_right1;
sprite_[player.bomb , dir.left] = s_player_WORM_run_right1;
sprite_[player.bomb , dir.up] = s_player_WORM_run_up1;
sprite_[player.bomb , dir.down] = s_player_WORM_run_up1;
//s_player_one_pause.sprite_index = s_worm_pause;