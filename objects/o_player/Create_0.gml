initialize_movement_entity(.5, 0.1, o_solid);
initialize_hurtbox_entity();
image_speed = 0;
acceleration_ = .5;
max_speed_ = 1.5;
roll_speed_ = 2;
direction_facing_ = dir.right;
roll_direction_ = 0;
speed_ = 0.75;

enum player {
	move,
	sword,
	evade,
	bomb,
	ranged,
	found_item,
	hit,
	melee
}

enum follower {
	passive,
	aggressive,
	pacifist,
	defend_wait,
}

enum dir {
	right,
	up,
	left,
	down
}


state_ = player.move;
starting_state_ = player.move;
follower_state_ = follower.passive;

//Sprite move lookup table
sprite_[player.move , dir.right] = s_player_run_right;
sprite_[player.move , dir.left] = s_player_run_right;
sprite_[player.move , dir.up] = s_player_run_up;
sprite_[player.move , dir.down] = s_player_run_down;

//Sprite attack lookup table
sprite_[player.sword , dir.right] = s_player_attack_right;
sprite_[player.sword , dir.left] = s_player_attack_right;
sprite_[player.sword , dir.up] = s_player_attack_up;
sprite_[player.sword , dir.down] = s_player_attack_down;

//Sprite evade lookup table
sprite_[player.evade , dir.right] = s_player_roll_right;
sprite_[player.evade , dir.left] = s_player_roll_right;
sprite_[player.evade , dir.up] = s_player_roll_up;
sprite_[player.evade , dir.down] = s_player_roll_down;

//Sprite hit lookup table
sprite_[player.hit , dir.right] = s_player_run_right;
sprite_[player.hit , dir.left] = s_player_run_right;
sprite_[player.hit , dir.up] = s_player_run_up;
sprite_[player.hit , dir.down] = s_player_run_down;


//Sprite bomb lookup table
sprite_[player.bomb , dir.right] = s_player_run_right;
sprite_[player.bomb , dir.left] = s_player_run_right;
sprite_[player.bomb , dir.up] = s_player_run_up;
sprite_[player.bomb , dir.down] = s_player_run_down;


//Sprite bomb lookup table
sprite_[player.ranged , dir.right] = s_player_run_right;
sprite_[player.ranged , dir.left] = s_player_run_right;
sprite_[player.ranged , dir.up] = s_player_run_up;
sprite_[player.ranged , dir.down] = s_player_run_down;

//Sprite attack lookup table
sprite_[player.melee , dir.right] = s_player_attack_right;
sprite_[player.melee , dir.left] = s_player_attack_right;
sprite_[player.melee , dir.up] = s_player_attack_up;
sprite_[player.melee , dir.down] = s_player_attack_down;