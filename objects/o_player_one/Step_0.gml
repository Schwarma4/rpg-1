event_inherited();

if myTextbox_ != noone {
	myTextbox_.text_ = myText_;
}

if global.player_one_health <= 0 and !invincible_ {
	instance_destroy();	
	instance_destroy(myTextbox_);
	if instance_exists(o_player_two){
		global.current_player = 2;
		o_camera.target_ = o_player_two;
	} else {
		game_restart();
	}
}