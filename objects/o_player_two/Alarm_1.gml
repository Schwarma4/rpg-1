/// @description STAMINA ALARM RECHARGE
global.player_two_stamina = min(global.player_two_stamina + 1, global.player_two_max_stamina);
if global.player_two_stamina < global.player_two_max_stamina {
	alarm[1] = global.one_second;
}