{
    "id": "28b924e3-4ce9-480b-ba17-fff93a426ce8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_fly_attack_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 6,
    "bbox_right": 41,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0138e7d2-eaf5-4a09-a976-04277dff3ce9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b924e3-4ce9-480b-ba17-fff93a426ce8",
            "compositeImage": {
                "id": "cd9c78a7-ac7b-403c-9e28-166866687a02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0138e7d2-eaf5-4a09-a976-04277dff3ce9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c3ace4e-d79c-4380-8581-8adfef581cf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0138e7d2-eaf5-4a09-a976-04277dff3ce9",
                    "LayerId": "596159b6-d014-45ae-a3b4-3f2d090d338b"
                },
                {
                    "id": "bfc6d82e-9723-401d-bff0-66132cd87fb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0138e7d2-eaf5-4a09-a976-04277dff3ce9",
                    "LayerId": "d9839136-a7e4-4b91-a943-38357e9ec63e"
                }
            ]
        },
        {
            "id": "bbe8434f-135d-45ca-910b-4a54dbc38d0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b924e3-4ce9-480b-ba17-fff93a426ce8",
            "compositeImage": {
                "id": "a114521d-e02a-4308-9873-ecbbb5160085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbe8434f-135d-45ca-910b-4a54dbc38d0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "230b5cec-23fa-4925-8b4e-a4e874de7fb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbe8434f-135d-45ca-910b-4a54dbc38d0a",
                    "LayerId": "596159b6-d014-45ae-a3b4-3f2d090d338b"
                },
                {
                    "id": "9a47542e-b87f-4731-b3d5-370dbb42247c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbe8434f-135d-45ca-910b-4a54dbc38d0a",
                    "LayerId": "d9839136-a7e4-4b91-a943-38357e9ec63e"
                }
            ]
        },
        {
            "id": "c3e58475-a826-4fce-b2c4-5e70f90a9fe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b924e3-4ce9-480b-ba17-fff93a426ce8",
            "compositeImage": {
                "id": "ba666a0f-97b1-4a25-bca5-614172de368c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3e58475-a826-4fce-b2c4-5e70f90a9fe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc2486cb-fdf5-444d-a8fc-f017b2104a17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3e58475-a826-4fce-b2c4-5e70f90a9fe0",
                    "LayerId": "596159b6-d014-45ae-a3b4-3f2d090d338b"
                },
                {
                    "id": "757104f4-ecee-46b4-b6cc-958c77a7f695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3e58475-a826-4fce-b2c4-5e70f90a9fe0",
                    "LayerId": "d9839136-a7e4-4b91-a943-38357e9ec63e"
                }
            ]
        },
        {
            "id": "307663a2-3a9d-459e-b7fa-5b5d14929c2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b924e3-4ce9-480b-ba17-fff93a426ce8",
            "compositeImage": {
                "id": "e500874e-24b3-479e-8a11-4735a8399df2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "307663a2-3a9d-459e-b7fa-5b5d14929c2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5741bc5-ed76-49fd-8cca-a4dd5f5be419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "307663a2-3a9d-459e-b7fa-5b5d14929c2a",
                    "LayerId": "596159b6-d014-45ae-a3b4-3f2d090d338b"
                },
                {
                    "id": "1ba58151-012b-4cc6-a17f-2adc483c68d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "307663a2-3a9d-459e-b7fa-5b5d14929c2a",
                    "LayerId": "d9839136-a7e4-4b91-a943-38357e9ec63e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "596159b6-d014-45ae-a3b4-3f2d090d338b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28b924e3-4ce9-480b-ba17-fff93a426ce8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d9839136-a7e4-4b91-a943-38357e9ec63e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28b924e3-4ce9-480b-ba17-fff93a426ce8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 23,
    "yorig": 23
}