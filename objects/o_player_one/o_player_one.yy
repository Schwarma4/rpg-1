{
    "id": "405ac906-b8ad-4e6f-82dc-f64f941e6f84",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player_one",
    "eventList": [
        {
            "id": "4e5c5b92-a63a-46b8-8d6d-68e56fb91460",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "405ac906-b8ad-4e6f-82dc-f64f941e6f84"
        },
        {
            "id": "ce02afed-4e04-4e91-82ca-8f0f99f6efc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "405ac906-b8ad-4e6f-82dc-f64f941e6f84"
        },
        {
            "id": "5f410874-b884-4772-8542-1554e843aefe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "405ac906-b8ad-4e6f-82dc-f64f941e6f84"
        },
        {
            "id": "fcf0ba5e-db77-493a-a84e-797f7b6d740a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8704a8a6-2273-4874-8986-2f1e41dc5331",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "405ac906-b8ad-4e6f-82dc-f64f941e6f84"
        },
        {
            "id": "cde19d87-2fdd-4b45-a6bf-ad6e01b4b109",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "405ac906-b8ad-4e6f-82dc-f64f941e6f84"
        },
        {
            "id": "307f3524-3d4e-4010-9cc2-baf0249661b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "405ac906-b8ad-4e6f-82dc-f64f941e6f84"
        }
    ],
    "maskSpriteId": "25f05a28-7c02-4a04-8c34-a514048ad117",
    "overriddenProperties": null,
    "parentObjectId": "151ae2c0-769b-41f3-a731-ed08995252b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2246c880-016b-4abb-8540-4658b3157e57",
    "visible": true
}