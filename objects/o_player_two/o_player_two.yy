{
    "id": "567b4618-acdc-4dba-a473-cc37ca31a43a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player_two",
    "eventList": [
        {
            "id": "d02b229d-204e-4870-b925-558f3221d2df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "567b4618-acdc-4dba-a473-cc37ca31a43a"
        },
        {
            "id": "d5e5037a-3f58-4641-b9c8-f0fa966ddb18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "567b4618-acdc-4dba-a473-cc37ca31a43a"
        },
        {
            "id": "dc9ab7da-ee3c-479e-9d24-fd7200af29c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "567b4618-acdc-4dba-a473-cc37ca31a43a"
        },
        {
            "id": "63d10d26-d00e-413f-95d2-7dbd0ec6d07e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8704a8a6-2273-4874-8986-2f1e41dc5331",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "567b4618-acdc-4dba-a473-cc37ca31a43a"
        },
        {
            "id": "e797b0c1-b435-4c68-9fce-00dd9fe65a6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "567b4618-acdc-4dba-a473-cc37ca31a43a"
        },
        {
            "id": "95a88ce5-765c-4083-994d-ba3438edc93a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "567b4618-acdc-4dba-a473-cc37ca31a43a"
        }
    ],
    "maskSpriteId": "25f05a28-7c02-4a04-8c34-a514048ad117",
    "overriddenProperties": null,
    "parentObjectId": "151ae2c0-769b-41f3-a731-ed08995252b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a9a08916-bf1c-46bb-8e78-04e0b66f47e0",
    "visible": true
}