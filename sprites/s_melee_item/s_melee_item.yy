{
    "id": "abb91329-12c5-46b2-bf70-4cbee6ce0940",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_melee_item",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d05229be-11fe-4ee5-9fec-d6c76e3f1932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abb91329-12c5-46b2-bf70-4cbee6ce0940",
            "compositeImage": {
                "id": "1a37a48f-c224-4b75-a050-53b55a9b3bbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d05229be-11fe-4ee5-9fec-d6c76e3f1932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "181dc220-5f81-4131-98a4-add50cbcb2bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d05229be-11fe-4ee5-9fec-d6c76e3f1932",
                    "LayerId": "e7e8013b-51e8-4015-b5e7-a2e215004d53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e7e8013b-51e8-4015-b5e7-a2e215004d53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abb91329-12c5-46b2-bf70-4cbee6ce0940",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}