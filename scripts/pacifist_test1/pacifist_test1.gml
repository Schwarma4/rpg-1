/// @arg player
var _player = argument0;
var _stamina = global.player_one_stamina;
var _max_stamina = global.player_one_max_stamina;
var _health = global.player_one_health;
var _max_health = global.player_one_max_health;
var _player_object = o_player_two;
var _item_0 = global.item[0];
var _item_1 = global.item[1];

if _player == 2 {
	_stamina = global.player_two_stamina;
	_max_stamina = global.player_two_max_stamina;
	_health = global.player_two_health;
	_max_health = global.player_two_max_health;
	_player_object = o_player_one;
	_item_0 = global.item[2];
	_item_1 = global.item[3];
}

//run towards main player
image_speed = 0.6;
direction_ = point_direction(x, y, _player_object.x, _player_object.y);	
roll_direction_ = point_direction(x, y, _player_object.x, _player_object.y);
set_movement(direction_, max_speed_);
set_sprite_facing();
get_direction_facing(direction_);	
max_speed_ = 1.5;
acceleration_ = 0.5;
var px, py;
px = instance_nearest(x, y, _player_object).x;
py = instance_nearest(x, y, _player_object).y;
if point_distance(x, y, px, py) > 24 {	
	add_movement_maxspeed(direction_, acceleration_, max_speed_);
	move_movement_entity(true);
} else {
	apply_friction_to_movement_entity();
	image_speed = 0;
}

//if main character is more than 42 pixels away and follower has evade equipped, roll towards main character
if point_distance(x, y, _player_object.x, _player_object.y) > 42 && ((instance_exists(_item_0) && _item_0.action_ == player.evade && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.evade && _item_1.cost_ <= _stamina)) && point_distance(x + lengthdir_x(roll_speed_,roll_direction_), y + lengthdir_y(roll_speed_,roll_direction_), o_enemy.x, o_enemy.y) > 64 {
	//set_movement(roll_direction_, 0.5);
	set_sprite_facing();
	get_direction_facing(roll_direction_);
	if _item_0.action_ == player.evade {
		state_ = _item_0.action_;
		_stamina -= _item_0.cost_;
		_stamina = max(0, _stamina);		
		alarm[1] = global.one_second;
	} else if _item_1.action_ == player.evade {
		state_ = _item_1.action_;
		_stamina -= _item_1.cost_;
		_stamina = max(0, _stamina);		
		alarm[1] = global.one_second;
	}
}


if _player == 2 {
	global.player_two_stamina = _stamina;
	global.player_two_health = _health;
} else {
	global.player_one_stamina = _stamina;
	global.player_one_health = _health;
}