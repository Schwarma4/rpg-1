{
    "id": "b5624a76-2002-4b6e-adb2-3a9f8f59b90f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_game",
    "eventList": [
        {
            "id": "f4326854-0fe4-4d54-a729-644281dbbbf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b5624a76-2002-4b6e-adb2-3a9f8f59b90f"
        },
        {
            "id": "8df9f058-7e4b-47e5-85fb-5671950c684a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "b5624a76-2002-4b6e-adb2-3a9f8f59b90f"
        },
        {
            "id": "e2d7f6c5-ab53-4238-8d1a-cc1b5e77fdc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "b5624a76-2002-4b6e-adb2-3a9f8f59b90f"
        },
        {
            "id": "4a16e79e-b422-41d6-86e0-04f2d0d08cd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b5624a76-2002-4b6e-adb2-3a9f8f59b90f"
        },
        {
            "id": "1cd3f0a2-3089-4055-853a-8534572763fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b5624a76-2002-4b6e-adb2-3a9f8f59b90f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}