if room == r_title {
	exit;	
}

if global.current_player = 1 {
	var _gui_width = display_get_gui_width();
	var _gui_height = display_get_gui_height();

	if sprite_exists(paused_sprite_) {
		draw_sprite_ext(paused_sprite_, 0, 0, 0, paused_sprite_scale_, paused_sprite_scale_, 0, c_white, 1);	
		draw_set_alpha(.6);
		draw_rectangle_color(0, 0, _gui_width, _gui_height, c_black, c_black, c_black, c_black, c_black);
		draw_set_alpha(1);
		for (var _i=0; _i<global.player_two_max_health; _i++) {
			var _filled = _i < global.player_two_health;
			draw_sprite(s_two_heart_ui, _filled, 300 - 15 * _i, _gui_height - 175);
		}
		for (var _i=0; _i<global.player_two_max_stamina; _i++) {
			var _filled = _i < global.player_two_stamina;
			draw_sprite(s_two_stamina_ui, _filled, 300 - 20 * _i, _gui_height - 160);
		}
		if instance_exists(o_player_two) {
			switch (o_player_two.follower_state_) {
				case 0:
					draw_text(230, _gui_height - 145, "PASSIVE");
					break;
				case 1:
					draw_text(230, _gui_height - 145, "AGGRESSIVE");
					break;
				case 2:
					draw_text(230, _gui_height - 145, "PACIFIST");
					break;
				case 3:
					draw_text(230, _gui_height - 145, "DEFEND/WAIT");
					break;
			}
		} else {
				draw_text(230, _gui_height - 145, "DECEASED :(");
		}
	}
	
	
	var _hud_right_edge = max(3 + global.player_one_max_health * 15, 2 + global.player_one_max_stamina * 35);
	draw_sprite_ext(s_hud, 0, 0, _gui_height, _hud_right_edge, 1, 0, c_white, 1);
	draw_sprite(s_hud_edge, 0, _hud_right_edge, _gui_height);

	for (var _i=0; _i<global.player_one_max_health; _i++) {
		var _filled = _i < global.player_one_health;
		draw_sprite(s_heart_ui, _filled, 4 + 15 * _i, _gui_height - 33);
	}
	for (var _i=0; _i<global.player_one_max_stamina; _i++) {
		var _filled = _i < global.player_one_stamina;
		draw_sprite(s_stamina_ui, _filled, 4 + 17 * _i, _gui_height - 20);
	}

	var _gem_string = string(global.player_gems);
	var _text_width = string_width(_gem_string);
	var _x = _gui_width - _text_width + 4;
	var _y = _gui_height - 16 + 4;
	draw_sprite(s_gem, 0, _x - 16, _y + 7);
	draw_text(_x - 8, _y -1, _gem_string);

	inventory_draw(4, 36, 1);
} else {
	var _gui_width = display_get_gui_width();
	var _gui_height = display_get_gui_height();

	if sprite_exists(paused_sprite_) {
		draw_sprite_ext(paused_sprite_, 0, 0, 0, paused_sprite_scale_, paused_sprite_scale_, 0, c_white, 1);	
		draw_set_alpha(.6);
		draw_rectangle_color(0, 0, _gui_width, _gui_height, c_black, c_black, c_black, c_black, c_black);
		draw_set_alpha(1);
		for (var _i=0; _i<global.player_one_max_health; _i++) {
			var _filled = _i < global.player_one_health;
			draw_sprite(s_two_heart_ui, _filled, 300 - 15 * _i, _gui_height - 175);
		}
		for (var _i=0; _i<global.player_one_max_stamina; _i++) {
			var _filled = _i < global.player_one_stamina;
			draw_sprite(s_two_stamina_ui, _filled, 300 - 20 * _i, _gui_height - 160);
		}
		
	if instance_exists(o_player_one) {
		switch (o_player_one.follower_state_) {
			case 0:
				draw_text(230, _gui_height - 145, "PASSIVE");
				break;
			case 1:
				draw_text(230, _gui_height - 145, "AGGRESSIVE");
				break;
			case 2:
				draw_text(230, _gui_height - 145, "PACIFIST");
				break;
			case 3:
				draw_text(230, _gui_height - 145, "DEFEND/WAIT");
				break;
			}
		} else {
				draw_text(230, _gui_height - 145, "DECEASED :(");
		}
	}
	
	var _hud_right_edge = max(3 + global.player_two_max_health * 15, 2 + global.player_two_max_stamina * 35);
	draw_sprite_ext(s_hud, 0, 0, _gui_height, _hud_right_edge, 1, 0, c_white, 1);
	draw_sprite(s_hud_edge, 0, _hud_right_edge, _gui_height);

	for (var _i=0; _i<global.player_two_max_health; _i++) {
		var _filled = _i < global.player_two_health;
		draw_sprite(s_heart_ui, _filled, 4 + 15 * _i, _gui_height - 33);
	}
	for (var _i=0; _i<global.player_two_max_stamina; _i++) {
		var _filled = _i < global.player_two_stamina;
		draw_sprite(s_stamina_ui, _filled, 4 + 17 * _i, _gui_height - 20);
	}

	var _gem_string = string(global.player_gems);
	var _text_width = string_width(_gem_string);
	var _x = _gui_width - _text_width + 4;
	var _y = _gui_height - 16 + 4;
	draw_sprite(s_gem, 0, _x - 16, _y + 7);
	draw_text(_x - 8, _y -1, _gem_string);

	inventory_draw(4, 36, 2);
}