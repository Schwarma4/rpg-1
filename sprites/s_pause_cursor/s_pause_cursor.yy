{
    "id": "aa1f81b3-26d3-4f2d-8951-39bc5337bcbe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pause_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbe8084b-1f62-40d4-87a7-0416bca5fc3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa1f81b3-26d3-4f2d-8951-39bc5337bcbe",
            "compositeImage": {
                "id": "0da1ec41-c0b0-40dc-b1e0-5ef3e78c4009",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbe8084b-1f62-40d4-87a7-0416bca5fc3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f67656e-a7fa-4b5e-ae9a-1b39852541f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbe8084b-1f62-40d4-87a7-0416bca5fc3c",
                    "LayerId": "ad956493-469b-4e72-9614-68f98d549a5d"
                }
            ]
        },
        {
            "id": "5f87fa65-7ee3-49b7-b956-d0e41447dc99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa1f81b3-26d3-4f2d-8951-39bc5337bcbe",
            "compositeImage": {
                "id": "ed1857f1-32e4-47ba-b600-5276e793737a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f87fa65-7ee3-49b7-b956-d0e41447dc99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c717e6e-ded2-4e4d-a1ae-bbf539ff374a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f87fa65-7ee3-49b7-b956-d0e41447dc99",
                    "LayerId": "ad956493-469b-4e72-9614-68f98d549a5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ad956493-469b-4e72-9614-68f98d549a5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa1f81b3-26d3-4f2d-8951-39bc5337bcbe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}