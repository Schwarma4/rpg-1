{
    "id": "31e5165e-95c3-4d7f-846d-621d41584832",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_projectile",
    "eventList": [
        {
            "id": "3e5fe3a6-0086-485a-8c36-35e8c6b91f17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "31e5165e-95c3-4d7f-846d-621d41584832"
        },
        {
            "id": "592a4e16-bda9-41bd-8dc8-45c9a0bd5de1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "31e5165e-95c3-4d7f-846d-621d41584832"
        },
        {
            "id": "ff115a8f-e05c-433c-9056-31e112cdd558",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "31e5165e-95c3-4d7f-846d-621d41584832"
        },
        {
            "id": "60d6e96d-af55-440e-8ef5-d1119633b0de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8b6eac83-532b-4339-b7ae-34b6c460fbbd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "31e5165e-95c3-4d7f-846d-621d41584832"
        },
        {
            "id": "a5dda260-f1b2-47a2-918c-feb60f29a9ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "65171c23-635c-480e-ab31-eaea4af5313f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "31e5165e-95c3-4d7f-846d-621d41584832"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}