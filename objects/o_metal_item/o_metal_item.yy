{
    "id": "f7d9fdf1-a968-4d83-b849-81d101f3ccff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_metal_item",
    "eventList": [
        {
            "id": "1be37762-155e-4c8a-b6b0-c01152998181",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f7d9fdf1-a968-4d83-b849-81d101f3ccff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d43e2dfd-4c7c-4148-8835-6544f350ca47",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0f5cb83b-4a3d-4bd8-b2d3-1cd95e13b085",
    "visible": false
}