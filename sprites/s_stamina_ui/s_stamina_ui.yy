{
    "id": "7487e9b0-1d10-416e-8b40-251c4757c98b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_stamina_ui",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75f81f3e-edc3-4e6d-bb3d-4c4041fe7f74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7487e9b0-1d10-416e-8b40-251c4757c98b",
            "compositeImage": {
                "id": "2a7a88a1-4c54-4a12-aab0-da11bab14012",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75f81f3e-edc3-4e6d-bb3d-4c4041fe7f74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1426d00-c9b8-4558-ad89-1061ff5cf21f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75f81f3e-edc3-4e6d-bb3d-4c4041fe7f74",
                    "LayerId": "4f3c5646-bd5d-45cb-8b03-adcf16796037"
                }
            ]
        },
        {
            "id": "01546be3-5f68-4fde-b38a-84b926ad6e1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7487e9b0-1d10-416e-8b40-251c4757c98b",
            "compositeImage": {
                "id": "30ec2bb7-9f32-44c9-9696-7a68c29292cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01546be3-5f68-4fde-b38a-84b926ad6e1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9102690-c651-4e0b-a0a8-cee1b277c203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01546be3-5f68-4fde-b38a-84b926ad6e1a",
                    "LayerId": "4f3c5646-bd5d-45cb-8b03-adcf16796037"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4f3c5646-bd5d-45cb-8b03-adcf16796037",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7487e9b0-1d10-416e-8b40-251c4757c98b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}