{
    "id": "884072d3-90cc-4aa7-898c-97f02f3af275",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_explosion_effect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 1,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a47922e-8a55-42ad-a686-9fd0b71965d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "884072d3-90cc-4aa7-898c-97f02f3af275",
            "compositeImage": {
                "id": "75453584-9960-432a-bc4a-6760ccd97e1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a47922e-8a55-42ad-a686-9fd0b71965d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d140cbc-c6ec-4495-b1b3-5aaa09577816",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a47922e-8a55-42ad-a686-9fd0b71965d8",
                    "LayerId": "6afb6e6e-2c9c-4f63-abdd-067b075d5ca3"
                }
            ]
        },
        {
            "id": "176ab3f6-ada8-4c87-9e39-f0b3acc1eaab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "884072d3-90cc-4aa7-898c-97f02f3af275",
            "compositeImage": {
                "id": "0a0e73b1-daee-47ed-bfd7-28ed8ef733bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "176ab3f6-ada8-4c87-9e39-f0b3acc1eaab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "094f9b20-9dbf-4914-a4c2-0df5d1816f5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "176ab3f6-ada8-4c87-9e39-f0b3acc1eaab",
                    "LayerId": "6afb6e6e-2c9c-4f63-abdd-067b075d5ca3"
                }
            ]
        },
        {
            "id": "50f09d00-a656-48ac-9479-f627260b9db6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "884072d3-90cc-4aa7-898c-97f02f3af275",
            "compositeImage": {
                "id": "0ed42526-0d20-4883-99db-991c857da812",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50f09d00-a656-48ac-9479-f627260b9db6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "051d9032-61cb-4096-800e-905906e48b25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50f09d00-a656-48ac-9479-f627260b9db6",
                    "LayerId": "6afb6e6e-2c9c-4f63-abdd-067b075d5ca3"
                }
            ]
        },
        {
            "id": "98048a0c-a10d-4064-9488-370f7719d11e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "884072d3-90cc-4aa7-898c-97f02f3af275",
            "compositeImage": {
                "id": "7bfde756-11dd-46f7-95db-33c088c88fdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98048a0c-a10d-4064-9488-370f7719d11e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d9a0e3a-d6ca-4f12-a9e2-60855d936c4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98048a0c-a10d-4064-9488-370f7719d11e",
                    "LayerId": "6afb6e6e-2c9c-4f63-abdd-067b075d5ca3"
                }
            ]
        },
        {
            "id": "bd578c1e-83d9-4778-9358-35a021c77954",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "884072d3-90cc-4aa7-898c-97f02f3af275",
            "compositeImage": {
                "id": "c67699eb-54f8-4a22-98d9-bea62a4eecfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd578c1e-83d9-4778-9358-35a021c77954",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7483810d-5f38-4866-9449-d0cceb586688",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd578c1e-83d9-4778-9358-35a021c77954",
                    "LayerId": "6afb6e6e-2c9c-4f63-abdd-067b075d5ca3"
                }
            ]
        },
        {
            "id": "25c467e8-3808-45bf-82d9-28374655a423",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "884072d3-90cc-4aa7-898c-97f02f3af275",
            "compositeImage": {
                "id": "d2725249-20ba-41e9-94fd-fa4f6b8ea451",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25c467e8-3808-45bf-82d9-28374655a423",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a22815b1-f23f-4ff0-a8d8-f05f69081686",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25c467e8-3808-45bf-82d9-28374655a423",
                    "LayerId": "6afb6e6e-2c9c-4f63-abdd-067b075d5ca3"
                }
            ]
        },
        {
            "id": "fd3c8b85-9c35-408d-b8bd-b7f54a52b37e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "884072d3-90cc-4aa7-898c-97f02f3af275",
            "compositeImage": {
                "id": "a4633cb0-10e7-4124-9185-963018a967e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd3c8b85-9c35-408d-b8bd-b7f54a52b37e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ce9ae2a-6019-4767-aa93-c022f81f011c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd3c8b85-9c35-408d-b8bd-b7f54a52b37e",
                    "LayerId": "6afb6e6e-2c9c-4f63-abdd-067b075d5ca3"
                }
            ]
        },
        {
            "id": "9cbb0a19-0837-476f-9b1a-37fb0b550a19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "884072d3-90cc-4aa7-898c-97f02f3af275",
            "compositeImage": {
                "id": "e9d08b2a-aca9-4abe-bc41-205423a2afd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cbb0a19-0837-476f-9b1a-37fb0b550a19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3122c02d-5b86-48b3-9bb9-686487cd6be3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cbb0a19-0837-476f-9b1a-37fb0b550a19",
                    "LayerId": "6afb6e6e-2c9c-4f63-abdd-067b075d5ca3"
                }
            ]
        },
        {
            "id": "c6f9b7a9-a359-419d-b002-083f429eaad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "884072d3-90cc-4aa7-898c-97f02f3af275",
            "compositeImage": {
                "id": "2e864b3e-54cd-42e7-9682-18cb96707b3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6f9b7a9-a359-419d-b002-083f429eaad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbed2f34-ce31-45be-9375-c5fd38c3b143",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6f9b7a9-a359-419d-b002-083f429eaad8",
                    "LayerId": "6afb6e6e-2c9c-4f63-abdd-067b075d5ca3"
                }
            ]
        },
        {
            "id": "dfc879cf-c6ff-407c-8a37-4abd4d309362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "884072d3-90cc-4aa7-898c-97f02f3af275",
            "compositeImage": {
                "id": "770c0e8c-15b9-4e1c-93b4-59d8a8406b24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfc879cf-c6ff-407c-8a37-4abd4d309362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "710d1454-848a-423c-8464-fcbcdb09f1a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfc879cf-c6ff-407c-8a37-4abd4d309362",
                    "LayerId": "6afb6e6e-2c9c-4f63-abdd-067b075d5ca3"
                }
            ]
        },
        {
            "id": "3ea7ecdf-296a-4fee-acf7-db26d544d72e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "884072d3-90cc-4aa7-898c-97f02f3af275",
            "compositeImage": {
                "id": "1db8eee8-ceb1-48e9-a264-caffd6999f1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ea7ecdf-296a-4fee-acf7-db26d544d72e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42450d44-7807-4224-aed0-e5e9c2d47955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ea7ecdf-296a-4fee-acf7-db26d544d72e",
                    "LayerId": "6afb6e6e-2c9c-4f63-abdd-067b075d5ca3"
                }
            ]
        },
        {
            "id": "41c7f006-7520-41c8-812b-7da349601d74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "884072d3-90cc-4aa7-898c-97f02f3af275",
            "compositeImage": {
                "id": "23b22e50-b9be-4ac8-96ed-bdc9a6648cbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41c7f006-7520-41c8-812b-7da349601d74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec2dd7f0-0d1d-46cd-87ed-8495f637cbfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41c7f006-7520-41c8-812b-7da349601d74",
                    "LayerId": "6afb6e6e-2c9c-4f63-abdd-067b075d5ca3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "6afb6e6e-2c9c-4f63-abdd-067b075d5ca3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "884072d3-90cc-4aa7-898c-97f02f3af275",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 64
}