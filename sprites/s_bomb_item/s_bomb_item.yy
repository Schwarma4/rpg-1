{
    "id": "9d5bcf7f-abcd-4e76-8cde-f0ea5b4e03cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bomb_item",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b26e76cc-6a6a-4140-88cc-8221ea0f189f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d5bcf7f-abcd-4e76-8cde-f0ea5b4e03cf",
            "compositeImage": {
                "id": "98d04e66-aa07-4de6-aada-d32ee507bac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b26e76cc-6a6a-4140-88cc-8221ea0f189f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6183c96-f27f-4ee2-afeb-dd66f65217d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b26e76cc-6a6a-4140-88cc-8221ea0f189f",
                    "LayerId": "0c1ca19a-10d4-4415-8e61-d3eb0fa14982"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0c1ca19a-10d4-4415-8e61-d3eb0fa14982",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d5bcf7f-abcd-4e76-8cde-f0ea5b4e03cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}