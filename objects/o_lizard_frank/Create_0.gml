event_inherited();
initialize_movement_entity(.5, .5, o_solid);


starting_state_ = lizard.idle;
state_ = starting_state_;

image_index = 0;
image_xscale = choose(1, -1);

alarm[1] = random_range(0, 1) * global.one_second;

myText_[0] = "Hey dude! Over here!";
myText_[1] = "I haven't met anyone in this forest in YEARS! Cool, bro!";
myText_[2] = "Wanna meet all of my lizard friends? Yes? HELL YEAH!";
myText_[3] = "Follow me into this dark forest. Watch out for hornets!";

MyName_ = "Frank";