{
    "id": "5591d8f4-156b-4a05-9e81-b1e93ca358c6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_input",
    "eventList": [
        {
            "id": "7b53e2d1-ba01-481a-be73-7a0ea59dcf54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5591d8f4-156b-4a05-9e81-b1e93ca358c6"
        },
        {
            "id": "ad2df107-35c3-4849-b0ae-3c9d48ec5f46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5591d8f4-156b-4a05-9e81-b1e93ca358c6"
        },
        {
            "id": "a0bc56df-d75e-4bb6-8270-e08426e96639",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "5591d8f4-156b-4a05-9e81-b1e93ca358c6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}