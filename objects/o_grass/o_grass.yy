{
    "id": "65171c23-635c-480e-ab31-eaea4af5313f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_grass",
    "eventList": [
        {
            "id": "be81cd57-859b-4f71-a6a5-98a422a04e6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "65171c23-635c-480e-ab31-eaea4af5313f"
        },
        {
            "id": "ad636715-b1e3-43b1-b388-c23b86fb2d4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8704a8a6-2273-4874-8986-2f1e41dc5331",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "65171c23-635c-480e-ab31-eaea4af5313f"
        },
        {
            "id": "d61d023a-9622-4a76-abb3-08a5fe6f49b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "65171c23-635c-480e-ab31-eaea4af5313f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "13189f20-d963-4385-b184-0a7bff234f36",
    "visible": true
}