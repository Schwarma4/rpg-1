{
    "id": "f1c9a551-2be0-48c2-98ee-13c77b98d604",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_melee_hitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 25,
    "bbox_right": 40,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b4726bc-dba6-43c9-958b-2fde9714645c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1c9a551-2be0-48c2-98ee-13c77b98d604",
            "compositeImage": {
                "id": "aa3f5e4c-4204-4a40-a8dc-ca4ebddc98e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b4726bc-dba6-43c9-958b-2fde9714645c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d47062ca-5e26-46e0-bb2e-8226443995d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b4726bc-dba6-43c9-958b-2fde9714645c",
                    "LayerId": "8e410541-f5b1-4014-9856-01dcd0cd246d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "8e410541-f5b1-4014-9856-01dcd0cd246d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1c9a551-2be0-48c2-98ee-13c77b98d604",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 16,
    "yorig": 24
}