{
    "id": "ac7733d2-bc55-417c-a71d-1ef72adc322a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_soild",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76afd527-08f6-4e07-8d46-30d28f6850c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac7733d2-bc55-417c-a71d-1ef72adc322a",
            "compositeImage": {
                "id": "de9aec87-bec9-48e4-87c8-1003d7014366",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76afd527-08f6-4e07-8d46-30d28f6850c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5987cdde-5610-40e3-acb7-8d66b8a01ca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76afd527-08f6-4e07-8d46-30d28f6850c1",
                    "LayerId": "20ed1a32-c8dd-422e-b2ad-f3e4a6171ce5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "20ed1a32-c8dd-422e-b2ad-f3e4a6171ce5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac7733d2-bc55-417c-a71d-1ef72adc322a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}