{
    "id": "dd533c36-fa69-499b-b3c6-3174a6006860",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pedestal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 16,
    "bbox_right": 46,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "141d23c1-8ffa-48af-8127-686a3aff6c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd533c36-fa69-499b-b3c6-3174a6006860",
            "compositeImage": {
                "id": "b11a7a7d-d8dd-44a1-9aa4-e0eaef9030de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "141d23c1-8ffa-48af-8127-686a3aff6c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "909fbed7-afb1-49f6-800d-f4cfc7430b9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "141d23c1-8ffa-48af-8127-686a3aff6c01",
                    "LayerId": "bd110e33-e958-4a57-b5c6-48c81fe11db7"
                }
            ]
        },
        {
            "id": "ef5309c5-03a4-446f-a22e-c2a9b1bbe177",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd533c36-fa69-499b-b3c6-3174a6006860",
            "compositeImage": {
                "id": "755ac5c0-4fda-49d4-820c-fb55f6e5a77b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef5309c5-03a4-446f-a22e-c2a9b1bbe177",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "483bdb33-920e-4fcb-bf4d-a1708e6b4133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef5309c5-03a4-446f-a22e-c2a9b1bbe177",
                    "LayerId": "bd110e33-e958-4a57-b5c6-48c81fe11db7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bd110e33-e958-4a57-b5c6-48c81fe11db7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd533c36-fa69-499b-b3c6-3174a6006860",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}