{
    "id": "49f5251c-f293-4a2e-aa32-c7db206481c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_fly_pause",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c68418b0-56c6-4c8a-84b4-4753017dddea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49f5251c-f293-4a2e-aa32-c7db206481c5",
            "compositeImage": {
                "id": "828e9a84-c14e-4b2f-b8aa-37a28cdb47d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c68418b0-56c6-4c8a-84b4-4753017dddea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98262a2a-5b63-47af-9bbf-7af2ce9824d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c68418b0-56c6-4c8a-84b4-4753017dddea",
                    "LayerId": "c74241dc-de5c-4d64-b23b-455bb7469afe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c74241dc-de5c-4d64-b23b-455bb7469afe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49f5251c-f293-4a2e-aa32-c7db206481c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 23
}