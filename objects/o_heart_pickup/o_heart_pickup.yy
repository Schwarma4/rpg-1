{
    "id": "6ed290c1-588e-4e19-b618-531eec356dd6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_heart_pickup",
    "eventList": [
        {
            "id": "9fc5c7f5-4b09-4ba0-9864-9b7d6656c3d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ed290c1-588e-4e19-b618-531eec356dd6"
        },
        {
            "id": "b67425fc-8de9-49c1-a6d3-70eb5d37fbe3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6ed290c1-588e-4e19-b618-531eec356dd6"
        },
        {
            "id": "115d6ed1-edd3-4dea-acbf-1296139d852b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "6ed290c1-588e-4e19-b618-531eec356dd6"
        },
        {
            "id": "e316aa88-c0bd-4e66-9a6e-b961574fb33d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "6ed290c1-588e-4e19-b618-531eec356dd6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8abdd0d5-6a40-4bf2-b52f-5d9751fb07e3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "61fd1865-c268-4f19-a726-10c379a4b731",
    "visible": true
}