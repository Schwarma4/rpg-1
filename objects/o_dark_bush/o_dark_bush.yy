{
    "id": "303e2a66-48ce-46b2-885b-3ed54abcade7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_dark_bush",
    "eventList": [
        {
            "id": "c0cd62d4-6408-401c-b186-0180b326e124",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "303e2a66-48ce-46b2-885b-3ed54abcade7"
        },
        {
            "id": "9ce2041c-c884-4d74-82d8-7e73d8aa446f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8704a8a6-2273-4874-8986-2f1e41dc5331",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "303e2a66-48ce-46b2-885b-3ed54abcade7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0aca68cb-5bc8-466d-bcbb-c9245a962434",
    "visible": true
}