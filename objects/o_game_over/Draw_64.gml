draw_set_halign(fa_center);
draw_text(x, y - 20, "Game Over");
for (var _i = 0; _i<gameover_options_length_; _i++) {
	if _i == index_ {
		draw_set_color(menu_color_);
	} else {
		draw_set_color(menu_dark_color_);
	}
	draw_text(x, y + _i * 12, gameover_options_[_i]);
}

draw_set_color(c_white);
draw_set_halign(fa_left);