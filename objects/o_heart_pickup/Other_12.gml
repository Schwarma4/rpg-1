/// @description PICKUP EVENT
instance_destroy();
global.player_two_health += 1;
if global.player_two_health > global.player_two_max_health {
	global.player_two_health = global.player_two_max_health;	
}
audio_play_sound(a_collect_item, 2, false);