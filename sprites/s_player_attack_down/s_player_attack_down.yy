{
    "id": "849d476b-afe4-4a6b-9879-d56e9f9192b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_attack_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 2,
    "bbox_right": 41,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db61197a-bad3-4e27-b830-9085fbd908a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849d476b-afe4-4a6b-9879-d56e9f9192b3",
            "compositeImage": {
                "id": "c9c933cd-23e1-47b3-8fd0-b65d769dad16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db61197a-bad3-4e27-b830-9085fbd908a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "874bb0cb-91e5-4105-9bc8-df666bbacfff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db61197a-bad3-4e27-b830-9085fbd908a6",
                    "LayerId": "7240f824-780b-4dee-ad16-252cfae31040"
                }
            ]
        },
        {
            "id": "7f744130-9beb-4713-a8bf-c799f61c8c93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849d476b-afe4-4a6b-9879-d56e9f9192b3",
            "compositeImage": {
                "id": "6eccda73-4e6d-458c-b10c-108f7edebee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f744130-9beb-4713-a8bf-c799f61c8c93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bcf9c2f-ef3d-47b3-ba0e-fed8e25ca595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f744130-9beb-4713-a8bf-c799f61c8c93",
                    "LayerId": "7240f824-780b-4dee-ad16-252cfae31040"
                }
            ]
        },
        {
            "id": "3292b403-fc79-4144-bcaf-cbeaf15cc2c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849d476b-afe4-4a6b-9879-d56e9f9192b3",
            "compositeImage": {
                "id": "557f2121-943d-4e89-9e70-a50febc9249c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3292b403-fc79-4144-bcaf-cbeaf15cc2c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a632d680-e818-4cb9-b24c-932b2f430a2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3292b403-fc79-4144-bcaf-cbeaf15cc2c6",
                    "LayerId": "7240f824-780b-4dee-ad16-252cfae31040"
                }
            ]
        },
        {
            "id": "58e335ca-2674-4f8f-9290-c51a0b9b3b5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849d476b-afe4-4a6b-9879-d56e9f9192b3",
            "compositeImage": {
                "id": "d9f8125e-76b9-4dff-80e2-2245ad7cdc06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58e335ca-2674-4f8f-9290-c51a0b9b3b5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84bee13e-d493-4af8-bfb0-6c46c40e6200",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58e335ca-2674-4f8f-9290-c51a0b9b3b5d",
                    "LayerId": "7240f824-780b-4dee-ad16-252cfae31040"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "7240f824-780b-4dee-ad16-252cfae31040",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "849d476b-afe4-4a6b-9879-d56e9f9192b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 23,
    "yorig": 27
}