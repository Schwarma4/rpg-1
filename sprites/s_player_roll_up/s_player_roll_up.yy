{
    "id": "09ed0eca-53bb-473f-94c7-b195b4c77b53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_roll_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 3,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1873c23a-7c72-4068-bacb-8097f511772c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09ed0eca-53bb-473f-94c7-b195b4c77b53",
            "compositeImage": {
                "id": "6a4d5136-62ed-49d8-84b8-f713c580bc5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1873c23a-7c72-4068-bacb-8097f511772c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "522bb0a4-38cd-4914-84c1-aa3495bc0ddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1873c23a-7c72-4068-bacb-8097f511772c",
                    "LayerId": "702600a4-cf1d-49bb-8d42-209f6bd030dd"
                }
            ]
        },
        {
            "id": "76e54480-17f6-435d-939e-5f9a2f21c2ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09ed0eca-53bb-473f-94c7-b195b4c77b53",
            "compositeImage": {
                "id": "513344ab-ea51-405d-9d1c-7267f04aac8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76e54480-17f6-435d-939e-5f9a2f21c2ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa5373f8-fe7f-46bb-991a-d10f51adedd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76e54480-17f6-435d-939e-5f9a2f21c2ad",
                    "LayerId": "702600a4-cf1d-49bb-8d42-209f6bd030dd"
                }
            ]
        },
        {
            "id": "163fe473-8e65-4ea4-85e4-697f7214a23f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09ed0eca-53bb-473f-94c7-b195b4c77b53",
            "compositeImage": {
                "id": "6b72148e-5554-4a06-a1f6-6f5a794b90cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "163fe473-8e65-4ea4-85e4-697f7214a23f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0929e892-d68f-48df-98e5-adf9dc61f85d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "163fe473-8e65-4ea4-85e4-697f7214a23f",
                    "LayerId": "702600a4-cf1d-49bb-8d42-209f6bd030dd"
                }
            ]
        },
        {
            "id": "6c928f45-a901-43fd-9558-50acd7a1ebd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09ed0eca-53bb-473f-94c7-b195b4c77b53",
            "compositeImage": {
                "id": "393f5564-e984-453e-bc7d-c8dc7599e7ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c928f45-a901-43fd-9558-50acd7a1ebd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "070d2d32-5c00-46bf-8cc3-09ee9d6c9f3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c928f45-a901-43fd-9558-50acd7a1ebd8",
                    "LayerId": "702600a4-cf1d-49bb-8d42-209f6bd030dd"
                }
            ]
        },
        {
            "id": "435b2f6b-acd3-4bae-9f09-966ac6547397",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09ed0eca-53bb-473f-94c7-b195b4c77b53",
            "compositeImage": {
                "id": "82716869-ba07-4b1c-a5bd-7b36400d5d70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "435b2f6b-acd3-4bae-9f09-966ac6547397",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31802742-da75-401b-a94d-215c1337bb80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "435b2f6b-acd3-4bae-9f09-966ac6547397",
                    "LayerId": "702600a4-cf1d-49bb-8d42-209f6bd030dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "702600a4-cf1d-49bb-8d42-209f6bd030dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09ed0eca-53bb-473f-94c7-b195b4c77b53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 24
}