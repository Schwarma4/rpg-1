{
    "id": "bf3a3b3e-0ec3-4602-94d6-c7dfc2a59368",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Metal_Thing_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85d27e62-8b9a-4d1f-8b31-e1493e626325",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf3a3b3e-0ec3-4602-94d6-c7dfc2a59368",
            "compositeImage": {
                "id": "d82c7932-b9aa-40e9-9991-b581dc2c115b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85d27e62-8b9a-4d1f-8b31-e1493e626325",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c9a9404-613a-4751-b38b-4e73b55180e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85d27e62-8b9a-4d1f-8b31-e1493e626325",
                    "LayerId": "4e2c1af7-150d-41a4-804d-835ff081beb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4e2c1af7-150d-41a4-804d-835ff081beb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf3a3b3e-0ec3-4602-94d6-c7dfc2a59368",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}