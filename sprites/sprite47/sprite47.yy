{
    "id": "2f202f80-2fa1-4c29-8464-136ff5b4e68d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite47",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8122d003-1a5a-4aad-8e69-f874a2e4a6d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f202f80-2fa1-4c29-8464-136ff5b4e68d",
            "compositeImage": {
                "id": "f391e8bf-0e09-4cfe-98fd-ef40d32b657d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8122d003-1a5a-4aad-8e69-f874a2e4a6d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b153559-faee-48f1-97b1-d6ce4f96a92b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8122d003-1a5a-4aad-8e69-f874a2e4a6d7",
                    "LayerId": "2e675510-6c67-4c92-80c6-3c45eca592c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2e675510-6c67-4c92-80c6-3c45eca592c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f202f80-2fa1-4c29-8464-136ff5b4e68d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}