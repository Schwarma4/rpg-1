{
    "id": "8abdd0d5-6a40-4bf2-b52f-5d9751fb07e3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_pickup",
    "eventList": [
        {
            "id": "69cf723f-298e-4f24-97f1-d57e65a1066f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8abdd0d5-6a40-4bf2-b52f-5d9751fb07e3"
        },
        {
            "id": "792521d2-ac99-4ad4-9c6f-44e5a5dcf962",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8abdd0d5-6a40-4bf2-b52f-5d9751fb07e3"
        },
        {
            "id": "cc07dcd4-9f9c-4d72-a6b3-6d3fbc0aa06e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8abdd0d5-6a40-4bf2-b52f-5d9751fb07e3"
        },
        {
            "id": "d4b61e15-a03b-4798-b2d9-21cdf403a685",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8abdd0d5-6a40-4bf2-b52f-5d9751fb07e3"
        },
        {
            "id": "62c0648d-4da9-4923-af11-4bfd1f4c3b80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "405ac906-b8ad-4e6f-82dc-f64f941e6f84",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8abdd0d5-6a40-4bf2-b52f-5d9751fb07e3"
        },
        {
            "id": "ff299f75-5405-4cf1-be85-a0c1e50aa281",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "567b4618-acdc-4dba-a473-cc37ca31a43a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8abdd0d5-6a40-4bf2-b52f-5d9751fb07e3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}