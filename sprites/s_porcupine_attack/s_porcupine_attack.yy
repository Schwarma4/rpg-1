{
    "id": "4441010e-70f5-4e4e-924f-de511ea75b77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_porcupine_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 1,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e12687c4-a2a5-47cc-94f7-4d016d43c7c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4441010e-70f5-4e4e-924f-de511ea75b77",
            "compositeImage": {
                "id": "d7855437-32c2-4bc3-ba39-e40d49aa3da6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e12687c4-a2a5-47cc-94f7-4d016d43c7c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faa8f7fd-9d47-41bf-a065-35e73f74eec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e12687c4-a2a5-47cc-94f7-4d016d43c7c4",
                    "LayerId": "feba8b24-9c34-4280-a912-21e59d013762"
                }
            ]
        },
        {
            "id": "11f7b6b6-154e-4b61-8240-96602f927276",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4441010e-70f5-4e4e-924f-de511ea75b77",
            "compositeImage": {
                "id": "a6bddaed-10ac-4ebf-b0e9-4d3dbd8e24da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11f7b6b6-154e-4b61-8240-96602f927276",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edd84e47-0f71-4fc5-9593-befaa0bf1f57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11f7b6b6-154e-4b61-8240-96602f927276",
                    "LayerId": "feba8b24-9c34-4280-a912-21e59d013762"
                }
            ]
        },
        {
            "id": "e0fbce1d-16e3-4291-ba6b-9a57bfd67f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4441010e-70f5-4e4e-924f-de511ea75b77",
            "compositeImage": {
                "id": "dc421fd2-d21f-410e-8cd1-9bcf2a22f216",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0fbce1d-16e3-4291-ba6b-9a57bfd67f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6be06607-85a1-46e6-95c9-6ab925db98fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0fbce1d-16e3-4291-ba6b-9a57bfd67f5a",
                    "LayerId": "feba8b24-9c34-4280-a912-21e59d013762"
                }
            ]
        },
        {
            "id": "349a384a-928f-4605-b1ad-8509b483f0c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4441010e-70f5-4e4e-924f-de511ea75b77",
            "compositeImage": {
                "id": "8c5a9281-1adc-49cc-839f-4a8e76cd7f8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "349a384a-928f-4605-b1ad-8509b483f0c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db579d5e-afa6-4bec-8347-d4504fbfb001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "349a384a-928f-4605-b1ad-8509b483f0c0",
                    "LayerId": "feba8b24-9c34-4280-a912-21e59d013762"
                }
            ]
        },
        {
            "id": "7b3777a3-b176-4181-a0a2-c2743f5b14bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4441010e-70f5-4e4e-924f-de511ea75b77",
            "compositeImage": {
                "id": "24493eef-d4ab-42a7-9a43-ce2c88535f19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b3777a3-b176-4181-a0a2-c2743f5b14bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4686539-8d16-471a-8145-51348f94a778",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b3777a3-b176-4181-a0a2-c2743f5b14bb",
                    "LayerId": "feba8b24-9c34-4280-a912-21e59d013762"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "feba8b24-9c34-4280-a912-21e59d013762",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4441010e-70f5-4e4e-924f-de511ea75b77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 23
}