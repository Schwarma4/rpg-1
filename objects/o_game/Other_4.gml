if global.load == true {
	instance_create_layer(global.start_x, global.start_y,"Instances", o_player_one);
	instance_create_layer(global.start_x, global.start_y,"Instances", o_player_two);
	global.load = false;
}


if instance_exists(global.player_start_position) {
	if instance_exists(o_player_one)  {
		o_player_one.persistent = false;	
		o_player_one.x = global.player_start_position.x;
		o_player_one.y = global.player_start_position.y;
		o_player_one.layer = layer_get_id("Instances");
	} else {
		var _start_x = global.player_start_position.x;
		var _start_y = global.player_start_position.y;
		instance_create_layer(_start_x, _start_y, "Instances", o_player_one);	
	}
	if instance_exists(o_player_two)  {
		o_player_two.persistent = false;	
		o_player_two.x = global.player_start_position.x;
		o_player_two.y = global.player_start_position.y;
		o_player_two.layer = layer_get_id("Instances");
	} else {
		var _start_x = global.player_start_position.x;
		var _start_y = global.player_start_position.y;
		instance_create_layer(_start_x, _start_y, "Instances", o_player_two);	
	}
	o_player_one.follower_state_ = global.follower_state_one;
	o_player_one.alarm[1] = global.one_second;
	o_player_two.follower_state_ = global.follower_state_two;
	o_player_two.alarm[1] = global.one_second;
}
