{
    "id": "eb32a416-de6c-4212-96b3-08794c63a3fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_swamp_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b62f9365-497f-4dc7-96ea-101c22a18b71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb32a416-de6c-4212-96b3-08794c63a3fc",
            "compositeImage": {
                "id": "aca69e0a-162f-49c2-939e-9a37d74d3d99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b62f9365-497f-4dc7-96ea-101c22a18b71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62be0032-9330-4bae-8fa8-452c677f5e67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b62f9365-497f-4dc7-96ea-101c22a18b71",
                    "LayerId": "16784a1c-1c74-49da-b510-e339e679b3af"
                }
            ]
        },
        {
            "id": "84422efe-f9f2-4f1d-926b-3d1d8aae33b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb32a416-de6c-4212-96b3-08794c63a3fc",
            "compositeImage": {
                "id": "75271b9b-cc74-4bde-9892-f24294f10aa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84422efe-f9f2-4f1d-926b-3d1d8aae33b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "381f1908-4766-40f3-a927-cb51c2a04128",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84422efe-f9f2-4f1d-926b-3d1d8aae33b4",
                    "LayerId": "16784a1c-1c74-49da-b510-e339e679b3af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "16784a1c-1c74-49da-b510-e339e679b3af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb32a416-de6c-4212-96b3-08794c63a3fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}