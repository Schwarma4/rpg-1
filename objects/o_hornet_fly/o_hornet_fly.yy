{
    "id": "0e39545d-60a5-47ba-99bb-b353f59197c8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_hornet_fly",
    "eventList": [
        {
            "id": "c3388401-ab7c-4965-a706-c13fa04021c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e39545d-60a5-47ba-99bb-b353f59197c8"
        },
        {
            "id": "988e6a9a-c694-4384-8694-d96343d27dfd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "0e39545d-60a5-47ba-99bb-b353f59197c8"
        },
        {
            "id": "9ef6ca02-1469-4324-803d-07cc27bf9783",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "0e39545d-60a5-47ba-99bb-b353f59197c8"
        },
        {
            "id": "2dabd7c1-3825-475d-8689-27563eb1a78f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "0e39545d-60a5-47ba-99bb-b353f59197c8"
        },
        {
            "id": "131e6b9a-b2b1-44fd-bf56-7a9d372a10ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "0e39545d-60a5-47ba-99bb-b353f59197c8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e45b7d6c-9a43-4674-a7e3-af78639eee20",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ccdb8ab6-3bd9-48f2-ad59-af64cd1433e4",
    "visible": true
}