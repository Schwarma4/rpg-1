{
    "id": "2391dae9-336d-46fc-98d7-108839af4683",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_lizard_frank",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 21,
    "bbox_top": 23,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed6b7c55-31aa-445e-b705-70038b57f5e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2391dae9-336d-46fc-98d7-108839af4683",
            "compositeImage": {
                "id": "7ba6ea3d-2aed-4564-a74e-00648911efd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed6b7c55-31aa-445e-b705-70038b57f5e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dfd3454-ce16-4168-8554-3cc33f537f15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed6b7c55-31aa-445e-b705-70038b57f5e8",
                    "LayerId": "fc611856-c650-45f0-912e-7ff32b360e7c"
                }
            ]
        },
        {
            "id": "e86788f9-bf2b-4e1a-bbb5-cc70fa609c0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2391dae9-336d-46fc-98d7-108839af4683",
            "compositeImage": {
                "id": "e47060bb-517f-4967-9af9-aff82cbfe5ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e86788f9-bf2b-4e1a-bbb5-cc70fa609c0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae085865-5068-4c15-9aa8-69c42c88a844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e86788f9-bf2b-4e1a-bbb5-cc70fa609c0c",
                    "LayerId": "fc611856-c650-45f0-912e-7ff32b360e7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fc611856-c650-45f0-912e-7ff32b360e7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2391dae9-336d-46fc-98d7-108839af4683",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}