{
    "id": "ccdb8ab6-3bd9-48f2-ad59-af64cd1433e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hornet_fly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 4,
    "bbox_right": 19,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c59e1c7-e24b-41cd-9f57-ba5dd93cfa15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccdb8ab6-3bd9-48f2-ad59-af64cd1433e4",
            "compositeImage": {
                "id": "19bb6978-b3a0-4d3e-8e1d-4320e3725568",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c59e1c7-e24b-41cd-9f57-ba5dd93cfa15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bdbe6a1-a2ed-47be-bf0e-aa32a4768c7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c59e1c7-e24b-41cd-9f57-ba5dd93cfa15",
                    "LayerId": "f2a354bf-c6bb-4069-be3a-d49861f0c4ad"
                }
            ]
        },
        {
            "id": "b1bb03c2-060c-4345-99b5-fb116295739f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccdb8ab6-3bd9-48f2-ad59-af64cd1433e4",
            "compositeImage": {
                "id": "8519a4df-90cc-40ea-828f-fc8ef135fe33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1bb03c2-060c-4345-99b5-fb116295739f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf5fa6bf-a434-4887-bca4-1cdfe68b3b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1bb03c2-060c-4345-99b5-fb116295739f",
                    "LayerId": "f2a354bf-c6bb-4069-be3a-d49861f0c4ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "f2a354bf-c6bb-4069-be3a-d49861f0c4ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccdb8ab6-3bd9-48f2-ad59-af64cd1433e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 23
}