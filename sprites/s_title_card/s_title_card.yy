{
    "id": "2fb94532-b17f-4abc-a04e-410fa5d90ed7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_title_card",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b00ba69-0e6f-4c1b-8077-01a83458d6e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb94532-b17f-4abc-a04e-410fa5d90ed7",
            "compositeImage": {
                "id": "955bc5e0-757a-424a-aaf9-a6873776ed1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b00ba69-0e6f-4c1b-8077-01a83458d6e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d98f99bf-91e8-413d-885b-981cc0ad5520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b00ba69-0e6f-4c1b-8077-01a83458d6e9",
                    "LayerId": "dca84b49-cb36-4ab7-a38d-c543a3ba03ae"
                },
                {
                    "id": "7a48f504-2330-4cc5-99eb-11a88497712c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b00ba69-0e6f-4c1b-8077-01a83458d6e9",
                    "LayerId": "dd53458e-a501-4faa-8e43-9d34972666dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "dd53458e-a501-4faa-8e43-9d34972666dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fb94532-b17f-4abc-a04e-410fa5d90ed7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "dca84b49-cb36-4ab7-a38d-c543a3ba03ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fb94532-b17f-4abc-a04e-410fa5d90ed7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}