{
    "id": "61fd1865-c268-4f19-a726-10c379a4b731",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_heart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23aa5e54-83bc-4a29-bc90-9aa164f9aee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61fd1865-c268-4f19-a726-10c379a4b731",
            "compositeImage": {
                "id": "ba217ae4-668a-4fe3-8735-5a519b6d3a2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23aa5e54-83bc-4a29-bc90-9aa164f9aee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "413e79c7-fdfd-4a0c-b310-c5e3ce738c29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23aa5e54-83bc-4a29-bc90-9aa164f9aee3",
                    "LayerId": "12ae6100-5208-4c5c-bc04-57a746baddb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "12ae6100-5208-4c5c-bc04-57a746baddb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61fd1865-c268-4f19-a726-10c379a4b731",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 7
}