/// @description MOVEMENT STATE

if global.current_player == 1 {
	image_speed = 0;
	var _x_input = o_input.right_ - o_input.left_;
	var _y_input = o_input.down_ - o_input.up_;
	var _input_direction = point_direction(0, 0, _x_input, _y_input);
	var _attack_input_ = o_input.action_one_pressed_;
	var _evade_input_ = o_input.action_two_pressed_;
	roll_dir = direction_facing_ * 90;

	if _x_input == 0 and _y_input == 0 {
		image_index = 0;
		image_speed = 0;
		apply_friction_to_movement_entity();
	} else {
		image_speed =  0.6;
		if _x_input != 0 {
			image_xscale = _x_input
		} else {
			image_xscale = 1;	
		}
		get_direction_facing(_input_direction);	
		add_movement_maxspeed(_input_direction, acceleration_, max_speed_);
		roll_direction_ = direction_facing_ * 90;
	}

	inventory_use_item(o_input.action_one_pressed_, global.item[0], 1);
	inventory_use_item(o_input.action_two_pressed_, global.item[1], 1);


	move_movement_entity(false);
	
	
} else {

	switch (o_player_one.follower_state_) {
		
		case 0:
			passive_test1(1);
			break;
		case 1:
			aggressive_test1(1);
			break;
		case 2:
			pacifist_test1(1);
			break;
		case 3:
			defend_test1(1);
			break;
	}

}
