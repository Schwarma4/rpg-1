{
    "id": "826bc2d7-6180-4d41-8755-e8cab20a7448",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_cat_Roper_bounce",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b2f1f9a-6713-4651-94f2-19b575b107b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826bc2d7-6180-4d41-8755-e8cab20a7448",
            "compositeImage": {
                "id": "1fa86fee-15c1-4aec-b380-05abc4fcc699",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b2f1f9a-6713-4651-94f2-19b575b107b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a53240ff-7f8e-4fd5-aa91-737fb1905442",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b2f1f9a-6713-4651-94f2-19b575b107b9",
                    "LayerId": "8d8a1805-616a-4e40-aac4-0fc1a87e41ce"
                }
            ]
        },
        {
            "id": "cb311438-66f7-4146-a38c-2ec192b5b1bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826bc2d7-6180-4d41-8755-e8cab20a7448",
            "compositeImage": {
                "id": "76d18550-2b82-45bf-be73-549b30a1cb1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb311438-66f7-4146-a38c-2ec192b5b1bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16a22722-eb7b-43c3-ad1d-a7f8305349a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb311438-66f7-4146-a38c-2ec192b5b1bd",
                    "LayerId": "8d8a1805-616a-4e40-aac4-0fc1a87e41ce"
                }
            ]
        },
        {
            "id": "1f186e77-041c-4f32-ab69-aad7fc15f866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826bc2d7-6180-4d41-8755-e8cab20a7448",
            "compositeImage": {
                "id": "fa1c73da-c023-4bd4-9607-fd92192b30b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f186e77-041c-4f32-ab69-aad7fc15f866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0c20647-e206-4cff-8d01-3a4baba35da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f186e77-041c-4f32-ab69-aad7fc15f866",
                    "LayerId": "8d8a1805-616a-4e40-aac4-0fc1a87e41ce"
                }
            ]
        },
        {
            "id": "8102b9a3-7715-47e0-b80a-836c85cca1df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826bc2d7-6180-4d41-8755-e8cab20a7448",
            "compositeImage": {
                "id": "f446ef7d-8044-475e-85f2-a560dde48722",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8102b9a3-7715-47e0-b80a-836c85cca1df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d628b29-60f1-41f7-b3e3-b7aae84784ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8102b9a3-7715-47e0-b80a-836c85cca1df",
                    "LayerId": "8d8a1805-616a-4e40-aac4-0fc1a87e41ce"
                }
            ]
        },
        {
            "id": "ed194b41-a582-4c45-b0c1-62f53e9f690a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826bc2d7-6180-4d41-8755-e8cab20a7448",
            "compositeImage": {
                "id": "ecc5320d-c199-4cf3-aecc-6cd3941d6882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed194b41-a582-4c45-b0c1-62f53e9f690a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceea3c1b-811e-4e0e-b12a-7aac85df81cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed194b41-a582-4c45-b0c1-62f53e9f690a",
                    "LayerId": "8d8a1805-616a-4e40-aac4-0fc1a87e41ce"
                }
            ]
        },
        {
            "id": "94b6de32-976c-4810-93c5-73917223ff44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826bc2d7-6180-4d41-8755-e8cab20a7448",
            "compositeImage": {
                "id": "79e3c968-30ac-4ecf-a328-be9d2ed09090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94b6de32-976c-4810-93c5-73917223ff44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5acbb0d4-142a-422b-8956-f149965e7d45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94b6de32-976c-4810-93c5-73917223ff44",
                    "LayerId": "8d8a1805-616a-4e40-aac4-0fc1a87e41ce"
                }
            ]
        },
        {
            "id": "174d37b9-ac63-474c-b1d4-03ab988c35dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826bc2d7-6180-4d41-8755-e8cab20a7448",
            "compositeImage": {
                "id": "2eeb19f8-585f-4e63-ad94-6d09d72c6852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "174d37b9-ac63-474c-b1d4-03ab988c35dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad5222c8-ab9f-4d7f-8861-360808a9044e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "174d37b9-ac63-474c-b1d4-03ab988c35dc",
                    "LayerId": "8d8a1805-616a-4e40-aac4-0fc1a87e41ce"
                }
            ]
        },
        {
            "id": "a3ebc965-c0d1-47d6-abfb-bc0d3269676d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826bc2d7-6180-4d41-8755-e8cab20a7448",
            "compositeImage": {
                "id": "d54b8dce-650c-4904-83a5-0b5f2588c043",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3ebc965-c0d1-47d6-abfb-bc0d3269676d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb1d6e74-baf6-4fd2-9b06-629dea33ffaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3ebc965-c0d1-47d6-abfb-bc0d3269676d",
                    "LayerId": "8d8a1805-616a-4e40-aac4-0fc1a87e41ce"
                }
            ]
        },
        {
            "id": "4c29cdf2-4ae5-46fb-9e45-95f8fffac0f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826bc2d7-6180-4d41-8755-e8cab20a7448",
            "compositeImage": {
                "id": "7df3030f-bb98-407e-98b8-4f486602a1e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c29cdf2-4ae5-46fb-9e45-95f8fffac0f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "036827d6-f878-4249-be60-1ca885287513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c29cdf2-4ae5-46fb-9e45-95f8fffac0f6",
                    "LayerId": "8d8a1805-616a-4e40-aac4-0fc1a87e41ce"
                }
            ]
        },
        {
            "id": "16b6bb6c-0bf6-4f16-87f0-200a8f9507b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826bc2d7-6180-4d41-8755-e8cab20a7448",
            "compositeImage": {
                "id": "2dd61968-2c5d-4530-b8f5-95b1665d4ce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16b6bb6c-0bf6-4f16-87f0-200a8f9507b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbcc5666-5ba3-45d7-b7c7-0a0d730e1464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16b6bb6c-0bf6-4f16-87f0-200a8f9507b0",
                    "LayerId": "8d8a1805-616a-4e40-aac4-0fc1a87e41ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "8d8a1805-616a-4e40-aac4-0fc1a87e41ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "826bc2d7-6180-4d41-8755-e8cab20a7448",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 23
}