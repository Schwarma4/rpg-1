{
    "id": "ebb871f6-9316-4b78-97dc-dfa79b0ed826",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_grass_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01c94e93-dfc9-4f81-b85b-54bc0ff74868",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebb871f6-9316-4b78-97dc-dfa79b0ed826",
            "compositeImage": {
                "id": "685c6bb0-e354-4b0b-845d-56775dcd1137",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01c94e93-dfc9-4f81-b85b-54bc0ff74868",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd675075-883b-4949-bd87-c59b81e73225",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c94e93-dfc9-4f81-b85b-54bc0ff74868",
                    "LayerId": "f78fc1a4-788b-4adf-bce5-3d64577c2a34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f78fc1a4-788b-4adf-bce5-3d64577c2a34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebb871f6-9316-4b78-97dc-dfa79b0ed826",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}