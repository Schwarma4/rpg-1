{
    "id": "ac8a4909-9cd7-40db-b843-eca84429d8ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_transparent_textbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a214a51a-bcb6-4fa8-bd14-6f8073e541df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac8a4909-9cd7-40db-b843-eca84429d8ad",
            "compositeImage": {
                "id": "3e01ec46-2a2d-414c-a7c3-c581faa56785",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a214a51a-bcb6-4fa8-bd14-6f8073e541df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71f389d2-6bf0-4c6c-9091-918f1f5f6963",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a214a51a-bcb6-4fa8-bd14-6f8073e541df",
                    "LayerId": "9dbafc25-e83c-4bbf-832a-bddca273fdfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 63,
    "layers": [
        {
            "id": "9dbafc25-e83c-4bbf-832a-bddca273fdfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac8a4909-9cd7-40db-b843-eca84429d8ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 140,
    "xorig": 0,
    "yorig": 0
}