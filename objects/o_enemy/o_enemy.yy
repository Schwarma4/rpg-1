{
    "id": "e45b7d6c-9a43-4674-a7e3-af78639eee20",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy",
    "eventList": [
        {
            "id": "aaefcf16-56d3-48c0-808b-a28a31f4e050",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e45b7d6c-9a43-4674-a7e3-af78639eee20"
        },
        {
            "id": "559169e3-175f-4709-baf2-d11efe39a1e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e45b7d6c-9a43-4674-a7e3-af78639eee20"
        },
        {
            "id": "26804fe0-6b8e-4574-b9fb-e226bf5b06be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8704a8a6-2273-4874-8986-2f1e41dc5331",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e45b7d6c-9a43-4674-a7e3-af78639eee20"
        },
        {
            "id": "e5cffdc5-7280-4f40-bed9-f5f3c69d1396",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e45b7d6c-9a43-4674-a7e3-af78639eee20"
        },
        {
            "id": "38347e47-6502-4cb4-a94d-6e0ab6e604d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e45b7d6c-9a43-4674-a7e3-af78639eee20"
        },
        {
            "id": "fb0e6efd-0c2e-43fd-a2be-2c89e053e296",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e45b7d6c-9a43-4674-a7e3-af78639eee20"
        },
        {
            "id": "00d133bd-8042-4330-ab2f-a9cc5fdfde93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e45b7d6c-9a43-4674-a7e3-af78639eee20"
        },
        {
            "id": "c5bf6d97-8123-40c9-a7b9-d7bc1e01f298",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e45b7d6c-9a43-4674-a7e3-af78639eee20",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e45b7d6c-9a43-4674-a7e3-af78639eee20"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}