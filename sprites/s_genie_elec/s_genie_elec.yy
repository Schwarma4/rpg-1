{
    "id": "3729f9bf-6c85-49b2-bdd7-57dc50de3b1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_genie_elec",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fedc9f01-f2dc-4f3c-9503-586f3029b814",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3729f9bf-6c85-49b2-bdd7-57dc50de3b1d",
            "compositeImage": {
                "id": "20c34ba9-d3e6-4e14-8829-e1e214c1a63c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fedc9f01-f2dc-4f3c-9503-586f3029b814",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa464dd0-ef4c-44ca-b7cd-e2a69efa96c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fedc9f01-f2dc-4f3c-9503-586f3029b814",
                    "LayerId": "30680361-6518-4f07-ad85-1737abac96cf"
                }
            ]
        },
        {
            "id": "f547f555-3461-4416-9e2d-ea7a883d3432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3729f9bf-6c85-49b2-bdd7-57dc50de3b1d",
            "compositeImage": {
                "id": "b3d1d1ea-e53d-46f6-a531-8e6ea1fc69d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f547f555-3461-4416-9e2d-ea7a883d3432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3b1a57b-9e11-4ff7-ab48-799fa7d77860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f547f555-3461-4416-9e2d-ea7a883d3432",
                    "LayerId": "30680361-6518-4f07-ad85-1737abac96cf"
                }
            ]
        },
        {
            "id": "344626b3-8000-4441-a42a-2c2f74cd8811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3729f9bf-6c85-49b2-bdd7-57dc50de3b1d",
            "compositeImage": {
                "id": "d26dc3d4-ff3a-45ff-9ea2-064b589032d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "344626b3-8000-4441-a42a-2c2f74cd8811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a68e174f-4a10-415c-94e8-796391575724",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "344626b3-8000-4441-a42a-2c2f74cd8811",
                    "LayerId": "30680361-6518-4f07-ad85-1737abac96cf"
                }
            ]
        },
        {
            "id": "83c7f496-17d3-48d9-a39c-65dfcc47a31c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3729f9bf-6c85-49b2-bdd7-57dc50de3b1d",
            "compositeImage": {
                "id": "1e5f72e5-1557-4759-801b-9d469f27037e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83c7f496-17d3-48d9-a39c-65dfcc47a31c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6610bff1-66fa-4098-ad2e-41475b5608fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83c7f496-17d3-48d9-a39c-65dfcc47a31c",
                    "LayerId": "30680361-6518-4f07-ad85-1737abac96cf"
                }
            ]
        },
        {
            "id": "7f4a171b-81ee-448e-b102-06222dd45080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3729f9bf-6c85-49b2-bdd7-57dc50de3b1d",
            "compositeImage": {
                "id": "7049881c-18ad-44df-b3cb-841473692a07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f4a171b-81ee-448e-b102-06222dd45080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9427428b-4ebd-43d6-abf1-c8267d421729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f4a171b-81ee-448e-b102-06222dd45080",
                    "LayerId": "30680361-6518-4f07-ad85-1737abac96cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "30680361-6518-4f07-ad85-1737abac96cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3729f9bf-6c85-49b2-bdd7-57dc50de3b1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}