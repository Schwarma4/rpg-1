{
    "id": "25f05a28-7c02-4a04-8c34-a514048ad117",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20bb5659-c48b-4ab6-8ad7-ce810116550c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25f05a28-7c02-4a04-8c34-a514048ad117",
            "compositeImage": {
                "id": "1eecc1f0-35e6-4022-9265-e5256e2e81a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20bb5659-c48b-4ab6-8ad7-ce810116550c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f20ee79c-2642-4e35-8695-6d67391057cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20bb5659-c48b-4ab6-8ad7-ce810116550c",
                    "LayerId": "9e2a558e-a0ed-4ada-aa71-cd5e6e42c6a3"
                }
            ]
        },
        {
            "id": "6f0ca25f-9174-4d82-9fef-c1b953bd7ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25f05a28-7c02-4a04-8c34-a514048ad117",
            "compositeImage": {
                "id": "e8676270-eb2c-4117-a94e-fe8043af8615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f0ca25f-9174-4d82-9fef-c1b953bd7ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5290ce0-34f4-4904-b3b5-4d43c5cb67bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f0ca25f-9174-4d82-9fef-c1b953bd7ed2",
                    "LayerId": "9e2a558e-a0ed-4ada-aa71-cd5e6e42c6a3"
                }
            ]
        },
        {
            "id": "b96c071d-47ea-45e2-8f71-763bc1c23ff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25f05a28-7c02-4a04-8c34-a514048ad117",
            "compositeImage": {
                "id": "b3baa453-f371-49ce-9513-cb162bf7d2e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b96c071d-47ea-45e2-8f71-763bc1c23ff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cef0e53c-b808-4ff7-b066-46a5c9238996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b96c071d-47ea-45e2-8f71-763bc1c23ff7",
                    "LayerId": "9e2a558e-a0ed-4ada-aa71-cd5e6e42c6a3"
                }
            ]
        },
        {
            "id": "158621a3-ce53-4bdf-96c3-ce32ca28c120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25f05a28-7c02-4a04-8c34-a514048ad117",
            "compositeImage": {
                "id": "c7b46d09-99e2-4554-898c-de18ccc70d93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "158621a3-ce53-4bdf-96c3-ce32ca28c120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e14f4a94-4dea-46e3-bf05-ce145305409a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "158621a3-ce53-4bdf-96c3-ce32ca28c120",
                    "LayerId": "9e2a558e-a0ed-4ada-aa71-cd5e6e42c6a3"
                }
            ]
        },
        {
            "id": "14567f4a-47f6-4b92-b090-02c42278c39a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25f05a28-7c02-4a04-8c34-a514048ad117",
            "compositeImage": {
                "id": "2dceb377-8173-4509-b794-abae09d0c2d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14567f4a-47f6-4b92-b090-02c42278c39a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d1843d8-7347-4bcb-b97f-66b3ed9463b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14567f4a-47f6-4b92-b090-02c42278c39a",
                    "LayerId": "9e2a558e-a0ed-4ada-aa71-cd5e6e42c6a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "9e2a558e-a0ed-4ada-aa71-cd5e6e42c6a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25f05a28-7c02-4a04-8c34-a514048ad117",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 23
}