{
    "id": "23f9ea91-a313-403c-8436-f2657965587b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_attack_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 3,
    "bbox_right": 41,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17ba9f2e-e6d9-4968-9528-07e954539d7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23f9ea91-a313-403c-8436-f2657965587b",
            "compositeImage": {
                "id": "d90aee23-8b54-41f4-ba99-ab72549fa2b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17ba9f2e-e6d9-4968-9528-07e954539d7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49b77ea7-812d-4cfe-8a61-225a982a7794",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17ba9f2e-e6d9-4968-9528-07e954539d7a",
                    "LayerId": "98ea163d-010f-490e-a6f0-28453787213f"
                }
            ]
        },
        {
            "id": "b23e761d-b63e-4e8e-90b1-eb71c499b0eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23f9ea91-a313-403c-8436-f2657965587b",
            "compositeImage": {
                "id": "d535ebf2-855e-4bc7-8e15-2c50d5812f97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b23e761d-b63e-4e8e-90b1-eb71c499b0eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b95797-2ead-4db1-921b-3c85f8612c13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b23e761d-b63e-4e8e-90b1-eb71c499b0eb",
                    "LayerId": "98ea163d-010f-490e-a6f0-28453787213f"
                }
            ]
        },
        {
            "id": "eb868a70-1a20-4af0-bf8f-80ac999177a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23f9ea91-a313-403c-8436-f2657965587b",
            "compositeImage": {
                "id": "9a567e2c-ba18-46ae-87b3-097d23b9e991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb868a70-1a20-4af0-bf8f-80ac999177a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9fe663c-7385-4a61-8ef0-2976b5c38e2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb868a70-1a20-4af0-bf8f-80ac999177a1",
                    "LayerId": "98ea163d-010f-490e-a6f0-28453787213f"
                }
            ]
        },
        {
            "id": "de9d7911-a569-4250-abeb-011a0f57fc82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23f9ea91-a313-403c-8436-f2657965587b",
            "compositeImage": {
                "id": "53041c58-2c08-4a33-bb37-1302f642a55d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de9d7911-a569-4250-abeb-011a0f57fc82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f7cd3fa-d234-4918-9269-58bb7ff0baf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de9d7911-a569-4250-abeb-011a0f57fc82",
                    "LayerId": "98ea163d-010f-490e-a6f0-28453787213f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "98ea163d-010f-490e-a6f0-28453787213f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23f9ea91-a313-403c-8436-f2657965587b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 14,
    "yorig": 33
}