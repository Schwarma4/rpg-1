{
    "id": "c2eeee32-6d96-4f6d-8c3d-778d6f68b294",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_gem_shine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 10,
    "bbox_right": 23,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "083419d3-674b-471d-8daa-2e52bc681558",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2eeee32-6d96-4f6d-8c3d-778d6f68b294",
            "compositeImage": {
                "id": "a9dc2469-423e-42db-b734-16f8fb98328b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "083419d3-674b-471d-8daa-2e52bc681558",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cc3cf33-62b5-4f45-accc-6ce658d20bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "083419d3-674b-471d-8daa-2e52bc681558",
                    "LayerId": "4ee11413-9a98-47e1-ad7f-a70b45f0d363"
                }
            ]
        },
        {
            "id": "34bbc6d3-b352-4882-8c92-41eb8c35e746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2eeee32-6d96-4f6d-8c3d-778d6f68b294",
            "compositeImage": {
                "id": "7082042b-fb58-4994-b9d6-430456ad7d59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34bbc6d3-b352-4882-8c92-41eb8c35e746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "882214de-7a11-4545-b984-8cff243deb50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34bbc6d3-b352-4882-8c92-41eb8c35e746",
                    "LayerId": "4ee11413-9a98-47e1-ad7f-a70b45f0d363"
                }
            ]
        },
        {
            "id": "40ee18d5-6cc2-4337-bfec-399503e7291a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2eeee32-6d96-4f6d-8c3d-778d6f68b294",
            "compositeImage": {
                "id": "b2eb49b1-62cf-47f7-a214-adea044ad64d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40ee18d5-6cc2-4337-bfec-399503e7291a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41d96546-ab9e-423c-b922-27f04a85733c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40ee18d5-6cc2-4337-bfec-399503e7291a",
                    "LayerId": "4ee11413-9a98-47e1-ad7f-a70b45f0d363"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4ee11413-9a98-47e1-ad7f-a70b45f0d363",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2eeee32-6d96-4f6d-8c3d-778d6f68b294",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}