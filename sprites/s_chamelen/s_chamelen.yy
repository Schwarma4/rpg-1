{
    "id": "9f8f694e-f77b-4c1c-8723-d627ffb1f2e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_chamelen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "842bf48d-298d-4927-b679-cbac7e40867f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f8f694e-f77b-4c1c-8723-d627ffb1f2e3",
            "compositeImage": {
                "id": "86344a47-6d9a-425e-be2a-2e44f0b93954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "842bf48d-298d-4927-b679-cbac7e40867f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5354a552-0375-42a6-852f-70e2b3af367a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "842bf48d-298d-4927-b679-cbac7e40867f",
                    "LayerId": "2ef32c2d-5d53-4434-b67b-03efd3116280"
                }
            ]
        },
        {
            "id": "b0982892-5a0d-4c5e-bacf-3de699ba5c7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f8f694e-f77b-4c1c-8723-d627ffb1f2e3",
            "compositeImage": {
                "id": "2dbf1ed5-add0-4748-b306-32ccff521184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0982892-5a0d-4c5e-bacf-3de699ba5c7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a74f3be4-5b28-4f33-8977-9096867ca420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0982892-5a0d-4c5e-bacf-3de699ba5c7d",
                    "LayerId": "2ef32c2d-5d53-4434-b67b-03efd3116280"
                }
            ]
        },
        {
            "id": "fb683147-8abb-4b20-bd54-6b3c5194e754",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f8f694e-f77b-4c1c-8723-d627ffb1f2e3",
            "compositeImage": {
                "id": "ccd6c1a3-6092-4911-b2e2-ed558fe9761c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb683147-8abb-4b20-bd54-6b3c5194e754",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d031291a-94f4-475d-bce1-55c2c179e333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb683147-8abb-4b20-bd54-6b3c5194e754",
                    "LayerId": "2ef32c2d-5d53-4434-b67b-03efd3116280"
                }
            ]
        },
        {
            "id": "57189e00-464f-47ad-8ee1-fdceb579b460",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f8f694e-f77b-4c1c-8723-d627ffb1f2e3",
            "compositeImage": {
                "id": "f0f7fe5e-3101-4084-a4db-f82a2c165dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57189e00-464f-47ad-8ee1-fdceb579b460",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c51e1969-07a1-4f6f-a4a0-fb2aa68f6ea9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57189e00-464f-47ad-8ee1-fdceb579b460",
                    "LayerId": "2ef32c2d-5d53-4434-b67b-03efd3116280"
                }
            ]
        },
        {
            "id": "0f483d20-6dd8-42e8-8ddd-8cdbcecfbcd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f8f694e-f77b-4c1c-8723-d627ffb1f2e3",
            "compositeImage": {
                "id": "1e898b12-e236-445f-be4d-a916519c23d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f483d20-6dd8-42e8-8ddd-8cdbcecfbcd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebee20b7-212c-4cf7-84ff-c9774ebb1982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f483d20-6dd8-42e8-8ddd-8cdbcecfbcd4",
                    "LayerId": "2ef32c2d-5d53-4434-b67b-03efd3116280"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "2ef32c2d-5d53-4434-b67b-03efd3116280",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f8f694e-f77b-4c1c-8723-d627ffb1f2e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 23
}