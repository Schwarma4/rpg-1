/// @arg player
var _player = argument0;
var _stamina = global.player_one_stamina;
var _max_stamina = global.player_one_max_stamina;
var _health = global.player_one_health;
var _max_health = global.player_one_max_health;
var _player_object = o_player_two;
var _item_0 = global.item[0];
var _item_1 = global.item[1];

if _player == 2 {
	_stamina = global.player_two_stamina;
	_max_stamina = global.player_two_max_stamina;
	_health = global.player_two_health;
	_max_health = global.player_two_max_health;
	_player_object = o_player_one;
	_item_0 = global.item[2];
	_item_1 = global.item[3];
}

//slow down to stop
image_index = 0;
apply_friction_to_movement_entity();

//attack enemy with ranged if close enough or sword or melee if too close
if instance_exists(o_enemy) {
	//face nearest enemy
	var ex, ey;
	ex = instance_nearest(x, y, o_enemy).x;
	ey = instance_nearest(x, y, o_enemy).y;
	direction_ = point_direction(x, y, ex, ey);
	set_movement(direction_, max_speed_);
	set_sprite_facing();
	get_direction_facing(direction_);	
	
	if point_distance(x, y, ex, ey) < 64 && point_distance(x, y, ex, ey) > 22 {
		if (instance_exists(_item_0) && _item_0.action_ == player.ranged && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.ranged && _item_1.cost_ <= _stamina) {	
			direction_ = point_direction(x, y, ex, ey);
			set_sprite_facing();
			get_direction_facing(direction_);	
			if _item_0.action_ == player.ranged {
				image_speed = 0;
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;	
			} else {
				image_speed = 0;
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;	
			}
		}
	} else if point_distance(x, y, ex, ey) <= 22 {
		if (instance_exists(_item_0) && _item_0.action_ == player.sword && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.sword && _item_1.cost_ <= _stamina) {	
			direction_ = point_direction(x, y, ex, ey);
			set_sprite_facing();
			get_direction_facing(direction_);			
			if _item_0.action_ == player.sword {
				image_speed = 0;
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else if _item_1.action_ == player.sword {
				image_speed = 0;
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			}
		} else if (instance_exists(_item_0) && _item_0.action_ == player.melee && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.melee && _item_1.cost_ <= _stamina) {	
			direction_ = point_direction(x, y, ex, ey);
			set_sprite_facing();
			get_direction_facing(direction_);	
			if _item_0.action_ == player.melee {
				image_speed = 0;
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			} else if _item_1.action_ == player.melee {
				image_speed = 0;
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
				_stamina = max(0, _stamina);		
				alarm[1] = global.one_second;
			}
		}
	}
}


if _player == 2 {
	global.player_two_stamina = _stamina;
	global.player_two_health = _health;
} else {
	global.player_one_stamina = _stamina;
	global.player_one_health = _health;
}