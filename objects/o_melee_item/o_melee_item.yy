{
    "id": "4b4f3157-509b-4757-970e-70d9aa57711c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_melee_item",
    "eventList": [
        {
            "id": "f93a87da-0974-4863-9225-a3d1bb622335",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4b4f3157-509b-4757-970e-70d9aa57711c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d43e2dfd-4c7c-4148-8835-6544f350ca47",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "abb91329-12c5-46b2-bf70-4cbee6ce0940",
    "visible": false
}