{
    "id": "c980f9ad-dd5f-4907-af4e-8db5a2a2b43d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_roper_cat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e3b275d-35be-4405-af81-d4d61a114714",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c980f9ad-dd5f-4907-af4e-8db5a2a2b43d",
            "compositeImage": {
                "id": "391e0888-995b-4a16-8882-4dd53afea795",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e3b275d-35be-4405-af81-d4d61a114714",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e6026d7-e06c-4b9b-ad16-2ad14e85f5cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e3b275d-35be-4405-af81-d4d61a114714",
                    "LayerId": "0e9c0423-34c6-4c0f-8bb7-852dfa7fb28b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "0e9c0423-34c6-4c0f-8bb7-852dfa7fb28b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c980f9ad-dd5f-4907-af4e-8db5a2a2b43d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 11,
    "yorig": 20
}