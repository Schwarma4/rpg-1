/// @arg player
var _player = argument0;
var _stamina = global.player_one_stamina;
var _max_stamina = global.player_one_max_stamina;
var _health = global.player_one_health;
var _max_health = global.player_one_max_health;
var _player_object = o_player_two;
var _item_0 = global.item[0];
var _item_1 = global.item[1];

if _player == 2 {
	_stamina = global.player_two_stamina;
	_max_stamina = global.player_two_max_stamina;
	_health = global.player_two_health;
	_max_health = global.player_two_max_health;
	_player_object = o_player_one;
	_item_0 = global.item[2];
	_item_1 = global.item[3];
}

if !instance_exists(_player_object) {
	if _player = 1 {
		global.current_player = 2;	
	} else {
		global.current_player = 1;	
	}
	exit;
}

image_speed = 0.6;
var px, py;
px = instance_nearest(x, y, _player_object).x;
py = instance_nearest(x, y, _player_object).y;
var hx, hy;
hx = noone;
hy = noone;

if instance_exists(o_heart_pickup).x && instance_exists(o_heart_pickup).y && instance_exists(o_heart_pickup) {
	hx = instance_nearest(x, y, o_heart_pickup).x;
	hy = instance_nearest(x, y, o_heart_pickup).y;
}

if instance_exists(o_enemy) {
	var ex, ey, e;
	ex = instance_nearest(x, y, o_enemy).x;
	ey = instance_nearest(x, y, o_enemy).y;		
	espeed = instance_nearest(x, y, o_enemy).speed_;
	edir = instance_nearest(x, y, o_enemy).direction_;
	direction_ = point_direction(x, y, ex, ey);
	roll_direction_ = point_direction(x, y, _player_object.x, _player_object.y);		
	var test = point_distance(x + lengthdir_x(speed_,direction_), y + lengthdir_y(speed_,direction_), instance_nearest(x, y, o_enemy).x + lengthdir_x(instance_nearest(x, y, o_enemy).speed_, instance_nearest(x, y, o_enemy).direction_), instance_nearest(x, y, o_enemy).y + lengthdir_y(instance_nearest(x, y, o_enemy).speed_, instance_nearest(x, y, o_enemy).direction_));
	var test2 = point_distance(x, y, instance_nearest(x, y, o_enemy).x, instance_nearest(x, y, o_enemy).y);
	//pick up heart if nearby and low on health
	if instance_exists(o_heart_pickup) &&  point_distance(x, y, instance_nearest(x, y, o_heart_pickup).x, instance_nearest(x, y, o_heart_pickup).y) < 36 && _health <= 1 {
	roll_direction_ = point_direction(x, y, instance_nearest(x, y, o_heart_pickup).x, instance_nearest(x, y, o_heart_pickup).y);		
	direction_ = point_direction(x, y, instance_nearest(x, y, o_heart_pickup).x, instance_nearest(x, y, o_heart_pickup).y);
		if ((instance_exists(_item_0) && _item_0.action_ == player.evade && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.evade && _item_1.cost_ <= _stamina))  && point_distance(x + lengthdir_x(roll_speed_,roll_direction_), y + lengthdir_y(roll_speed_,roll_direction_), o_enemy.x + lengthdir_x(o_enemy.speed_, o_enemy.direction_), o_enemy.y + lengthdir_y(o_enemy.speed_, o_enemy.direction_)) > 64 {
			set_sprite_facing();
			get_direction_facing(roll_direction_);
			add_movement_maxspeed(direction_, acceleration_, max_speed_);
			if _item_0.action_ == player.evade {
				image_index = 0;
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
			} else {
				image_index = 0;
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
			}
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		} else {
			set_sprite_facing();
			get_direction_facing(direction_);
			add_movement_maxspeed(direction_, acceleration_, max_speed_);
			move_movement_entity(true);
		}
// if enemy is between 45 and 22 pixels away and player has ranged attack, fire weapon
	} else if speed_ == 0 && point_distance(x, y, instance_nearest(x, y, o_enemy).x, instance_nearest(x, y, o_enemy).y) < 45 && point_distance(x, y, instance_nearest(x, y, o_enemy).x, instance_nearest(x, y, o_enemy).y) > 22 && (instance_exists(_item_0) && _item_0.action_ == player.ranged && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.ranged && _item_1.cost_ <= _stamina) {	
		direction_ = point_direction(x, y, instance_nearest(x, y, o_enemy).x, instance_nearest(x, y, o_enemy).y);
		set_sprite_facing();
		get_direction_facing(direction_);	
		move_movement_entity(true);	
		if _item_0.action_ == player.ranged {
			image_index = 0;
			state_ = _item_0.action_;
			_stamina -= _item_0.cost_;
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		} else {
			image_index = 0;
			state_ = _item_1.action_;
			_stamina -= _item_1.cost_;
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		}	
	} else if  point_distance(x + lengthdir_x(speed_,direction_), y + lengthdir_y(speed_,direction_), instance_nearest(x, y, o_enemy).x + lengthdir_x(instance_nearest(x, y, o_enemy).speed_, instance_nearest(x, y, o_enemy).direction_), instance_nearest(x, y, o_enemy).y + lengthdir_y(instance_nearest(x, y, o_enemy).speed_, instance_nearest(x, y, o_enemy).direction_)) < 22 && ((instance_exists(_item_0) && _item_0.action_ == player.sword || instance_exists(_item_1) && _item_1.action_ == player.sword) || (instance_exists(_item_0) && _item_0.action_ == player.melee || instance_exists(_item_1) && _item_1.action_ == player.melee)) {
		image_speed = 0.6;
		direction_ = point_direction(x, y, ex, ey);
		set_movement(direction_, 0.5);
		set_sprite_facing();
		get_direction_facing(direction_);	
		if _item_0.action_ == player.sword && _item_0.cost_ <= _stamina {
			image_index = 0;
			state_ = _item_0.action_;
			_stamina -= _item_0.cost_;
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		} else if _item_1.action_ == player.sword  && _item_1.cost_ <= _stamina {
			image_index = 0;
			state_ = _item_1.action_;
			_stamina -= _item_1.cost_;
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		} else if _item_0.action_ == player.melee && _item_0.cost_ <= _stamina {
			image_index = 0;
			state_ = _item_0.action_;
			_stamina -= _item_0.cost_;
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		} else if _item_1.action_ == player.melee && _item_1.cost_ <= _stamina {
			image_index = 0;
			state_ = _item_1.action_;
			_stamina -= _item_1.cost_;
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		}
	} else if  point_distance(x, y, px, py) > 52 && _stamina = _max_stamina && ((instance_exists(_item_0) && _item_0.action_ == player.evade && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.evade && _item_1.cost_ <= _stamina)) && point_distance(x + lengthdir_x(roll_speed_,roll_direction_), y + lengthdir_y(roll_speed_,roll_direction_), ex + lengthdir_x(espeed, edir), ey + lengthdir_y(espeed, edir)) > 72 {	
		//set_movement(roll_direction_, 0.5);
		set_sprite_facing();
		get_direction_facing(roll_direction_);
		if _item_0.action_ == player.evade {
			image_index = 0;
			state_ = _item_0.action_;
			_stamina -= _item_0.cost_;
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		} else if _item_1.action_ == player.evade {
			image_index = 0;
			state_ = _item_1.action_;
			_stamina -= _item_1.cost_;
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		}
	} else {
		direction_ = point_direction(x, y, _player_object.x, _player_object.y);
		set_sprite_facing();
		get_direction_facing(direction_);
		if point_distance(x, y, px, py) > 36 {	
			add_movement_maxspeed(direction_, acceleration_, max_speed_);
			move_movement_entity(true);
		} else {
			apply_friction_to_movement_entity();
			image_speed = 0;
			image_index = 0;
		}
	}
} else if hx != -4 {
	if point_distance(x, y, hx, hy) < 36 {
		roll_direction_ = point_direction(x, y, hx, hy);		
		direction_ = point_direction(x, y, hx, hy);
		if ((instance_exists(_item_0) && _item_0.action_ == player.evade && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.evade && _item_1.cost_ <= _stamina)) {
			set_sprite_facing();
			get_direction_facing(roll_direction_);
			if _item_0.action_ == player.evade {
				image_index = 0;
				state_ = _item_0.action_;
				_stamina -= _item_0.cost_;
			} else {
				image_index = 0;
				state_ = _item_1.action_;
				_stamina -= _item_1.cost_;
			}
			_stamina = max(0, _stamina);		
			alarm[1] = global.one_second;
		} else {
			set_sprite_facing();
			get_direction_facing(direction_);
			add_movement_maxspeed(direction_, acceleration_, max_speed_);
			move_movement_entity(true);
		}
	} else {
		direction_ = point_direction(x, y, px, py);	
		set_sprite_facing();
		get_direction_facing(direction_);
		if point_distance(x, y, px, py) > 36 {	
			add_movement_maxspeed(direction_, acceleration_, max_speed_);
			move_movement_entity(true);
		} else {
			apply_friction_to_movement_entity();
			image_speed = 0;
			image_index = 0;
		}
	}
} else {
	if point_distance(x, y, px, py) > 52 && ((instance_exists(_item_0) && _item_0.action_ == player.evade && _item_0.cost_ <= _stamina) || (instance_exists(_item_1) && _item_1.action_ == player.evade && _item_1.cost_ <= _stamina)) {		
		roll_direction_ = point_direction(x, y, _player_object.x, _player_object.y);
		set_sprite_facing();
		get_direction_facing(roll_direction_);
		if _item_0.action_ == player.evade {
				image_index = 0;
			state_ = _item_0.action_;
			_stamina -= _item_0.cost_;
		} else {
				image_index = 0;
			state_ = _item_1.action_;
			_stamina -= _item_1.cost_;
		}
		_stamina = max(0, _stamina);		
		alarm[1] = global.one_second;
	} else {
		direction_ = point_direction(x, y, _player_object.x, _player_object.y);
		//set_movement(direction_, 0.5);
		set_sprite_facing();
		get_direction_facing(direction_);
		if point_distance(x, y, px, py) > 36 {	
			add_movement_maxspeed(direction_, acceleration_, max_speed_);
			move_movement_entity(true);
		} else {
			apply_friction_to_movement_entity();
			image_speed = 0;
			image_index = 0;
		}
	}
}


if _player == 2 {
	global.player_two_stamina = _stamina;
	global.player_two_health = _health;
} else {
	global.player_one_stamina = _stamina;
	global.player_one_health = _health;
}

