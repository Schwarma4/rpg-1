{
    "id": "933ddf5c-24d5-4fbf-b947-bcabef5973c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_sword_hitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 16,
    "bbox_right": 40,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb8282ef-cb7b-46be-84d9-87cfc1a02882",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933ddf5c-24d5-4fbf-b947-bcabef5973c5",
            "compositeImage": {
                "id": "a26f3f3e-e5bc-4d80-9d2a-796ff7211e45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb8282ef-cb7b-46be-84d9-87cfc1a02882",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a896f26-ec96-4b2f-b247-50ac2c3afb4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb8282ef-cb7b-46be-84d9-87cfc1a02882",
                    "LayerId": "0b38e063-b915-42b4-9792-1787c028a487"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "0b38e063-b915-42b4-9792-1787c028a487",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "933ddf5c-24d5-4fbf-b947-bcabef5973c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 16,
    "yorig": 24
}