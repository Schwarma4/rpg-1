{
    "id": "f2c6e64f-5d8f-4487-9943-ce06813193da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite57",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ed74ecb-1580-4972-bef0-697a42bd7e84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2c6e64f-5d8f-4487-9943-ce06813193da",
            "compositeImage": {
                "id": "a828a572-4370-40df-b0ab-0f3fe9066196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed74ecb-1580-4972-bef0-697a42bd7e84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88ba4996-56ed-40be-88c7-c6f771e00783",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed74ecb-1580-4972-bef0-697a42bd7e84",
                    "LayerId": "4991bb3e-9572-428c-ad61-2a274493f0ea"
                }
            ]
        },
        {
            "id": "12ff7607-e65c-4592-827c-8a10df98a1e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2c6e64f-5d8f-4487-9943-ce06813193da",
            "compositeImage": {
                "id": "f4ce975f-41bc-40e8-90c8-21333e172739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12ff7607-e65c-4592-827c-8a10df98a1e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "339ecf01-7556-4cb3-9a43-0d6d259df52c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12ff7607-e65c-4592-827c-8a10df98a1e6",
                    "LayerId": "4991bb3e-9572-428c-ad61-2a274493f0ea"
                }
            ]
        },
        {
            "id": "90bd2032-a5eb-44af-a414-30d9f78bb3a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2c6e64f-5d8f-4487-9943-ce06813193da",
            "compositeImage": {
                "id": "1fefa448-f196-42f8-8cf9-5569f2f625e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90bd2032-a5eb-44af-a414-30d9f78bb3a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e230c307-7333-419d-8540-02eee9383233",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90bd2032-a5eb-44af-a414-30d9f78bb3a6",
                    "LayerId": "4991bb3e-9572-428c-ad61-2a274493f0ea"
                }
            ]
        },
        {
            "id": "76e6547c-d372-4c40-a864-75f497ce5572",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2c6e64f-5d8f-4487-9943-ce06813193da",
            "compositeImage": {
                "id": "37e2097d-9e41-4480-b42c-a514cd99e3bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76e6547c-d372-4c40-a864-75f497ce5572",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07431afe-19ec-482b-9e18-5aa904c11b7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76e6547c-d372-4c40-a864-75f497ce5572",
                    "LayerId": "4991bb3e-9572-428c-ad61-2a274493f0ea"
                }
            ]
        },
        {
            "id": "5e246111-1c44-4239-98e5-36ed4d4dcde3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2c6e64f-5d8f-4487-9943-ce06813193da",
            "compositeImage": {
                "id": "e7d70859-bf0f-4b17-bccf-1375a34cbac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e246111-1c44-4239-98e5-36ed4d4dcde3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0c8d1b9-d2d7-41c0-9fe4-8e61d1cd9abd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e246111-1c44-4239-98e5-36ed4d4dcde3",
                    "LayerId": "4991bb3e-9572-428c-ad61-2a274493f0ea"
                }
            ]
        },
        {
            "id": "9babf45c-753b-48b2-a685-7c51da462772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2c6e64f-5d8f-4487-9943-ce06813193da",
            "compositeImage": {
                "id": "b651db4a-971b-4b8d-b97f-1cf2b99a1bc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9babf45c-753b-48b2-a685-7c51da462772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91955bed-b60a-42b8-b7d2-e0a041bc0ede",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9babf45c-753b-48b2-a685-7c51da462772",
                    "LayerId": "4991bb3e-9572-428c-ad61-2a274493f0ea"
                }
            ]
        },
        {
            "id": "7cd59791-3465-4e4b-b7dd-656d086cb827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2c6e64f-5d8f-4487-9943-ce06813193da",
            "compositeImage": {
                "id": "fc93bc66-7510-4ede-bc73-9e0d9463d556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cd59791-3465-4e4b-b7dd-656d086cb827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d73467f-91ed-44b6-a043-65303096c81b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cd59791-3465-4e4b-b7dd-656d086cb827",
                    "LayerId": "4991bb3e-9572-428c-ad61-2a274493f0ea"
                }
            ]
        },
        {
            "id": "7e693da7-92bc-495b-af9e-ee7c5367db10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2c6e64f-5d8f-4487-9943-ce06813193da",
            "compositeImage": {
                "id": "b83766cd-5786-42f2-8172-73e4642f88b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e693da7-92bc-495b-af9e-ee7c5367db10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddd18adc-c901-46fc-aba7-fa97d8456cdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e693da7-92bc-495b-af9e-ee7c5367db10",
                    "LayerId": "4991bb3e-9572-428c-ad61-2a274493f0ea"
                }
            ]
        },
        {
            "id": "39484311-f9af-4779-86dd-2d694b92abf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2c6e64f-5d8f-4487-9943-ce06813193da",
            "compositeImage": {
                "id": "296e1dff-50c2-41d7-9051-b06357726c0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39484311-f9af-4779-86dd-2d694b92abf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6f1b111-c08b-4020-a3f2-9579361613f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39484311-f9af-4779-86dd-2d694b92abf4",
                    "LayerId": "4991bb3e-9572-428c-ad61-2a274493f0ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "4991bb3e-9572-428c-ad61-2a274493f0ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2c6e64f-5d8f-4487-9943-ce06813193da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}