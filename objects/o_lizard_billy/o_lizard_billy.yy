{
    "id": "a3970eab-2941-4659-a52b-b595abbd0374",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_lizard_billy",
    "eventList": [
        {
            "id": "7b3a9674-d01a-4aed-929f-663e9c9108e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a3970eab-2941-4659-a52b-b595abbd0374"
        },
        {
            "id": "67363b07-7bfe-4830-9506-12ccfa59a59b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "a3970eab-2941-4659-a52b-b595abbd0374"
        },
        {
            "id": "79e8a269-a185-492d-bda9-1095fcb7a9e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "a3970eab-2941-4659-a52b-b595abbd0374"
        },
        {
            "id": "dce7db15-0e66-431a-b2d6-73e6c11f76a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a3970eab-2941-4659-a52b-b595abbd0374"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "046d396d-2913-4110-9e0b-8ba8614cd420",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "94562f1a-dbf3-4823-9268-02a240e4f3fc",
    "visible": true
}