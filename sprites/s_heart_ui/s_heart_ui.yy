{
    "id": "ad885076-7aed-4524-aa89-ba7d23f4677f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_heart_ui",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28781df8-01ea-45a4-a7db-aec2fac35d4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad885076-7aed-4524-aa89-ba7d23f4677f",
            "compositeImage": {
                "id": "053b511c-7e78-4455-b2bc-7864c7f0830d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28781df8-01ea-45a4-a7db-aec2fac35d4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f6c74bf-1dcf-41f8-820f-46b63f3580d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28781df8-01ea-45a4-a7db-aec2fac35d4e",
                    "LayerId": "c3b25bb8-67de-4ded-b1db-0400bbce3fdb"
                }
            ]
        },
        {
            "id": "c5d98bb8-b953-4437-86c8-647fb40b4b30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad885076-7aed-4524-aa89-ba7d23f4677f",
            "compositeImage": {
                "id": "1f5c0607-b312-4546-9958-e5e45a5e9773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5d98bb8-b953-4437-86c8-647fb40b4b30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa4e38c-0a16-419d-a237-4a8d96a4cc4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5d98bb8-b953-4437-86c8-647fb40b4b30",
                    "LayerId": "c3b25bb8-67de-4ded-b1db-0400bbce3fdb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "c3b25bb8-67de-4ded-b1db-0400bbce3fdb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad885076-7aed-4524-aa89-ba7d23f4677f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}